//[[Rcpp::depends(RcppArmadillo)]]
#include "utils.h"
using namespace Rcpp;


// [[Rcpp::export]]
arma::imat get_uptri_mask(unsigned int nrow, 
	unsigned int ncol, 
	bool PosDiag=true){
	bool tran_flag = false;
	if (nrow < ncol) {
		tran_flag = true;
		unsigned int temp = ncol;
		ncol = nrow;
		nrow = temp;
	}
	arma::imat Z(nrow, ncol); Z.fill(2);
	
	unsigned int r_from, r_to;
	bool zero_flag = false;
	for (unsigned int c=0; c<ncol; c++) {
		if (PosDiag) {
			Z(c,c) = 1;
		}

		if (tran_flag) {
			r_from = c+1;
			r_to = nrow;
			zero_flag = true;
		} else {
			r_from = 0;
			r_to = c;
			zero_flag = (c>0);
		}


		if (zero_flag) {
			for (unsigned int r=r_from; r<r_to; r++) {
				Z(r,c) = 0;
			}
		}
	}

	if (tran_flag) {
		return Z.t();
	} else {
		return Z;
	}
}



double loglike_para(const arma::mat& Y, // N x T
	const arma::mat& Lambda, // N x K
	const arma::mat& F, // K x T
	const arma::colvec& esigma2) { // N x 1
	int T = Y.n_cols;
	arma::mat E = Y - Lambda*F;
	double rss = arma::trace(E.t()*arma::diagmat(1./esigma2)*E);
	return -.5 * (T*arma::accu(arma::log(esigma2)) + rss);
}


double loglike_model(const arma::mat& Y, // N x T
	const arma::cube& Lambda, // N x K x B
	const arma::cube& F, // K x T x B
	const arma::mat& esigma2) { // N x B

	int B = Lambda.n_slices;
	double loglike = 0;
	for (int b=0; b<B; b++) {
		loglike += loglike_para(Y, Lambda.slice(b), F.slice(b), 
			esigma2.col(b));
	}

	loglike /= B;

	return loglike;
}


arma::cube R2ArmaCube(SEXP Vec) {
	NumericVector VecRcpp = Rcpp::as<NumericVector>(Vec);
	IntegerVector VecDims = VecRcpp.attr("dim");

	int ndim = VecDims.length();
	if (ndim == 2) {
		VecDims.push_front(1);
	} else if (ndim == 1) {
		VecDims.push_front(1);
		VecDims.push_front(1);
	}

	arma::cube VecCube(VecRcpp.begin(), 
		VecDims[0], VecDims[1], VecDims[2], false);

	return VecCube;
}


arma::mat cube2mat(arma::cube A){ // column-wise operation
	return arma::mat(A.memptr(),A.n_rows*A.n_cols,
		A.n_slices,false);
}


// [[Rcpp::export]]
SEXP dyn_corr(const arma::mat& Y, 
	SEXP Lambda_in, 
	SEXP h_in, 
	SEXP esigma2_in){

	// SEXP to arma - Fast.
	arma::cube Lambda = R2ArmaCube(Lambda_in); // N x K x B
	arma::cube h = R2ArmaCube(h_in); // T x K x B

	NumericMatrix esigma2_rcpp(esigma2_in);
 	arma::mat esigma2(esigma2_rcpp.begin(), esigma2_rcpp.nrow(), 
 		esigma2_rcpp.ncol(), false);

	if (Lambda.n_slices != h.n_slices || Lambda.n_slices != esigma2.n_cols) {
		::Rf_error("dyn_corr: Numbers of samples of Lambda, h, esigma2 should be equal");
	}

	if (Lambda.has_nan() || Lambda.has_inf()) {
		::Rf_error("dyn_corr: NaN or Inf in LambdaArma.");
	}
	if (h.has_nan() || h.has_inf()) {
		::Rf_error("dyn_corr: NaN or Inf in hArma.");
	}

	// Initialization - Fast.

	const int N = Y.n_rows;
	const int T = Y.n_cols;
	const int B = Lambda.n_slices;

	arma::mat D_tmp(N,N);
	arma::mat In(N,N,arma::fill::eye);

	arma::vec Sigmat_tmpv(B,arma::fill::zeros);

	arma::cube Sigmat_avg(N,N,T); Sigmat_avg.fill(0.0);
	arma::cube Sigmat_std(N,N,T); Sigmat_std.fill(0.0);
	arma::cube Sigmat_tmp(N,N,B); Sigmat_tmp.fill(0.0);

	// Calculation - Slow

	for (int t=0; t<T; t++) {
		for (int b=0; b<B; b++) {
			Sigmat_tmp.slice(b) = Lambda.slice(b) * arma::diagmat(arma::exp(h.slice(b).row(t))) * Lambda.slice(b).t() + arma::diagmat(esigma2.col(b));
			if (Sigmat_tmp.slice(b).has_nan() || Sigmat_tmp.slice(b).has_inf()) {
				::Rf_error("dyn_corr: NaN or Inf in Sigmat_tmp.slice(b).");
				Rcout << Sigmat_tmp.slice(b).diag().t() << std::endl;
			}
			
			D_tmp = arma::diagmat(1 / arma::sqrt(Sigmat_tmp.slice(b).diag()));
			Sigmat_tmp.slice(b) = D_tmp * Sigmat_tmp.slice(b) * D_tmp;
		}

		Sigmat_avg.slice(t) = arma::mean(Sigmat_tmp, 2);

		for (int i=0; i<N; i++) {
			for (int j=0; j<N; j++) {
				Sigmat_tmpv = Sigmat_tmp.subcube(i,j,0,i,j,B-1);
				Sigmat_std(i,j,t) = arma::stddev(Sigmat_tmpv);
			}
		}

	}

	List Sigmat_stat;
	Sigmat_stat["mean"] = Rcpp::wrap(Sigmat_avg);
	Sigmat_stat["std"] = Rcpp::wrap(Sigmat_std);
	return Sigmat_stat;
}

#ifndef _SAMPLERLIN_H
#define _SAMPLERLIN_H


#define ARMA_NO_DEBUG
// #define ARMA_DONT_PRINT_ERRORS


#include <iostream>
#include <vector>
#include <cassert>
#include <cmath>
#include <algorithm>
#include <RcppArmadillo.h>
#include <RcppArmadilloExtensions/sample.h>
#include <bigmemory/BigMatrix.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "updateparams.h"
#include "posteriors.h"
#include "./stochvol/auxmix.h"
#include "./stochvol/progutils.h"
#include "./stochvol/regression.h"
#include "./stochvol/parameterization.h"
#include "./GIGrvg/GIGrvg.h"
#include "../evaluation/evaluation.h"

Rcpp::List gibbs_dfsv_SS(
	SEXP Y_pb, // N x Tall x S
	const arma::mat &group, // Tall x G, g=0 is the baseline
	const arma::mat &X, // S x M, M behavioral/clinical measure for S subjects
	const arma::vec &Design, // G x 1 mask for design matrix, entries in {-1,0,1}
	const arma::mat &aPiProb, // N x K, =0 Jeffrey's, =1 Uniform, =2 Epsilon, =3 m_pi=0.7 a_pi=10, =4 m_pi=0.5, =5 m_pi=0.1, =6 ROI ICA, =7 Voxel ICA
	const arma::mat &bPiProb, // N x K, =0 Jeffrey's, =1 Uniform, =2 Epsilon, =3 m_pi=0.7 a_pi=10, =4 m_pi=0.5, =5 m_pi=0.1, =6 ROI ICA, =7 Voxel ICA
	const arma::imat &mask, // N x K
	SEXP hMu_pb, // (K*G*S) x nkeep, Pointer for storage
	SEXP hPhi_pb, // (K*G*S) x nkeep, Pointer for storage
	SEXP hSigma_pb, // (K*G*S) x nkeep, Pointer for storage
	SEXP ModLogLike_pb, // (S*G) x nkeep
	SEXP XBeta_pb, // (K*M) x nkeep, SEXP XMu_pb, // M x nkeep, SEXP XVarInv_pb, // M x nkeep
	SEXP Pi_pb, // (N*K) x nkeep
	const arma::vec& dims, // {K,N,G,Tall,S,M}
	const arma::vec& niters, // {nburnin,nthin,B,nkeep}
	arma::cube FacTmp, // K x Tall x S
	arma::cube LambdaTmp, // N x K x S
	arma::cube VarObsTmp, // N x G x S
	arma::mat PiTmp, // N x K
	arma::mat XBetaTmp, // K x M, arma::vec XMuTmp, // M x 1, arma::vec XVarInvTmp, // M x 1
	arma::mat hMuTmp, // K x (G*S)
	arma::mat hPhiTmp, // K x (G*S)
	arma::mat hSigmaTmp, // K x (G*S)
	arma::cube hTmp, // K x Tall x S
	arma::cube h0, // K x G x S
	arma::imat r, // T x (K*G*S)
	arma::cube ZLambda, // N x K x S
	const arma::vec& behav_hyperpara, // N(a_x,b_x), IG(c_x,d_x)
	const arma::vec& ng_hyperpara, // (a_ng,c_ng,d_ng),normal-gamma prior for shrinkage 
	const arma::vec& sv_hyperpara, // {Bsigma,a_phi,b_phi,bMuSv,BMuSv}
	const double c_sigma, // inverse-gamma prior for variance - shape
	double d_sigma, // inverse-gamma prior for variance - rate
	const double B011inv, // 10^-12
	const double B022inv, // 10^-8
	const int parameterization, // {1,2,3,4}
	const int MHsteps, // default = 2
	const bool dontupdatemu,
	const bool MultiSubject,
	const bool BehavioralResponse,
	const int NormVarLambdaNK, // -1=10, 0=exp(hMuBase), 1=tau(k)^2
	const bool IsotropicObsVar,
	const bool fixdiag,
	const bool Gammaprior,
	const bool FactorLevel,
	const double ci_coverage,
	const bool debug); 


Rcpp::List gibbs_reg_sparse(
	const arma::mat &Delta, // S x K, Pointer for storage
	const arma::mat &Z, // S x M
	const unsigned int nburnin,
	const unsigned int niter,
	const unsigned int nthin,
	const double a_beta,
	const double b_beta,
	const double s_ig,
	const double alpha1_ig,
	const double alpha2_ig);

#endif
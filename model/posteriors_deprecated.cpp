void update_Lambda(arma::mat& Lambda_new,// Lambda: N x K
	const arma::mat& Y, // N x T
	const arma::mat& F, // K x T
	const arma::vec& esigma2, // error variance
	const double& sigma2_v, // prior for Lambda
	const bool& restricted=false) { // checked.
	/*
	Prior distribution of Lambda_ij
		N(0, sigma2_v)

	Posterior distribution of Lambda_ij
		N(Sigma*F*Y[i,:]/esigma2, Sigma.i() = I_K/sigma2_v + F*F.t()/esigma2)

	Interweaving
		(Lambda_kk)^2 ~ GIG(lambda=(N+1-T)/2, 
							psi=(1+Lambda[:,k].t()*Lambda[:,k]/Lambda[k,k])/sigma2_v),
							chi=f[k,:].t()Lambda[k,k]^2f[k,:]/sigma2_v)

	*/
	int K = F.n_rows; 
	int N = Y.n_rows;
	int r = K;
	double tmp;

	arma::mat Ftmp, Psi, Mu;
	arma::mat SigmaInv, SigmaInv_chol, SigmaInv_R2, Sigma;

	for (int i=0; i<N; i++) { // sample Lambda by row
		if (restricted) {
			r = ((i+1)<K?(i+1):K);
		}
		
		Psi = arma::mat(r,r,arma::fill::eye) / sigma2_v;
		Ftmp = F.rows(0,r-1);
		SigmaInv = Psi + (Ftmp*Ftmp.t()/esigma2(i)); // checked. no guarantee to be psd
		try {
			SigmaInv_chol = arma::chol(SigmaInv);
		} catch (...) {
			::Rf_error("update_Lambda: Couldn't Cholesky-decompose posterior factor precision Sigma - 1.");
		}

		try {
			if (VERBOSE) {
				tmp = arma::rcond(arma::trimatu(SigmaInv_chol));
				if (tmp < EPSILON) {
					Rcout << "update_Lambda: trimatu(SigmaInv_chol) seems singular - rcond=" << tmp << std::endl;
				}
			}
			
			SigmaInv_R2 = arma::solve(arma::trimatu(SigmaInv_chol), 
				arma::eye<arma::mat>(r,r));
		} catch (...) {
			::Rf_error("update_Lambda: Couldn't invert Cholesky factor of posterior factor precision Sigma - 2.");
		}

		Sigma = SigmaInv_R2 * SigmaInv_R2.t();
		Mu = Sigma * Ftmp* Y.row(i).t() / esigma2(i);
		Lambda_new.row(i).head(r) = (Mu + SigmaInv_R2*arma::randn(r,1)).t();

		Psi.reset();
		Ftmp.reset();
		SigmaInv.reset();
		SigmaInv_chol.reset();
		SigmaInv_R2.reset();
		Sigma.reset();
		Mu.reset();
	}

	// if (restricted) {
	// 	Lambda.diag().ones();
	// }

	return;
}




void update_Lambda_NG(arma::mat& Lambda_new,
	const arma::mat& Y, 
	const arma::mat& tau2, 
	const arma::mat& F, 
	const arma::vec& esigma2, 
	const bool& restricted=false) { // checked.

	int K = F.n_rows; // F: K x T
	int N = Y.n_rows; // Y: N x T
	int r = K;

	arma::mat Ftmp, Sigma, Sigma_chol, Sigma_invR2;
	arma::vec Mu;

	for (int i=0; i<N; i++) {
		if (restricted) {
			r = std::min(i+1, K);
		}
		Ftmp = F.rows(0,r-1).t(); // T x r

		// Calculate posterior precision matrix
		Sigma = (Ftmp.t()*Ftmp/esigma2(i)); // r x r
		Sigma.diag() += 1/arma::nonzeros(tau2.row(i).head(r));

		try { // Find Cholesky factor of posterior precision matrix
			Sigma_chol = arma::chol(Sigma);
		} catch (...) {
			::Rf_error("update_Lambda_NG: Cholesky-decompose posterior factor precision - 1.");
		}
		try { // Calculate the inverse of upper triangular Cholesky factor
			Sigma_invR2 = arma::solve(arma::trimatu(Sigma_chol), arma::eye<arma::mat>(r,r));
		} catch (...) {
			::Rf_error("update_Lambda_NG: Invert the Cholesky factor of posterior factor precision - 2.");
		}

		// Calculate the posterior covariance
		Sigma = Sigma_invR2 * Sigma_invR2.t();
		Mu = Sigma * Ftmp.t() * Y.row(i).t() / esigma2(i);
		Lambda_new.row(i).head(r) = (Mu + Sigma_invR2*arma::randn(r,1)).t();

		Ftmp.reset();
		Sigma.reset();
		Sigma_chol.reset();
		Sigma_invR2.reset();
		Mu.reset();
	}

	return;
}


void update_Lambda_SS(arma::mat& Z, // N x K
	arma::mat& Lambda_new,
	const arma::mat& Lambda,  // N x K
	const arma::mat& Y, // N x T
	const arma::mat& F,  // K x T
	const arma::vec& fsigma2, // K x 1
	const arma::vec& esigma2, // N x 1
	const double& beta=1.0) {
	/*
	Lambda: N x K, factor loadings
	Z: N x K, indicator matrix of factor loadings
	Y: N x T, time series at the node level
	F: K x T, time series at the factor level

	model: Y = F * Lambda' or Y' = Lambda * F'
	*/
	int N = Lambda.n_rows;
	int K = Lambda.n_cols;
	int T = Y.n_cols;
	int mk = N-1;

	arma::uvec indices(K,arma::fill::zeros);
	arma::uvec idx(K-1,arma::fill::zeros);
	arma::uvec ii(1,arma::fill::zeros);
	arma::rowvec Err(T,arma::fill::zeros);
	arma::colvec tmp(N,arma::fill::zeros);
	double lambda, mu;
	double logodd_prior, logodd_ll, logodd_post, prob;

	for (unsigned int k=0; k<K; k++) {
		indices.at(k) = k;
	}

	for (unsigned int k=0; k<K; k++) {
		idx = indices.elem(arma::find(indices!=k));

		for (unsigned int i=0; i<N; i++) {
			mk = arma::accu(Z.col(k)) - Z(i,k);
			if (mk > 0) {
				// prior odds
				logodd_prior = std::log( mk / (beta+N-1-mk));

				// likelihood odds
				ii.at(0) = i;
				Err = Y.row(i) - Lambda(ii,idx) * F.rows(idx);
				lambda = arma::accu(arma::square(F.row(k))) / esigma2(i) + 1.0 / fsigma2.at(k);
				mu = (1/esigma2(i))*arma::accu(F.row(k)%Err) / lambda;
				logodd_ll = 0.5 * (lambda*std::pow(mu,2.0)-std::log(lambda)-std::log(fsigma2.at(k)));

				// posterior odds
				logodd_post = logodd_prior + logodd_ll;
				prob = 1.0 / (1.0+std::exp(-logodd_post));

				if (!std::isfinite(prob)) {
					::Rf_error("update_Lambda_SS: Infinite posterior log odds.");
				}

				Z.at(i,k) = (arma::randu(1)[0]<prob?1:0);
				if (Z.at(i,k) == 1) {
					Lambda_new.at(i,k) = mu + std::pow(lambda,-0.5)*arma::randn(1)[0];
				} else {
					Lambda_new.at(i,k) = 0.0;
				}
			}
		}
	}

	return;
}


void update_Lambda_SS(arma::mat& Z, // N X K
	arma::mat& Pi,
	arma::mat& Lambda_new,
	const arma::mat& Lambda, // N x K, factor loadings
	const arma::mat& Y, // N x T, time series at the node level
	const arma::mat& F, // K x T, time series at the factor level
	const arma::vec& fsigma2, 
	const arma::vec& esigma2, 
	const arma::vec& Pi_rho, // prior for Pi(rho,a,m)
	const arma::vec& Pi_a,
	const arma::vec& Pi_m) {
	/*
	model: Y = F * Lambda' or Y' = Lambda * F'
	*/
	int N = Lambda.n_rows;
	int K = Lambda.n_cols;
	int T = Y.n_cols;
	int mk = N-1;

	// N x K, indicator matrix of factor loadings
	arma::uvec indices(K,arma::fill::zeros);
	arma::uvec idx(K-1,arma::fill::zeros);
	arma::uvec ii(1,arma::fill::zeros);
	arma::rowvec Err(T,arma::fill::zeros);
	arma::colvec tmp(N,arma::fill::zeros);
	double lambda, mu;
	double logodd_prior, logodd_ll, logodd_post, prob;
	double temp;

	for (unsigned int k=0; k<K; k++) {
		indices(k) = k;
	}

	for (unsigned int k=0; k<K; k++) {
		idx = indices.elem(arma::find(indices!=k));

		for (unsigned int i=0; i<N; i++) {
			mk = arma::accu(Z.col(k)) - Z(i,k);
			if (mk > 0) {
				// prior odds
				temp = Pi_rho(k)*Pi_m(k);
				logodd_prior = std::log( temp/(1-temp));
				if (!std::isfinite(logodd_prior)) {
					::Rf_error("update_Lambda_SS: Infinite prior log odds.");
				}

				// likelihood odds
				ii(0) = i;
				Err = Y.row(i) - Lambda(ii,idx) * F.rows(idx);
				lambda = arma::accu(arma::square(F.row(k))) / esigma2(i) + 1 / fsigma2(k);
				mu = (1/esigma2(i))*arma::accu(F.row(k)%Err) / lambda;
				logodd_ll = 0.5 * (lambda*std::pow(mu,2.0)-std::log(lambda)-std::log(fsigma2(k)));

				// posterior odds
				logodd_post = logodd_prior + logodd_ll;
				prob = 1 / (1+std::exp(-logodd_post));

				if (!std::isfinite(prob)) {
					Rcout << "logodd_prior: " << logodd_prior << std::endl;
					Rcout << "logodd_ll: " << logodd_ll << std::endl;
					Rcout << "Posterior prob: " << prob << std::endl;
					::Rf_error("update_Lambda_SS: Infinite posterior probability.");
				}

				Z(i,k) = (arma::randu(1)[0]<prob?1:0);
				if (Z(i,k) == 1) {
					Lambda_new(i,k) = mu + std::pow(lambda,-0.5)*arma::randn(1)[0];
				} else {
					Lambda_new(i,k) = 0;
				}

				if (Lambda_new(i,k) == 0) {
					prob = Pi_rho(k)*(1-Pi_m(k)) / (1-Pi_rho(k)*Pi_m(k));
					Z(i,k) = (arma::randu(1)[0]<prob?1:0);
					if (Z(i,k) == 1) {
						Pi(i,k) = R::beta(Pi_a(k)*Pi_m(k), 
							Pi_a(k)*(1-Pi_m(k))+1);
					} else {
						Pi(i,k) = 0;
					}

				} else { // Lambda_new(i,k) != 0
					Pi(i,k) = R::rbeta(Pi_a(k)*Pi_m(k)+1,
						Pi_a(k)*(1-Pi_m(k)));
				}
			}
		}
	}

	return;
}


void update_Lambda_SS_fused(arma::mat& Z, 
	arma::mat& Lambda_new,
	const arma::mat& Lambda, 
	const arma::mat& YA, 
	const arma::mat& YB,
	const arma::mat& FA,
	const arma::mat& FB, 
	const arma::vec& fsigma2, 
	const arma::vec& esigma2A,
	const arma::vec& esigma2B, 
	const double& beta) {
	/*
	Lambda: N x K, factor loadings
	Z: N x K, indicator matrix of factor loadings
	Y: N x T, time series at the node level
	F: K x T, time series at the factor level

	model: Y = F * Lambda' or Y' = Lambda * F'
	*/
	const int N = Lambda.n_rows;
	const int K = Lambda.n_cols;
	const int Ta = YA.n_cols;
	const int Tb = YB.n_cols;
	int mk = N-1;

	arma::uvec indices(K,arma::fill::zeros);
	arma::uvec idx(K-1,arma::fill::zeros);
	arma::uvec ii(1,arma::fill::zeros);
	arma::rowvec ErrA(Ta,arma::fill::zeros);
	arma::rowvec ErrB(Tb,arma::fill::zeros);
	arma::colvec tmp(N,arma::fill::zeros);
	double lambda, mu;
	double logodd_prior, logodd_ll, logodd_post, prob;

	for (unsigned int k=0; k<K; k++) {
		indices(k) = k;
	}

	for (unsigned int k=0; k<K; k++) {
		idx = indices.elem(arma::find(indices!=k));

		for (unsigned int i=0; i<N; i++) {
			mk = sum(Z.col(k)) - Z(i,k);
			if (mk > 0) {
				// prior odds
				logodd_prior = std::log( mk / (beta+N-1-mk));

				// likelihood odds
				ii(0) = i;
				ErrA = YA.row(i) - Lambda(ii,idx) * FA.rows(idx);
				ErrB = YB.row(i) - Lambda(ii,idx) * FB.rows(idx);

				lambda = arma::accu(arma::square(FA.row(k))) / esigma2A(i) + arma::accu(arma::square(FB.row(k))) / esigma2B(i) + 1 / fsigma2(k);
				mu = (arma::accu(FA.row(k)%ErrA)/esigma2A(i) + arma::accu(FB.row(k)%ErrB)/esigma2B(i)) / lambda;
				logodd_ll = 0.5 * (lambda*std::pow(mu,2.0)-std::log(lambda)-std::log(fsigma2(k)));

				// posterior odds
				logodd_post = logodd_prior + logodd_ll;
				prob = 1 / (1+std::exp(-logodd_post));

				if (!std::isfinite(prob)) {
					::Rf_error("update_Lambda_SS_fused: Infinite posterior log odds.");
				}

				Z(i,k) = (arma::randu(1)[0]<prob?1:0);
				if (Z(i,k) == 1) {
					Lambda_new(i,k) = mu + std::pow(lambda,-0.5)*arma::randn(1)[0];
				} else {
					Lambda_new(i,k) = 0;
				}
			}
		}
	}

	return;
}



arma::mat update_Lambda_SS_single(const arma::mat& Pi, // N x K
	const arma::mat& LambdaOld, // N x K
	const arma::mat& Y, // N x Tall
	const arma::mat& F, // K x Tall
	const arma::vec& tau, // K, variance of nonzero Lambda(n,k)
	const arma::mat& esigma2, // N x G
	const arma::mat& group, // Tall x G
	const arma::imat& mask) { // N x K

	const unsigned int N = Pi.n_rows;
	const unsigned int K = Pi.n_cols;
	const unsigned int Tall = group.n_rows;
	const unsigned int G = group.n_cols;

	arma::mat LambdaNew(N,K,arma::fill::zeros);
	arma::vec Err_n(Tall,arma::fill::zeros);
	arma::uvec idx;

	for (unsigned int n=0; n<N; n++) { //parallel
		Err_n = (Y.row(n) - LambdaOld.row(n)*F).t();

		for (unsigned int k=0; k<K; k++) {
			if (mask(n,k) == 0) { 
				// zero restriction
				LambdaNew(n,k) = 0;
				
			} else {
				// Spike-and-slab prior
				LambdaNew(n,k) = update_Lambda_nk(Pi(n,k), LambdaOld(n,k), 
					Err_n, F.row(k).t(), tau(k), esigma2.row(n).t(), 
					group, mask(n,k),G,idx);
			}
		}
	}

	return LambdaNew;
}

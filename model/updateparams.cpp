//[[Rcpp::depends(RcppArmadillo)]]
#include "updateparams.h"
using namespace Rcpp;


void initialize(const arma::cube& Y, // N x Tall x S
	const arma::mat& group, // Tall x G
	const int K,
	arma::cube& MuObs, // N x G x S
	arma::cube& VarObs,  // N x G x S
	arma::cube& Lambda, // N x K x S
	arma::cube& F, // K x Tall x S
	arma::cube& h, // K x Tall x S
	arma::mat& hMu_g, // K x (G*S)
	arma::mat& hPhi_g, // K x (G*S)
	arma::mat& hVar_g, // K x (G*S)
	arma::mat& hMu, // 
	arma::mat& hMuVar){

	double c = 1e-4; 

	const int N = Y.n_rows;
	const int G = group.n_cols;
	const int Tall = Y.n_cols;
	const int S = Y.n_slices;

	arma::uvec idx;
	unsigned int idx_tg_from = 0;
	unsigned int idx_tg_to = -1;
	int Tg=0;

	/* BEGIN -- Assigning initial values */
	// 1. Initialize observed mean and var
	arma::cube Ycentered(N,Tall,S);
	arma::mat Ytemp;
	arma::vec Mutemp;
	for (int g=0; g<G; g++) {
		idx = arma::find(group.col(g)==1);
		Tg = idx.n_elem;
		idx_tg_from = idx_tg_to + 1;
		idx_tg_to = idx_tg_from + Tg - 1;
		for (int s=0; s<S; s++) {
			Ytemp = Y.slice(s).cols(idx_tg_from,idx_tg_to); // N x Tg

			Mutemp = arma::mean(Ytemp,1);
			MuObs.slice(s).col(g) = Mutemp; // N x 1
			VarObs.slice(s).col(g) = arma::var(Ytemp,0,1); // N x 1

			Ytemp.each_col() -= Mutemp;
			Ycentered.slice(s).cols(idx_tg_from,idx_tg_to) = Ytemp;
			Ytemp.clear(); Ytemp.reset();
		}

		idx.clear(); idx.reset();
	}

	Ytemp = arma::mat(Ycentered.memptr(),N,Tall*S,false);

	// 2. Initialize latent factors with Principle component analysis
	
	arma::mat coeff, score; // coeff: N x K, score: Tall*S x K
	arma::vec latent, tsquared;
	arma::princomp(coeff, score, latent, tsquared, Ytemp.t());
	
	arma::mat Ftemp0;
	arma::mat acov(2,K,arma::fill::zeros); // acov(0), acov(1)
	for (int s=0; s<S; s++) {
		Lambda.each_slice() = coeff.cols(0,K-1);

		idx_tg_to = -1;
		for (int g=0; g<G; g++) {
			idx = arma::find(group.col(g)==1);
			Tg = idx.n_elem;
			idx_tg_from = idx_tg_to + 1;
			idx_tg_to = idx_tg_from + Tg - 1;

			Ftemp0 = score(arma::span(s*Tall+idx_tg_from,s*Tall+idx_tg_to),arma::span(0,K-1)); // Tg x K
			F.slice(s).cols(idx_tg_from,idx_tg_to) = Ftemp0.t();

			// 3. Initialize stochastic volatilities
			Ftemp0 = arma::log(arma::square(Ftemp0)+c); // Tg x K
			h.slice(s).cols(idx_tg_from,idx_tg_to) = Ftemp0.t();
			hMu_g.col(G*s+g) = arma::mean(Ftemp0,0).t(); // K x 1

			Ftemp0.each_row() -= hMu_g.col(G*s+g).t(); // Tg x K, demean
			acov.row(0) = arma::var(Ftemp0,0); // 1 x K
			acov.row(1) = arma::sum(Ftemp0.rows(0,Tg-2)%Ftemp0.rows(1,Tg-1),0) / Tg;
			hPhi_g.col(G*s+g) = (acov.row(1) / acov.row(0)).t();
			hVar_g.col(G*s+g) = (acov.row(0)-arma::square(acov.row(1))/acov.row(0)).t();
			hMu.col(s) = arma::mean(hMu_g.cols(G*s,G*(s+1)-1),1); // K x 1
			hMuVar.col(s) = arma::var(hMu_g.cols(G*s,G*(s+1)-1),0,1);

			Ftemp0.clear(); Ftemp0.reset();
			idx.clear(); idx.reset();
		}
	}
	/* END -- Assigning initial values */
	return;
}


void initialize(const arma::cube& Y, // N x Tall x S
	const arma::mat& group, // Tall x G
	const int K,
	arma::cube& MuObs, // N x G x S
	arma::cube& VarObs,  // N x G x S
	arma::cube& Lambda, // N x K x S
	arma::cube& F, // K x Tall x S
	arma::cube& h, // K x Tall x S
	arma::mat& hMu_g, // K x (G*S) 
	arma::mat& hPhi_g, // K x (G*S) 
	arma::mat& hVar_g){ // K x (G*S) 

	double c = 1e-4; 

	const int N = Y.n_rows;
	const int G = group.n_cols;
	const int Tall = Y.n_cols;
	const int S = Y.n_slices;

	arma::uvec idx;
	unsigned int idx_tg_from = 0;
	unsigned int idx_tg_to = -1;
	int Tg=0;

	/* BEGIN -- Assigning initial values */
	// 1. Initialize observed mean and var
	arma::cube Ycentered(N,Tall,S);
	arma::mat Ytemp;
	arma::vec Mutemp;
	for (int g=0; g<G; g++) {
		idx = arma::find(group.col(g)==1);
		Tg = idx.n_elem;
		idx_tg_from = idx_tg_to + 1;
		idx_tg_to = idx_tg_from + Tg - 1;
		for (int s=0; s<S; s++) {
			if (Tg==Tall) {
				Mutemp = arma::mean(Y.slice(s),1);
				MuObs.slice(s).col(g) = Mutemp; // N x 1
				VarObs.slice(s).col(g) = arma::var(Y.slice(s),0,1); // N x 1
				Ycentered.slice(s) = Y.slice(s); // N x Tall
			
			} else {
				Ytemp = Y.slice(s).cols(idx_tg_from,idx_tg_to); // N x Tg

				Mutemp = arma::mean(Ytemp,1);
				MuObs.slice(s).col(g) = Mutemp; // N x 1
				VarObs.slice(s).col(g) = arma::var(Ytemp,0,1); // N x 1

				Ytemp.each_col() -= Mutemp;
				Ycentered.slice(s).cols(idx_tg_from,idx_tg_to) = Ytemp;
				Ytemp.clear(); Ytemp.reset();
			}
			
		}
	}

	Ytemp = arma::mat(Ycentered.memptr(),N,Tall*S,false);

	// 2. Initialize latent factors with Principle component analysis
	arma::mat coeff, score; // coeff: N x K, score: Tall*S x K
	arma::vec latent, tsquared;
	arma::princomp(coeff, score, latent, tsquared, Ytemp.t());
	arma::mat Ftemp0;
	arma::mat acov(2,K,arma::fill::zeros); // acov(0), acov(1)
	for (int s=0; s<S; s++) {
		Lambda.each_slice() = coeff.cols(0,K-1);

		idx_tg_to = -1;
		for (int g=0; g<G; g++) {
			idx = arma::find(group.col(g)==1);
			Tg = idx.n_elem;
			idx_tg_from = idx_tg_to + 1;
			idx_tg_to = idx_tg_from + Tg - 1;

			Ftemp0 = score(arma::span(s*Tall+idx_tg_from,s*Tall+idx_tg_to),arma::span(0,K-1)); // Tg x K
			if (Tg==Tall) {
				F.slice(s) = Ftemp0.t();
			} else {
				F.slice(s).cols(idx_tg_from,idx_tg_to) = Ftemp0.t();
			}

			// 3. Initialize stochastic volatilities
			Ftemp0 = arma::log(arma::square(Ftemp0)+c); // Tg x K
			if (Tg==Tall) {
				h.slice(s) = Ftemp0.t();
			} else {
				h.slice(s).cols(idx_tg_from,idx_tg_to) = Ftemp0.t();
			}
			
			hMu_g.col(G*s+g) = arma::mean(Ftemp0,0).t(); // K x 1

			Ftemp0.each_row() -= hMu_g.col(G*s+g).t(); // Tg x K, demean
			acov.row(0) = arma::var(Ftemp0,0); // 1 x K
			acov.row(1) = arma::sum(Ftemp0.rows(0,Tg-2)%Ftemp0.rows(1,Tg-1),0) / Tg;
			hPhi_g.col(G*s+g) = (acov.row(1) / acov.row(0)).t();
			hVar_g.col(G*s+g) = (acov.row(0)-arma::square(acov.row(1))/acov.row(0)).t();

			// if (g>0){ // calculate delta
			// 	hMu_g.col(G*s+g) -= hMu_g.col(G*s);
			// }
		}
	}
	/* END -- Assigning initial values */


	return;
}


void initialize(const arma::cube& Y,
	const arma::mat& group,
	const int K,
	arma::cube& MuObs,
	arma::cube& VarObs,
	arma::field<arma::sp_mat>& Lambda,
	arma::cube& F,
	arma::cube& h,
	arma::mat& hMu_g, // K x (G*S) 
	arma::mat& hPhi_g, // K x (G*S) 
	arma::mat& hVar_g){ // K x (G*S) 

	double c = 1e-4; 

	const int N = Y.n_rows;
	const int G = group.n_cols;
	const int Tall = Y.n_cols;
	const int S = Y.n_slices;

	arma::uvec idx;
	unsigned int idx_tg_from = 0;
	unsigned int idx_tg_to = -1;
	int Tg=0;

	/* BEGIN -- Assigning initial values */
	// 1. Initialize observed mean and var
	arma::cube Ycentered(N,Tall,S);
	arma::mat Ytemp;
	arma::vec Mutemp;
	for (int g=0; g<G; g++) {
		idx = arma::find(group.col(g)==1);
		Tg = idx.n_elem;
		idx_tg_from = idx_tg_to + 1;
		idx_tg_to = idx_tg_from + Tg - 1;
		for (int s=0; s<S; s++) {
			Ytemp = Y.slice(s).cols(idx_tg_from,idx_tg_to); // N x Tg

			Mutemp = arma::mean(Ytemp,1);
			MuObs.slice(s).col(g) = Mutemp; // N x 1
			VarObs.slice(s).col(g) = arma::var(Ytemp,0,1); // N x 1

			Ytemp.each_col() -= Mutemp;
			Ycentered.slice(s).cols(idx_tg_from,idx_tg_to) = Ytemp;
			Ytemp.clear(); Ytemp.reset();
		}

		idx.clear(); idx.reset();
	}

	Ytemp = arma::mat(Ycentered.memptr(),N,Tall*S,false);

	// 2. Initialize latent factors with Principle component analysis
	
	arma::mat coeff, score; // coeff: N x K, score: Tall*S x K
	arma::vec latent, tsquared;
	arma::princomp(coeff, score, latent, tsquared, Ytemp.t());
	Lambda.for_each([&](arma::sp_mat& X){X = arma::sp_mat(coeff.cols(0,K-1));});
	
	arma::mat Ftemp0;
	arma::mat acov(2,K,arma::fill::zeros); // acov(0), acov(1)
	for (int s=0; s<S; s++) {
		// Lambda.each_slice() = coeff.cols(0,K-1);

		idx_tg_to = -1;
		for (int g=0; g<G; g++) {
			idx = arma::find(group.col(g)==1);
			Tg = idx.n_elem;
			idx_tg_from = idx_tg_to + 1;
			idx_tg_to = idx_tg_from + Tg - 1;

			Ftemp0 = score(arma::span(s*Tall+idx_tg_from,s*Tall+idx_tg_to),arma::span(0,K-1)); // Tg x K
			F.slice(s).cols(idx_tg_from,idx_tg_to) = Ftemp0.t();

			// 3. Initialize stochastic volatilities
			Ftemp0 = arma::log(arma::square(Ftemp0)+c); // Tg x K
			h.slice(s).cols(idx_tg_from,idx_tg_to) = Ftemp0.t();
			hMu_g.col(G*s+g) = arma::mean(Ftemp0,0).t(); // K x 1

			Ftemp0.each_row() -= hMu_g.col(G*s+g).t(); // Tg x K, demean
			acov.row(0) = arma::var(Ftemp0,0); // 1 x K
			acov.row(1) = arma::sum(Ftemp0.rows(0,Tg-2)%Ftemp0.rows(1,Tg-1),0) / Tg;
			hPhi_g.col(G*s+g) = (acov.row(1) / acov.row(0)).t();
			hVar_g.col(G*s+g) = (acov.row(0)-arma::square(acov.row(1))/acov.row(0)).t();

			if (g>0){ // calculate delta
				hMu_g.col(G*s+g) -= hMu_g.col(G*s);
			}

			Ftemp0.clear(); Ftemp0.reset();
			idx.clear(); idx.reset();
		}
	}
	/* END -- Assigning initial values */


	return;
}


void update_meanvar(arma::mat& ParaStats, // nelem x 5
	arma::cube& ParaNew, // dim1 x dim2 x dim3
	const int& idx){

	const arma::colvec ParaNewFlat(ParaNew.memptr(),ParaNew.n_elem,false); // column-wise
	if (idx==0) {
		ParaStats.col(0) = ParaNewFlat;
	} else {
		arma::vec tmp = ParaNewFlat - ParaStats.col(0);
		ParaStats.col(0) += tmp / (idx + 1);
		ParaStats.col(1) += tmp % (ParaNewFlat - ParaStats.col(0));
	}
	
	return;
}



void update_quantile(arma::cube& ParaNew, // dim1 x dim2 x dim3
	std::vector<stmpct::gk<double>>& ParaPct){ // (dim1*dim2*dim3)
	const arma::colvec ParaNewFlat(ParaNew.memptr(),ParaNew.n_elem,false);
	for (unsigned int i=0; i<ParaNewFlat.n_elem; i++) {
		ParaPct[i].insert(ParaNewFlat(i));
	}

	return;
}


arma::mat get_quantile(const std::vector<stmpct::gk<double>>& ParaPct,
	const arma::vec& pct,
	const int nelem){

	unsigned int npct = pct.n_elem;
	arma::mat ParaQuantile(nelem,npct,arma::fill::zeros);

	for (unsigned int i=0; i<nelem; i++) {
		for (unsigned int j=0; j<npct; j++) {
			ParaQuantile(i,j) = (double)ParaPct[i].quantile(pct[j]);
			if (j>0){
				if (ParaQuantile(i,j)<ParaQuantile(i,j-1)){
					::Rf_error("get_quantile: higher quantile has a smaller value.");
				}
			}
		}
	}

	return ParaQuantile;
}


void label_switching(arma::field<arma::cube>& LambdaTmp, //(B)(N,K,S)
	arma::field<arma::cube>& FacTmp,//(B)(K,Tall,S)
	const unsigned int& S,
	const unsigned int& N,
	const unsigned int& K,
	const unsigned int& Tall,
	const arma::uvec& idx_keep) { // nkeep

	const unsigned int B = idx_keep.n_elem;

	#ifdef _OPENMP
	#pragma omp parallel for collapse(2) schedule(auto)
	#endif
	for (unsigned int s=0; s<S; s++) {
		for (unsigned int k=0; k<K; k++) {
			arma::mat Lambda(N,B);
			arma::mat Factor(Tall,B);
			arma::rowvec mysign(B);
			unsigned int identifier = 0;

			for (unsigned int b=0; b<B; b++) {
				Lambda.unsafe_col(b) = LambdaTmp.at(idx_keep.at(b)).slice(s).unsafe_col(k);
				Factor.unsafe_col(b) = FacTmp.at(idx_keep.at(b)).slice(s).row(k).t();
			}

			identifier = arma::min(arma::abs(Lambda),1).index_max();
			mysign = arma::sign(Lambda.row(identifier));

			// for each row, element-wise product with my sign
			Lambda.each_row() %= mysign;
			Factor.each_row() %= mysign;

			for (unsigned int b=0; b<B; b++) {
				LambdaTmp.at(idx_keep.at(b)).slice(s).unsafe_col(k) = Lambda.unsafe_col(b);
				FacTmp.at(idx_keep.at(b)).slice(s).row(k) = Factor.unsafe_col(b).t();
			}

		} // loop through K
	} // loop through S
	return;
} // end of label_switching


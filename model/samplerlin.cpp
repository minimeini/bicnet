#include "samplerlin.h"
using namespace Rcpp;

// [[Rcpp::depends(RcppArmadillo, RcppDist, BH, bigmemory)]]


/*
Identifiability: non-identifiability == multimodality
*/


/*
STATIC FACTOR MODEL WITH(OUT) NORMAL-GAMMA SPARSE SHRINKAGE: gibbs_static_ng

Ref: Kastner, 2019; Kastner et al., 2017.

1. sample factor and idiosyncratic variance: update_static_evar, update_static_fvar
2. sample shrinkage parameters: update_ng_lambda, update_ng_tau
3. sample factor loadings: update_Lambda, shallow_interweave_Lambda
4. sample factors: update_static_factors
*/


//' @export
// [[Rcpp::export]]
Rcpp::List gibbs_dfsv_SS(
	SEXP Y_pb, // N x Tall x S
	const arma::mat &group, // Tall x G, g=0 is the baseline
	const arma::mat &X, // S x M, M behavioral/clinical measure for S subjects
	const arma::vec &Design, // G x 1 mask for design matrix, entries in {-1,0,1}
	const arma::mat &aPiProb, // N x K, =0 Jeffrey's, =1 Uniform, =2 Epsilon, =3 m_pi=0.7 a_pi=10, =4 m_pi=0.5, =5 m_pi=0.1, =6 ROI ICA, =7 Voxel ICA
	const arma::mat &bPiProb, // N x K, =0 Jeffrey's, =1 Uniform, =2 Epsilon, =3 m_pi=0.7 a_pi=10, =4 m_pi=0.5, =5 m_pi=0.1, =6 ROI ICA, =7 Voxel ICA
	const arma::imat &mask, // N x K
	SEXP hMu_pb, // (K*G*S) x nkeep, Pointer for storage
	SEXP hPhi_pb, // (K*G*S) x nkeep, Pointer for storage
	SEXP hSigma_pb, // (K*G*S) x nkeep, Pointer for storage
	SEXP ModLogLike_pb, // (S*G) x nkeep
	SEXP XBeta_pb, // (K*M) x nkeep, SEXP XMu_pb, // M x nkeep, SEXP XVarInv_pb, // M x nkeep
	SEXP Pi_pb, // (N*K) x nkeep
	const arma::vec& dims, // {K,N,G,Tall,S,M}
	const arma::vec& niters, // {nburnin,nthin,B,nkeep}
	arma::cube FacTmp, // K x Tall x S
	arma::cube LambdaTmp, // N x K x S
	arma::cube VarObsTmp, // N x G x S
	arma::mat PiTmp, // N x K
	arma::mat XBetaTmp, // K x M, arma::vec XMuTmp, // M x 1, arma::vec XVarInvTmp, // M x 1
	arma::mat hMuTmp, // K x (G*S)
	arma::mat hPhiTmp, // K x (G*S)
	arma::mat hSigmaTmp, // K x (G*S)
	arma::cube hTmp, // K x Tall x S
	arma::cube h0, // K x G x S
	arma::imat r, // T x (K*G*S)
	arma::cube ZLambda, // N x K x S
	const arma::vec& behav_hyperpara, // N(a_x,b_x), IG(c_x,d_x)
	const arma::vec& ng_hyperpara, // (a_ng,c_ng,d_ng),normal-gamma prior for shrinkage 
	const arma::vec& sv_hyperpara, // {Bsigma,a_phi,b_phi,bMuSv,BMuSv}
	const double c_sigma = 2, // inverse-gamma prior for variance - shape
	double d_sigma = 0, // inverse-gamma prior for variance - rate
	const double B011inv = 0.01, // 10^-12
	const double B022inv = 0.01, // 10^-8
	const int parameterization = 3, // {1,2,3,4}
	const int MHsteps = 2, // default = 2
	const bool dontupdatemu = false,
	const bool MultiSubject = false,
	const bool BehavioralResponse = false,
	const int NormVarLambdaNK = 2, // -1=10, 0=exp(hMuBase), 1=tau(k)^2
	const bool IsotropicObsVar = true,
	const bool fixdiag = true,
	const bool Gammaprior = false,
	const bool FactorLevel = true,
	const double ci_coverage = 0.95,
	const bool debug = true) { 

	unsigned int K = dims.at(0);
	const unsigned int N = dims.at(1);
	const unsigned int G = dims.at(2);
	const unsigned int Tall = dims.at(3);
	const unsigned int S = dims.at(4);
	const unsigned int M = dims.at(5);

	const unsigned int nburnin = niters.at(0);
	const unsigned int nthin = niters.at(1);
	const unsigned int B = niters.at(2);
	const unsigned int nkeep = niters.at(3);

	const double Bsigma = sv_hyperpara.at(0);
	const double a_phi = sv_hyperpara.at(1);
	const double b_phi = sv_hyperpara.at(2);
	const double bMuSv = sv_hyperpara.at(3);
	const double BMuSv = sv_hyperpara.at(4);

	// nburnin += 1;
	// const unsigned int B = nburnin + niter;
	// const unsigned int nkeep = (B-nburnin)/nthin;
	// const unsigned int N = Y.n_rows;
	// const unsigned int G = group.n_cols;
	// const unsigned int Tall = Y.n_cols;
	// const unsigned int S = Y.n_slices;
	// const unsigned int M = X.n_cols;

	// const double CAPVAL = 1e-9;

	XPtr<BigMatrix> Y_xp(Y_pb);
	arma::Cube<double> Y((double*)Y_xp->matrix(),N,Tall,S,false,true);

	if (d_sigma == 0) {
		// d_sigma = 1/mean(apply(Y,c(1,3),var))*(c_sigma-1)
		arma::mat Yvar(N,S,arma::fill::zeros);
		for (unsigned int s=0; s<S; s++) {
			for (unsigned int n=0; n<N; n++) {
				Yvar.at(n,s) = arma::var(Y.slice(s).row(n),0);
			}
		}

		d_sigma = 1./arma::mean(arma::vectorise(Yvar)) * (c_sigma-1);
	}

	arma::mat SecTime;
	if (debug){ SecTime.set_size(B+1,6); }
	double start_time;

	/* BEGIN -- Initialize index and temporal parameters */
	const double CAPVAL = 1e-9;
	const arma::mat Ik(K,K,arma::fill::eye);
	const double normvar_prior2 = std::exp(bMuSv);
	// const double BMuObsInv = 1./BMuObs;
	const double bInv_x = 1./behav_hyperpara.at(1);
	const double cNew_x = behav_hyperpara.at(2) + 0.5*S;
	const double cpost_lambda2 = ng_hyperpara.at(1) + ng_hyperpara.at(0)*K;
	double runtime = 0.0;

	const bool centered_baseline = parameterization % 2; 
  	const double c0 = 2.5;
  	const double C0 = 1.5*Bsigma;
	const bool truncnormal = false; // default=false
	const double MHcontrol = -1;
	const double priorlatent0 = -1;
	unsigned int kgs = K*G*S;

	// XD: (G*S) x S design matrix mask
	arma::mat XD(G*S,S,arma::fill::zeros);
	for (unsigned int g=0; g<G; g++) {
		XD.rows(S*g,S*(g+1)-1).fill(Design.at(g));
	}
	/* END -- Initialize index and temporal parameters */

	arma::uvec gid(Tall,arma::fill::zeros);
	arma::vec cTg(G); cTg.fill(-10000.);
	arma::uvec Tg(G);
	arma::field<arma::uvec> idx(G);
	for (unsigned int t=0; t<Tall; t++) {
		gid(t) = arma::as_scalar(arma::find(group.row(t)==1));
	}
	for (unsigned int g=0; g<G; g++) {
		idx.at(g) = arma::find(group.col(g)==1);
		Tg(g) = idx.at(g).n_elem;

  		if (Gammaprior) {  
    		if (MHsteps == 2 || MHsteps == 3) cTg.at(g) = static_cast<double>(Tg.at(g))/2.; // we want IG(-.5,0) as proposal
    		else if (MHsteps == 1) cTg.at(g) = (static_cast<double>(Tg.at(g))-1.)/2.; // we want IG(-.5,0) as proposal
  		}
  		else {
    		if (MHsteps == 2) cTg.at(g) = c0 + (static_cast<double>(Tg.at(g))+1)/2.;  // pre-calculation outside the loop
    		else ::Rf_error("This setup is not yet implemented");
  		}
  	}

	/* BEGIN -- Initialize input */
	// arma::cube bMuObs(N,G,S,arma::fill::zeros);
	/* END -- Initialize input */


	/* BEGIN -- Initialize output storage */
	// Single subject level - only store the last sample
	XPtr<BigMatrix> ModLogLike_xp(ModLogLike_pb);
	arma::Mat<double> ModLogLike((double*)ModLogLike_xp->matrix(),S*G,nkeep,false,true);

	XPtr<BigMatrix> Pi_xp(Pi_pb);
	arma::Mat<double> Pi((double*)Pi_xp->matrix(),N*K,nkeep,false,true);

	XPtr<BigMatrix> XBeta_xp(XBeta_pb);
	arma::Mat<double> XBeta((double*)XBeta_xp->matrix(),K*M,nkeep,false,true);

	// XPtr<BigMatrix> XMu_xp(XMu_pb);
	// arma::Mat<double> XMu((double*)XMu_xp->matrix(),M,nkeep,false,true);

	// XPtr<BigMatrix> XVarInv_xp(XVarInv_pb);
	// arma::Mat<double> XVarInv((double*)XVarInv_xp->matrix(),M,nkeep,false,true);

	// arma::cube ModLogLike(S,G,nkeep,arma::fill::zeros); // TODO: external storage
	arma::mat ModLogLikeTmp(S,G,arma::fill::zeros);

	// arma::cube MuObsTmp, 
	arma::cube ErrG,tau2;
	// arma::mat MuObsStats, 
	arma::mat VarObsStats, FacStats, LambdaStats;
	arma::mat tau2Tmp,lambda2;
	arma::vec lambda2Tmp;

	std::vector<double> pctVec = {(1-ci_coverage)*0.5,0.5,0.5*(1+ci_coverage)};
	arma::vec pct(pctVec); pct = arma::sort(pct,"ascend");
	std::vector<stmpct::gk<double>> LambdaPct(N*K*S,
		stmpct::gk<double>(0.1));

	if (FactorLevel) {
		ErrG.set_size(Tall,N,S); // hundres of Mb
		ErrG.fill(0.0);

		// FacTmp.set_size(K,Tall,S); 
		// FacTmp.fill(0.0);

		// LambdaTmp.set_size(N,K,S); 
		// LambdaTmp.fill(0.0);

		// ZLambda.set_size(N,K,S);
		// ZLambda.fill(0.0);

		// VarObsTmp.set_size(N,G,S);
		// VarObsTmp.fill(1./(c_sigma*d_sigma)); 

		FacStats.set_size(K*Tall*S,3);
		LambdaStats.set_size(N*K*S,6);
		VarObsStats.set_size(N*G*S,3);

		// if (EstimateObsMu) {
		// 	if (bMuObsIn.isNotNull()) {
		// 		Rcpp::NumericVector bMuObsRcpp(bMuObsIn);
		// 		if (bMuObsRcpp.size()!= N*G*S) {
		// 			::Rf_error("Dimension of bMuObs is NxGxS");
		// 		}
		// 		bMuObsRcpp.attr("dim") = Rcpp::Dimension(N,G,S);
		// 		bMuObs = Rcpp::as<arma::cube>(bMuObsRcpp);
		// 	} 
		// 	MuObsTmp = bMuObs; 
		// 	MuObsStats.set_size(N*G*S,2);
		// }

			// PiTmp.set_size(N,K);
			// PiTmp.fill(0.5); 
			// PiTmp = PiTmp%arma::conv_to<arma::mat>::from(mask);

			// if (BehavioralResponse) {
			// 	XBetaTmp.set_size(K,M); XBetaTmp.fill(0.);
			// 	XMuTmp.set_size(M); XMuTmp.fill(0.);
			// 	XVarInvTmp.set_size(M); XVarInvTmp.fill(0.);
			// }

		if (NormVarLambdaNK==1) {
			tau2.set_size(K,S,nkeep); 
			tau2Tmp.set_size(K,S);
			tau2Tmp.fill(1.0);// var of kth col factor loadings

			lambda2.set_size(S,nkeep);
			lambda2Tmp.set_size(S);
			lambda2Tmp.fill(1.0);
		}	
	} else {
		K = N;
		// FacTmp = Y;
	}

	// arma::cube hMuSub;
	// arma::mat hMuSubTmp;
	// if (HierarchyMuSv) {
	// 	hMuSub.set_size(K,S,nkeep);
	// 	hMuSubTmp.set_size(K,S);
	// 	hMuSubTmp.fill(bMuSv);
	// }

	XPtr<BigMatrix> hMu_xp(hMu_pb);
	arma::Mat<double> hMu((double*) hMu_xp->matrix(), 
		kgs, nkeep, false, true);

	XPtr<BigMatrix> hPhi_xp(hPhi_pb);
	arma::Mat<double> hPhi((double*) hPhi_xp->matrix(), 
		kgs, nkeep, false, true);

	XPtr<BigMatrix> hSigma_xp(hSigma_pb);
	arma::Mat<double> hSigma((double*) hSigma_xp->matrix(), 
		kgs, nkeep, false, true);

	// // arma::cube hMu(K,G*S,nkeep);
	// arma::mat hMuTmp(K,G*S); 
	// hMuTmp.fill(bMuSv);

	// // arma::cube hPhi(K,G*S,nkeep); 
	// arma::mat hPhiTmp(K,G*S);
	// hPhiTmp.fill(2.*a_phi/(a_phi+b_phi)-1.);

	// // arma::cube hSigma(K,G*S,nkeep); 
	// arma::mat hSigmaTmp(K,G*S);
	// hSigmaTmp.fill(std::sqrt(Bsigma));

	// arma::cube hTmp(K,Tall,S); hTmp.fill(bMuSv); // sv
	// if (!centered_baseline) hTmp = (hTmp-bMuSv) / std::sqrt(Bsigma);
	// arma::cube h0(K,G,S); h0.fill(bMuSv);
	// if (!centered_baseline) h0 = (h0-bMuSv) / std::sqrt(Bsigma);
	arma::mat hStats(K*Tall*S,3,arma::fill::zeros);
	bool saveiter;
	/* END -- Initialize output storage */

	/* BEGIN - MCMC chain */

	#ifdef _OPENMP
	start_time = omp_get_wtime();
	if (debug) {
		SecTime.at(0,0) = start_time;
	}
	double start_time_all = omp_get_wtime();
	#endif
	for (unsigned int b=1; b<B; b++) {
		saveiter = b > (nburnin-1) && ((b-nburnin)%nthin==0);

		if (FactorLevel) {
			#ifdef _OPENMP
			if (debug) { start_time = omp_get_wtime(); }
			#pragma omp parallel for shared(d_sigma,cTg,Tg,idx,ErrG,Y,LambdaTmp,FacTmp,VarObsTmp) default(none) schedule(auto)
			#endif
			for (unsigned int s=0; s<S; s++) { // VarObsTmp: N x G x S
				// Loop: S-G-N
				arma::uvec idx_one(1);
				// double c_post;
				ErrG.slice(s) = (Y.slice(s) - LambdaTmp.slice(s)*FacTmp.slice(s)).t(); // Tall x N

				for (unsigned int g=0; g<G; g++){
					arma::uvec idxtmp = idx.at(g);
					unsigned int T = Tg.at(g);
					// if (EstimateObsMu) {
					// 	for (unsigned int n=0; n<N; n++) {
					// 		idx_one.at(0) = n;
					// 		// 1. Mean for observed variables: `update_observed_mean`
					// 		// Loop: S-G-N, O(S*G*N)
					// 		double post_var = 1. / (BMuObsInv+T/VarObsTmp.at(n,idx_gs));
					// 		double post_mean = post_var * (arma::accu(ErrG.slice(s).submat(idxtmp,idx_one))/VarObsTmp.at(n,idx_gs)+bMuObs.at(n,g,s)*BMuObsInv);
					// 		MuObsTmp.at(n,g,s) = R::rnorm(post_mean,std::sqrt(post_var));
					// 		ErrG.slice(s).submat(idxtmp,idx_one) -= MuObsTmp.at(n,g,s);
					// 	}
					// } // Observed Mean

					double ag, bg,cg;
					if (IsotropicObsVar) {
						ag = c_sigma+0.5*N*T;
						bg = 1./(d_sigma+0.5*arma::trace(ErrG.slice(s).rows(idxtmp)*ErrG.slice(s).rows(idxtmp).t()));
						cg = R::rgamma(ag,bg);
						VarObsTmp.slice(s).col(g).fill(1./cg);
					} else { // Idiosyncratic Observed Variance
						ag = c_sigma+0.5*T;
						for (unsigned int n=0; n<N; n++) {
							// 2. Variance for observed variables: `update_observed_var`
							// Loop: S-G-N, O(S*G*N)
							bg = 1.0/(d_sigma+0.5*std::pow(arma::norm(ErrG.slice(s).submat(idxtmp,idx_one)),2.));
							cg = R::rgamma(ag, bg);
							VarObsTmp.at(n,g,s) = 1.0/cg;
						}
					}
				
				}
			}

			// Rcout << "\nSample VarObs" << std::endl;


			#ifdef _OPENMP
			if (debug) {
				SecTime.at(b,0) = omp_get_wtime()-start_time;
				start_time = omp_get_wtime();
			}
			#pragma omp parallel for shared(K,ErrG,idx,Y,mask,LambdaTmp,FacTmp,VarObsTmp,tau2Tmp,PiTmp,ZLambda,hMuTmp) default(none) collapse(2) schedule(auto)
			#endif
			for (unsigned int s=0; s<S; s++) {
				for (unsigned int k=0; k<K; k++) { //parallel
					double logodd_prior = 0.;
					double mk = 1.;
					double beta = 1.;
					double normvar_prior = 10.; // weakly informative prior
					arma::uvec idx_one(1); idx_one.at(0) = k;
					switch (NormVarLambdaNK) {
						case 0: // exp(hMuBase),hMu(K,G*S,B)
							normvar_prior = std::exp(hMuTmp.at(k,G*s));
							break;
						case 1: // NG prior
							normvar_prior = tau2Tmp.at(k,s);
							break;
						case 2: // exp(bMuSv)
							normvar_prior = normvar_prior2;
							break;
					} 
					normvar_prior = 1./normvar_prior;

					// unsigned int colss = G*s;

					for (unsigned int n=0; n<N; n++) {	
						if (MultiSubject) {
							logodd_prior = std::log(PiTmp.at(n,k)/(1.-PiTmp.at(n,k)));
						} else {
							mk = arma::accu(ZLambda.slice(s).col(k))-ZLambda.at(n,k,s);
							logodd_prior = std::log(mk/(beta+N-1.-mk));
							if (mk<=0. && k!=n) 
							{
								LambdaTmp.at(n,k,s) = 0.;
								ZLambda.at(n,k,s) = 0.;
								continue;
							}
						}
					
						if (mask.at(n,k) != 0 ) {// Spike-and-slab prior
							arma::vec Err = ErrG.slice(s).col(n)+LambdaTmp.at(n,k,s)*FacTmp.slice(s).row(k).t();
							double post_prec = normvar_prior;
							double post_mean = 0.;
							for(unsigned int g=0; g<G; g++) {
								post_prec += 1./VarObsTmp.at(n,g,s)*std::pow(arma::norm(FacTmp.slice(s).submat(idx_one,idx.at(g))),2.);
								post_mean += 1./VarObsTmp.at(n,g,s)*arma::dot(FacTmp.slice(s).submat(idx_one,idx.at(g)),Err.elem(idx.at(g)));
							}

							if (post_prec < 1e-9) post_prec = 1e-9;
							post_mean /= post_prec;

							if (mask.at(n,k) == 1) {
								if (fixdiag) {
									LambdaTmp.at(n,k,s) = 1.;
								} else {
								// diagonal - nonnegative
									double post_std = std::pow(post_prec,-0.5);
									LambdaTmp.at(n,k,s) = r_truncnorm(post_mean, 
										post_std,0.,2.*post_std);
								}
		
								ZLambda.at(n,k,s) = 1.;
							} else { 
								// mask_nk == 2
								// off diagonal
								double prob = logodd_prior+0.5*(post_prec*std::pow(post_mean,2.)
									-std::log(post_prec)+std::log(normvar_prior)); // likelihood
								prob = 1./(1.+std::exp(-prob));

								if (!std::isfinite(prob)) {
									::Rf_error("update_Lambda_nk: not finite posterior probability.");
								}

								ZLambda.at(n,k,s) = (arma::randu(1)[0]<prob?1.:0.);
								if (ZLambda.at(n,k,s)==1.) {
									LambdaTmp.at(n,k,s) = post_mean + std::pow(post_prec,-0.5)*arma::randn(1)[0];
								}

							}
						}
					}
				}
			}

			// Rcout << "Sample Lambda" << std::endl;


			#ifdef _OPENMP
			if (debug) {
				SecTime.at(b,1) = omp_get_wtime()-start_time;
				start_time = omp_get_wtime();
			}
			#pragma omp parallel for shared(K,Y,gid,LambdaTmp,FacTmp,VarObsTmp,hTmp) default(none) collapse(2) schedule(auto)
			#endif
			for (unsigned int s=0; s<S; s++) {
				for (unsigned int t=0; t<Tall; t++) {
					arma::sp_mat SigmaInv(arma::diagmat(1./VarObsTmp.slice(s).col(gid.at(t)))); //N x N
					arma::mat PrecArma = arma::symmatu(LambdaTmp.slice(s).t()*SigmaInv*LambdaTmp.slice(s)); // K*K
					PrecArma.diag() += arma::exp(-hTmp.slice(s).col(t));
					arma::mat CholInv;
					int rk = K;
					try {
						CholInv = arma::chol(PrecArma);
						CholInv = arma::solve(arma::trimatu(CholInv),Ik,arma::solve_opts::fast); // K x K
					} catch (std::exception& e) {
						::Rf_warning(e.what());
						arma::vec eigval;
						arma::mat eigvec;
						arma::eig_sym(eigval,eigvec,PrecArma);
						arma::uvec iidx = arma::find(eigval>0.);
						rk = iidx.n_elem;
						CholInv = eigvec.cols(iidx) * arma::diagmat(1./arma::sqrt(eigval.elem(iidx)));
					}

					FacTmp.slice(s).col(t) = CholInv * (CholInv.t()*LambdaTmp.slice(s).t()*SigmaInv*Y.slice(s).col(t) 
						+arma::randn(rk,1));
				}
			}

			// Rcout << "Sample Factor" << std::endl;
		}

		
		#ifdef _OPENMP
		if (debug) {
			SecTime.at(b,2) = omp_get_wtime()-start_time;
			start_time = omp_get_wtime();
		}
		#pragma omp parallel for shared(K,mix_pre,mix_mean,mix_2varinv,mix_varinv,Tg,cTg,idx,hMuTmp,hPhiTmp,hSigmaTmp,hTmp,h0,FacTmp,r) default(none) collapse(3) schedule(auto)
		#endif
		for (unsigned int s=0; s<S; s++) {
			for (unsigned int g=0; g<G; g++) {
				for (unsigned int k=0; k<K; k++) {
					unsigned int T = Tg.at(g);
					double cT = cTg.at(g);
					arma::uvec idx_one(1);
					arma::uvec idxg = idx.at(g);
					unsigned int idx_gs  = G*s+g;
					unsigned int idx_kgs = k + K*(idx_gs);
				
					arma::vec omega_diag(T+1);  // contains diagonal elements of precision matrix
  					double omega_offdiag;  // contains off-diag element of precision matrix (const)
  					arma::vec chol_offdiag(T), chol_diag(T+1);  // Cholesky-factor of Omega
  					arma::vec covector(T+1);  // holds covector (see McCausland et al. 2011)
  					arma::vec htmp(T+1),hnew(T+1);  // intermediate vector for sampling h
  					// arma::ivec r(T);
					idx_one.at(0) = k;
					arma::vec curpara = {hMuTmp.at(k,idx_gs),
						hPhiTmp.at(k,idx_gs),
						hSigmaTmp.at(k,idx_gs)};

					if (dontupdatemu) curpara.at(0) = bMuSv;
					double sigma2inv = std::pow(curpara.at(2),-2);
					double phi2tmp = curpara.at(1)*curpara.at(1);
					double Bh0inv = 1./priorlatent0;
					if (priorlatent0 < 0) Bh0inv = 1.-phi2tmp;
					arma::vec innov = arma::randu(T); 

					double h0tmp = h0.at(k,g,s);
					arma::vec h = hTmp.slice(s).submat(idx_one,idxg).t();
					arma::vec data = arma::square(FacTmp.slice(s).submat(idx_one,idxg)).t();
					if (arma::any(data==0.)) {
						data += std::max(arma::stddev(data,0)/10000.,1e-16);
					}
					data = arma::log(data);
					if (centered_baseline) {
						data -= h; // Th x 1
					} else {
						data -= curpara.at(0)+curpara.at(2)*h;
					}

					for (unsigned int t=0; t<T; t++) {
						arma::vec mixp(10); mixp.fill(0.0);
      					mixp.at(0) = std::exp(mix_pre[0]-(data.at(t)-mix_mean[0])
      						*(data.at(t)-mix_mean[0])*mix_2varinv[0]);
      					for (int r = 1; r < 10; r++) {
        					mixp.at(r) = mixp.at(r-1) + std::exp(mix_pre[r]-(data.at(t)-mix_mean[r])
        						*(data.at(t)-mix_mean[r])*mix_2varinv[r]);
      					}

      					unsigned int index = (10-1)/2;  // start searching in the middle
      					double temp = innov.at(t)*mixp.at(9);  // current (non-normalized) value
      					bool larger = false;  // indicates that we already went up
      					bool smaller = false; // indicates that we already went down
      					unsigned int rcnt = 0;
      					while(rcnt < 20) {
        					if (temp > mixp(index)) {
          						if (smaller == true) {
            						index++;
            						break;
          						}
          						else {
            						index++;
            						larger = true;
          						}
        					}
        					else {
          						if (larger == true) {
            						break;
          						}
          						else {
            						if (index == 0) {
              							break;
            						}
            						else {
              							index--;
              							smaller = true;
            						}
          						} 
        					}
        					rcnt ++;
      					}
      					r.at(t,idx_kgs) = index;
					}
					
					if (centered_baseline) { // fill precision matrix omega and covector c for CENTERED para:
      					data += h; // T x 1

      					omega_diag.at(0) = (Bh0inv + phi2tmp) * sigma2inv;
      					covector.at(0) = curpara.at(0) * (Bh0inv-curpara.at(1)+phi2tmp) * sigma2inv;

      					for (unsigned int t = 1; t < T; t++) {
        					omega_diag.at(t) = mix_varinv[r.at(t-1,idx_kgs)] + (1+phi2tmp)*sigma2inv; 
        					covector.at(t) = (data.at(t-1) 
        						- mix_mean[r.at(t-1,idx_kgs)])*mix_varinv[r.at(t-1,idx_kgs)]
          						+ curpara.at(0)*(1-curpara.at(1))*(1-curpara.at(1))*sigma2inv;
      					}
      					omega_diag.at(T) = mix_varinv[r.at(T-1,idx_kgs)] + sigma2inv;
      					covector.at(T) = (data.at(T-1) 
      						- mix_mean[r.at(T-1,idx_kgs)])*mix_varinv[r.at(T-1,idx_kgs)]
        					+ curpara.at(0)*(1-curpara.at(1))*sigma2inv;
      					omega_offdiag = -curpara.at(1)*sigma2inv;  // omega_offdiag is constant

    				} else { // fill precision matrix omega and covector c for NONCENTERED para:
     		 			data += curpara.at(0) + curpara.at(2)*h; // T x 1

      					double sigmainvtmp = std::sqrt(sigma2inv);
      					omega_diag.at(0) = phi2tmp + Bh0inv;
      					covector.at(0) = 0.;

      					for (unsigned int t = 1; t < T; t++) {
        					omega_diag.at(t) = mix_varinv[r.at(t-1,idx_kgs)]/sigma2inv + 1 + phi2tmp; 
        					covector.at(t) = mix_varinv[r.at(t-1,idx_kgs)]/sigmainvtmp*(data.at(t-1) 
        						- mix_mean[r.at(t-1,idx_kgs)] - curpara.at(0));
      					}
      					omega_diag.at(T) = mix_varinv[r.at(T-1,idx_kgs)]/sigma2inv + 1;
      					covector.at(T) = mix_varinv[r.at(T-1,idx_kgs)]/sigmainvtmp*(data.at(T-1) 
      						- mix_mean[r.at(T-1,idx_kgs)] - curpara.at(0));
      					omega_offdiag = -curpara.at(1);  // omega_offdiag is constant
    				} 

    				cholTridiag(omega_diag, omega_offdiag, 
    					chol_diag, chol_offdiag);
    				forwardAlg(chol_diag, chol_offdiag, covector, htmp);
    				htmp += arma::randn(T+1); // T+1
    				backwardAlg(chol_diag, chol_offdiag, htmp, hnew);    				
    				h = hnew.tail(T);
    				h0tmp = hnew.at(0);

  					/*
   					* Step (b): sample mu, phi, sigma
   					*/

    				if (centered_baseline) {  // this means we have C as base
      					regressionCentered(h0tmp, h, 
        					curpara.at(0), curpara.at(1), 
        					curpara.at(2), C0, cT, Bsigma, 
        					a_phi, b_phi, bMuSv, BMuSv, B011inv, 
        					B022inv, Gammaprior, truncnormal, 
        					MHcontrol, MHsteps, dontupdatemu, 
        					priorlatent0);

      					if (parameterization == 3) {  
      						// this means we should interweave
        					h -= curpara.at(0); 
        					h /= curpara.at(2);
        					h0tmp -= curpara.at(0); 
        					h0tmp /= curpara.at(2);
        					regressionNoncentered(data, 
        						h0tmp, h, r.col(idx_kgs), curpara.at(0), 
        						curpara.at(1), curpara.at(2), 
          						Bsigma, a_phi, b_phi, bMuSv, 
          						BMuSv, truncnormal, MHsteps, 
          						dontupdatemu, priorlatent0);
        					h *= curpara.at(2); 
        					h += curpara.at(0);
        					h0tmp *= curpara.at(2); 
        					h0tmp += curpara.at(0);
      					}
    				} else {  // NC as base
      					regressionNoncentered(data, 
      						h0tmp, h, r.col(idx_kgs), curpara.at(0), 
      						curpara.at(1), curpara.at(2),
        					Bsigma, a_phi, b_phi, bMuSv, 
        					BMuSv, truncnormal, MHsteps, 
        					dontupdatemu, priorlatent0);

      					if (parameterization == 4) { 
      						// this means we should interweave
        					h *= curpara.at(2); 
        					h += curpara.at(0);
        					h0tmp *= curpara.at(2); 
        					h0tmp += curpara.at(0);
        					regressionCentered(h0tmp, h, 
          						curpara.at(0), curpara.at(1), 
          						curpara.at(2), C0, cT, 
          						Bsigma, a_phi, b_phi, bMuSv, BMuSv, 
          						B011inv, B022inv, Gammaprior, 
          						truncnormal, MHcontrol, MHsteps, 
          						dontupdatemu, priorlatent0);

        					h -= curpara.at(0); 
        					h /= curpara.at(2);
        					h0tmp -= curpara.at(0); 
        					h0tmp /= curpara.at(2);
      					}
    				}

					hMuTmp.at(k,idx_gs) = curpara.at(0);
					hPhiTmp.at(k,idx_gs) = curpara.at(1);
					hSigmaTmp.at(k,idx_gs) = curpara.at(2);
					hTmp.slice(s).submat(idx_one,idxg) = h.t();
					h0.at(k,g,s) = h0tmp; 
				}
			}

			// idx_s_from = (unsigned int)(s*G);
			// idx_s_to = (unsigned int)(s*G+G-1);
			// // 6. Individual based mean and var for {hMu_sg, all g}
			// // 	  - `update_Mu_ind`
			// hMu.slice(b).col(s) = update_Mu_ind(
			// 	hMu_g.slice(b).cols(idx_s_from,idx_s_to), 
			// 	hMuVar.slice(b-1).col(s), b_mu, B_mu);
			// //	  - `update_Var_ind`
			// hMuVar.slice(b).col(s) = update_Var_ind(
			// 	hMu_g.slice(b).cols(idx_s_from,idx_s_to), 
			// 	hMu.slice(b).col(s),c_sigma, d_sigma);		
		
		}

		// Rcout << "Sample svpara and h" << std::endl;

		#ifdef _OPENMP
		if (debug) {
			SecTime.at(b,3) = omp_get_wtime()-start_time;
			start_time = omp_get_wtime();
		}
		#endif
		if (FactorLevel) {
			if (NormVarLambdaNK==1) {
				// Individual NG shrinkage on factor loading
				// Loop: S-K
				for (unsigned int s=0; s<S; s++) {
					for (unsigned int k=0; k<K; k++) {
						arma::vec temp = arma::nonzeros(LambdaTmp.slice(s).col(k));
						double lambda = ng_hyperpara.at(0) - 0.5*temp.n_elem;
						double chi = std::pow(arma::norm(temp),2.);
						double psi = ng_hyperpara.at(0)*lambda2Tmp.at(s);
						if (chi > 1e-9 && psi > 1e-9) {
							tau2Tmp.at(k,s) = do_rgig(lambda,chi,psi);
							tau2Tmp.at(k,s) = (tau2Tmp.at(k,s)<CAPVAL)?CAPVAL:tau2Tmp.at(k,s);
						}
					}
					lambda2Tmp.at(s) = std::max(R::rgamma(cpost_lambda2,
						1./(ng_hyperpara.at(2) + (ng_hyperpara.at(0)/2.)*arma::accu(tau2Tmp.col(s)))),CAPVAL);
				}
			}


			if (MultiSubject) {
				// 8. Group inclusion probability
				arma::umat Zind(N,K,arma::fill::zeros);
				for (unsigned int s=0; s<S; s++) {
					Zind += (arma::abs(LambdaTmp.slice(s)) > CAPVAL);
				}

				#ifdef _OPENMP
				#pragma omp parallel for shared(K,mask,PiTmp,aPiProb,bPiProb,Zind) default(none) collapse(2) schedule(auto)
				#endif
				for (unsigned int k=0; k<K; k++) {
					for (unsigned int n=0; n<N; n++) {
						if (mask.at(n,k)>0) {
							PiTmp.at(n,k) = R::rbeta(aPiProb.at(n,k)+Zind.at(n,k),
								bPiProb.at(n,k)+S-Zind.at(n,k));
						} else {
							PiTmp.at(n,k) = 0.0;
						}
					}
				}

				// Rcout << "Sample Pi" << std::endl;

				if (BehavioralResponse) {
					// 9. Behavioral/Clinical Responses
					// X: S x M, M behavioral measures for S subjects
					// Design: G x 1 mask for design matrix
					// XD: (G*S) x S design matrix mask
					// XBetaTmp: K x M
					// XMuTmp,XVarInvTmp: M x 1
					// hMuTmp: K x (G*S)
					// hyperparameters: a_x, bInv_x; cNew_x, d_x

					// 9.0 Design matrix
					arma::mat XX = hMuTmp * XD; // K x S
					arma::inplace_trans(XX); // S x K

					for (unsigned int m=0; m<M; m++) {
						// 9.1 Mean of Behavioral Responses
						// double bNew = 1. / (bInv_x+S*XVarInvTmp.at(m));
						// double aNew = (behav_hyperpara.at(0)*bInv_x + arma::accu(X.col(m)-XX*XBetaTmp.col(m))*XVarInvTmp.at(m)) * bNew;
						// XMuTmp.at(m) = R::rnorm(aNew,std::sqrt(bNew));

						// 9.2 Regression coefficients of Behavioral Responses
						// Set XMuTmp == 0, XVarInvTmp == 1
						// arma::vec ErrNew = X.col(m) - XMuTmp.at(m); // S x 1
						// arma::vec MuNew = XX.t()*ErrNew*XVarInvTmp.at(m) + behav_hyperpara.at(0)*bInv_x; // K x 1
						arma::vec MuNew = XX.t()*X.col(m) + behav_hyperpara.at(0)*bInv_x; // K x 1
						// arma::mat PrecNew = XX.t() * XX * XVarInvTmp.at(m); // K x K
						arma::mat PrecNew = XX.t() * XX; // K x K
						PrecNew.diag() += bInv_x;
						arma::mat CholNew = arma::chol(PrecNew); // K x K
						CholNew = arma::solve(arma::trimatu(CholNew),Ik,arma::solve_opts::fast);
						XBetaTmp.col(m) = CholNew*CholNew.t()*MuNew + CholNew*arma::randn(K,1);

						// 9.3 Variance of Behavioral Responses
						// ErrNew -= XX*XBetaTmp.col(m);
						// XVarInvTmp.at(m) = R::rgamma(cNew_x,
						// 	1./(behav_hyperpara.at(3)+0.5*arma::accu(arma::square(ErrNew))));

					}
				} // end of BehavioralResponse

				// Rcout << "Sample Behavioral Regression" << std::endl;
			} // end of MultiSubject

			#ifdef _OPENMP
			#pragma omp parallel for shared(ModLogLikeTmp,Y,idx,LambdaTmp,FacTmp,VarObsTmp) default(none) collapse(2) schedule(auto)
			#endif
			for (unsigned int s=0; s<S; s++) {
				for (unsigned int g=0; g<G; g++) {
					ModLogLikeTmp.at(s,g) = loglike_para(Y.slice(s).cols(idx.at(g)), // N x Tg
						LambdaTmp.slice(s), // N x K
						FacTmp.slice(s).cols(idx.at(g)), // K x Tg
						VarObsTmp.slice(s).col(g)); // N x 1
				}
			}

			// Rcout << "Calculate likelihood" << std::endl;

		}


		#ifdef _OPENMP
		if (debug) {
			SecTime.at(b,4) = omp_get_wtime()-start_time;
			start_time = omp_get_wtime();
		}
		#endif
		if (saveiter || b==(B-1)) {
			unsigned int idx_run;
			if (saveiter){
				idx_run = (b-nburnin)/nthin;
			} else {
				idx_run = nkeep - 1;
			}

			hMu.col(idx_run) = hMuTmp.as_col();
			hPhi.col(idx_run) = hPhiTmp.as_col();
			hSigma.col(idx_run) = hSigmaTmp.as_col();

			// hStats: (K*Tall*S) x 3
			update_meanvar(hStats,hTmp,idx_run);
			hStats.col(2) = arma::vectorise(hTmp);

			if (FactorLevel) {
				ModLogLike.col(idx_run) = ModLogLikeTmp.as_col();

				if (MultiSubject) {
					Pi.col(idx_run) = PiTmp.as_col();
					if (BehavioralResponse) {
						XBeta.col(idx_run) = XBetaTmp.as_col();
						// XMu.col(idx_run) = XMuTmp;
						// XVarInv.col(idx_run) = XVarInvTmp;
					}
				}

				if (NormVarLambdaNK==1) {
					tau2.slice(idx_run) = tau2Tmp;
					lambda2.col(idx_run) = lambda2Tmp;
				}

				// if (EstimateObsMu) {
				// 	update_meanvar(MuObsStats,MuObsTmp,idx_run);
				// }

				update_meanvar(FacStats,FacTmp,idx_run);
				FacStats.col(2) = arma::vectorise(FacTmp);

				// LambdaStats: (N*K*S) x 6
				update_meanvar(LambdaStats,LambdaTmp,idx_run);
				update_quantile(LambdaTmp,LambdaPct);
				LambdaStats.col(5) = arma::vectorise(LambdaTmp);

				// VarObsStats: (N*G*S) x 3
				update_meanvar(VarObsStats,VarObsTmp,idx_run);
				VarObsStats.col(2) = arma::vectorise(VarObsTmp);
			}
		}

		// Rcout << "Store samples" << std::endl;
		#ifdef _OPENMP
		if (debug) {
			SecTime.at(b,5) = omp_get_wtime()-start_time;
		}
		#endif
		
		Rcout << "\rProgress: " << b << "/" << B-1;
		
	}

	Rcout << std::endl;
	// Rcout << "\rProgress: " << B << "/" << B << std::endl;
	Rcpp::List output;

	#ifdef _OPENMP
	start_time = omp_get_wtime();
	runtime = start_time - start_time_all;
	Rcout << "Elapsed time " << runtime << "s.\n";
	output["runtime"] = runtime;
	#endif

	output["h0"] = Rcpp::wrap(h0);
	output["r"] = Rcpp::wrap(r);
	output["ZLambda"] = Rcpp::wrap(ZLambda);

	// output["loglike"] = Rcpp::wrap(ModLogLike);

	// Fix label switching a posteriori
	// if (!debug && signswitch) {
	// 	if (verbose) {
	// 		Rcout << "Proprocessing label switching.\n";
	// 	}
	// 	label_switching(LambdaTmp,FacTmp,S,N,K,Tall,idx_keep);		
	// 	for (unsigned int i=0; i<idx_keep.n_elem; i++) {
	// 		update_meanvar(FacStats,FacTmp.at(idx_keep.at(i)),i);
	// 		update_meanvar(LambdaStats,LambdaTmp.at(idx_keep.at(i)), i);
	// 	}
	// }
	// calculate std

	int normval = nkeep - 1;

	{
		VarObsStats.col(1) = arma::sqrt(VarObsStats.col(1)/normval);
		Rcpp::NumericVector VarObsStats_rcpp = Rcpp::wrap(VarObsStats);
		VarObsStats_rcpp.attr("dim") = Rcpp::IntegerVector::create(N,G,S,3);
		output["VarObs"]=VarObsStats_rcpp;
	}

	if (FactorLevel) {
		if (NormVarLambdaNK==1) {
				// output["lambda2"] = Rcpp::wrap(lambda2);
				// output["tau2"] = Rcpp::wrap(tau2);
			arma::mat lambda2_stats = MCMC_MeanStd_1D(lambda2);
			arma::cube tau2_stats = MCMC_MeanStd_2D(tau2);
			output["tau2"] = Rcpp::wrap(tau2_stats);
			output["lambda2"] = Rcpp::wrap(lambda2_stats);
		}

		// if (MultiSubject) {
		// 	if (whole_chain) {
		// 		output["Pi"] = Rcpp::wrap(Pi);
		// 	} else {
		// 		arma::cube Pi_stats = MCMC_MeanStd_2D(Pi);
		// 		output["Pi"] = Rcpp::wrap(Pi_stats);
		// 	}
		// }	

		// if (EstimateObsMu) {
		// 	MuObsStats.col(1) = arma::sqrt(MuObsStats.col(1)/normval);
		// 	output["MuObs"]=Rcpp::wrap(MuObsStats);
		// }

		{
			FacStats.col(1) = arma::sqrt(FacStats.col(1)/normval);
			Rcpp::NumericVector FacStats_rcpp = Rcpp::wrap(FacStats);
			FacStats_rcpp.attr("dim") = Rcpp::IntegerVector::create(K,Tall,S,3);
			output["Factor"]=FacStats_rcpp;
		}

		{
			LambdaStats.col(1) = arma::sqrt(LambdaStats.col(1)/normval);
			LambdaStats.cols(2,4) = get_quantile(LambdaPct,pct,N*K*S);
			Rcpp::NumericVector LambdaStats_rcpp = Rcpp::wrap(LambdaStats);
			LambdaStats_rcpp.attr("dim") = Rcpp::IntegerVector::create(N,K,S,6);
			output["Lambda"]=LambdaStats_rcpp;
		}
	}


	// {
	// 	Rcpp::NumericVector hMu_rcpp = Rcpp::wrap(hMu);
	// 	hMu_rcpp.attr("dim") = Rcpp::IntegerVector::create(K,G,S,nkeep);
	// 	output["hMu"] = hMu_rcpp; 
	// }

	// {
	// 	Rcpp::NumericVector hPhi_rcpp = Rcpp::wrap(hPhi);
	// 	hPhi_rcpp.attr("dim") = Rcpp::IntegerVector::create(K,G,S,nkeep);
	// 	output["hPhi"] = hPhi_rcpp; 
	// }

	// {
	// 	Rcpp::NumericVector hSigma_rcpp = Rcpp::wrap(hSigma);
	// 	hSigma_rcpp.attr("dim") = Rcpp::IntegerVector::create(K,G,S,nkeep);
	// 	output["hSigma"] = hSigma_rcpp; 
	// }

	{
		hStats.col(1) = arma::sqrt(hStats.col(1)/normval);
		Rcpp::NumericVector hStats_rcpp = Rcpp::wrap(hStats);
		hStats_rcpp.attr("dim") = Rcpp::IntegerVector::create(K,Tall,S,3);
		output["h"]=hStats_rcpp;
	}

	// arma::cube hMu_keep = hMu.slices(idx_keep); // K x S x nsample
	// arma::cube hMuVar_keep = hMuVar.slices(idx_keep); // K x S x nsample
	// output["hMu"] = Rcpp::wrap(hMu_keep);
	// output["hMuVar"] = Rcpp::wrap(hMuVar_keep);
	/*
	stats: mean, std, lowpct, median, uppct, ness, mcse
	7 in total
	column-wise concatenation

	*_stats: dim1 x dim2 x nstats(=7)
	*/
	
		// // arma::cube hMu_stats(K,S,7,arma::fill::zeros);
		// // hMu_stats.slices(0,1) = MCMC_MeanStd_2D(hMu_keep);
		// // hMu_stats.slices(2,4) = MCMC_Quantile_2D(hMu_keep,pct);
		// // hMu_stats.slice(5) = MCMC_EffectiveSampleSize_2D(hMu_keep,0);
		// // hMu_stats.slice(6) = MCMC_MCSE_2D(hMu_stats.slice(1),
		// // 	hMu_stats.slice(5));

		// // arma::cube hMuVar_stats(K,S,7,arma::fill::zeros);
		// // hMuVar_stats.slices(0,1) = MCMC_MeanStd_2D(hMuVar_keep);
		// // hMuVar_stats.slices(2,4) = MCMC_Quantile_2D(hMuVar_keep,pct);
		// // hMuVar_stats.slice(5) = MCMC_EffectiveSampleSize_2D(hMuVar_keep,0);
		// // hMuVar_stats.slice(6) = MCMC_MCSE_2D(hMuVar_stats.slice(1),
		// // 	hMuVar_stats.slice(5));

		// output["hMu"] = Rcpp::wrap(hMu_stats);
		// output["hMuVar"] = Rcpp::wrap(hMuVar_stats);
		
	#ifdef _OPENMP
	if (debug) {
		SecTime.at(B,0) = omp_get_wtime() - start_time;
		output["SecTime"] = Rcpp::wrap(SecTime);
	}	
	#endif

	return output;
}


//' @export
// [[Rcpp::export]]
Rcpp::List gibbs_reg_sparse(
	const arma::mat &Delta, // S x K, Pointer for storage
	const arma::mat &Z, // S x M
	const unsigned int nburnin = 1000,
	const unsigned int niter = 1000,
	const unsigned int nthin = 1,
	const double a_beta = 1,
	const double b_beta = 1,
	const double s_ig = 0.5,
	const double alpha1_ig = 0.01,
	const double alpha2_ig = 0.01) {

	const unsigned int S = Delta.n_rows;
	const unsigned int K = Delta.n_cols;
	const unsigned int M = Z.n_cols;
	const unsigned int B = nburnin + niter + 1;
	const unsigned int nkeep = (B-niter-1)/nthin;
	bool saveiter;

	const double s2_ig = 0.5*s_ig * s_ig;

	arma::mat theta(M,nkeep+1); theta.fill(0.5);
	arma::mat tau2inv(M,nkeep+1); tau2inv.fill(0.1);
	arma::mat sigma2inv(M,nkeep+1); sigma2inv.fill(0.1);
	arma::cube beta(K,M,nkeep+1,arma::fill::zeros);
	arma::cube pi(K,M,nkeep+1); pi.fill(0.5);

	const arma::mat Ik(K,K,arma::fill::eye);
	const arma::mat DZ = Delta.t()*Z; // K x M

	for (unsigned int b=1; b<B; b++) {
		saveiter = (b>(nburnin-1)) &&((b-nburnin)%nthin==0);

		#ifdef _OPENMP
		#pragma omp parallel for shared(beta,pi,theta,Z,Delta,sigma2inv,tau2inv) default(none) schedule(auto)
		#endif
		for (unsigned int m=0; m<M; m++) {
			arma::vec betatmp = beta.slice(0).col(m); // K x 1
			double pi_sum = arma::accu(pi.slice(0).col(m));
			theta.at(m,0) = R::rbeta(a_beta+pi_sum,
				b_beta+K-pi_sum);

			arma::vec Err = Z.col(m) - Delta*betatmp;
			sigma2inv.at(m,0) = R::rgamma(alpha1_ig+S*0.5,
				1./(alpha2_ig+0.5*arma::accu(Err%Err)));

			tau2inv.at(m,0) = R::rgamma(0.5+0.5*pi_sum,
				1./(s2_ig+0.5*sigma2inv.at(m,0)*arma::accu(betatmp%betatmp)));

			arma::mat prec = (Delta.t()*Delta)*sigma2inv.at(m,0) + Ik*(sigma2inv.at(m,0)*tau2inv.at(m,0));			
			arma::mat chol = arma::chol(arma::symmatu(prec));
			chol = arma::solve(arma::trimatu(chol),Ik);
			arma::vec tmp = arma::randn(K,1);
			beta.slice(0).col(m) = chol*(chol.t()*DZ.col(m)*sigma2inv.at(m,0)+tmp);
		}
		
		#ifdef _OPENMP
		#pragma omp parallel for shared(beta,Z,Delta,tau2inv,theta,sigma2inv,pi) default(none) collapse(2) schedule(auto)
		#endif
		for (unsigned int m=0; m<M; m++) {
			for (unsigned int k=0; k<K; k++) {
				arma::vec betatmp = beta.slice(0).col(m); // K x 1
				betatmp.at(k) = 0;
				arma::vec Err2 = Z.col(m) - Delta*betatmp;
				double cond_var = arma::accu(Delta.col(k)%Delta.col(k))+tau2inv.at(m,0);
				double l1 = std::log(theta.at(m,0));
				l1 += 0.5*std::log(sigma2inv.at(m,0)*tau2inv.at(m,0));
				l1 += 0.5*std::pow(arma::accu(Delta.col(k)%Z.col(m)),2.0)*sigma2inv.at(m,0)/cond_var;
				l1 -= 0.5*std::log(sigma2inv.at(m,0)*cond_var);
				l1 = std::exp(l1);
				pi.at(k,m,0) = R::rbinom(1,l1/(l1+1-theta.at(m,0)));
			}
		}

		if (saveiter) {
			unsigned int idx_run = (b-nburnin)/nthin;
			theta.col(idx_run) = theta.col(0);
			sigma2inv.col(idx_run) = sigma2inv.col(0);
			tau2inv.col(idx_run) = tau2inv.col(0);
			beta.slice(idx_run) = beta.slice(0);
			pi.slice(idx_run) = pi.slice(0);
		}

		Rcout << "\rProgress: " << b << "/" << B-1;
	}

	Rcout << std::endl;
	// Rcout << "\rProgress: " << B << "/" << B << std::endl;
	Rcpp::List output;
	output["theta"] = Rcpp::wrap(theta);
	output["sigma2inv"] = Rcpp::wrap(sigma2inv);
	output["tau2inv"] = Rcpp::wrap(tau2inv);
	output["beta"] = Rcpp::wrap(beta);
	output["pi"] = Rcpp::wrap(pi);

	return output;

}


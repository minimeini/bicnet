#include <RcppArmadillo.h>
#include <omp.h>
#include "sampler.h"
#include "auxmix.h"
#include "progutils.h"
#include "regression.h"
#include "parameterization.h"

using namespace Rcpp;

//[[Rcpp::depends(RcppArmadillo)]]



//[[Rcpp::export]]
arma::mat gibbs_ar(
  const arma::vec& h, // T x 1
  const unsigned int nburnin = 1000,
  const unsigned int niter = 1000,
  const unsigned int nthin = 1,
  const double Bsigma = 0.9, // prior for the sqrt(sigma2) ~ N(0,Bsigma) of SVs
  const double a_phi = 10.0, // prior for (phi+1)/2 ~ Beta(a_phi,b_phi) of SVs
  const double b_phi = 1.0, // prior for (phi+1)/2 ~ Beta(a_phi,b_phi) of SVs
  const double bMuSv = 0.0, // G x S
  const double BMuSv = 3.0,
  const double B011inv = 0.01, // 10^-12
  const double B022inv = 0.01, // 10^-8
  const int MHsteps = 2, // default = 2
  const bool Gammaprior = true) { 

  const unsigned int B = nburnin + niter;
  const unsigned int T = h.n_elem;

  /* BEGIN -- Initialize SV parameters */
  arma::mat svpara(3,B,arma::fill::zeros);//mu,phi,sigma
  svpara.row(2).ones();

  /* BEGIN --- SV Settings */
  const double c0 = 2.5;
  const double C0 = 1.5*Bsigma;
  const bool centered_baseline = true;
  const bool truncnormal = false; // default=false
  const double MHcontrol = -1;
  const double priorlatent0 = -1;
  const bool dontupdatemu = false;
  double cT = -10000; 
  if (Gammaprior) {  
    if (MHsteps == 2 || MHsteps == 3) cT = T/2.0; // we want IG(-.5,0) as proposal
    else if (MHsteps == 1) cT = (T-1)/2.0; // we want IG(-.5,0) as proposal
  }
  else {
    if (MHsteps == 2) cT = c0 + (T+1)/2.0;  // pre-calculation outside the loop
    else Rf_error("This setup is not yet implemented");
  }
  /* END -- Initialize index and temporal parameters */


  /* BEGIN - MCMC chain */
  for (unsigned int b=1; b<B; b++) {
    svpara.col(b) = svpara.col(b-1);
    regressionCentered(h.at(0), h.tail(T-1), 
      svpara.at(0,b), svpara.at(1,b), svpara.at(2,b),
      C0, cT, Bsigma, a_phi, b_phi, bMuSv, 
      BMuSv, B011inv, B022inv, Gammaprior, 
      truncnormal, MHcontrol, MHsteps, 
      dontupdatemu, priorlatent0);
    Rcout << "\rProgress: " << b+1 << "/" << B;
  }

  Rcout << std::endl;

  return svpara;
}


//[[Rcpp::export]]
List svsample_cpp(
    const arma::vec& y,
    const int draws=10000,
    const int burnin=0,
    const double bmu=0,
    const double Bmu=1,
    const double a0=10.,
    const double b0=1.,
    const double Bsigma=0.5,
    const int thin=1,
    const int thintime=1,
    const bool quiet=true,
    const int parameterization=3,
    const int MHsteps=2,
    const double B011inv=0.01,
    const double B022inv=0.01,
    const double MHcontrol=-1,
    const bool Gammaprior=true,
    const bool truncnormal=false,
    const bool dontupdatemu=false,
    const double priorlatent0=-1str()) {

  // arma::vec y(y_in);
  int T = y.size();
  int N = burnin + draws;

  // verbosity control

  // "expert" settings:
  bool centered_baseline = parameterization % 2; // 0 for C, 1 for NC baseline

  // moment-matched IG-prior
  double c0 = 2.5;
  double C0 = 1.5*Bsigma;

  // pre-calculation of a posterior parameter
  double cT = -10000; 
  if (Gammaprior) {  
    if (MHsteps == 2 || MHsteps == 3) cT = T/2.0; // we want IG(-.5,0) as proposal
    else if (MHsteps == 1) cT = (T-1)/2.0; // we want IG(-.5,0) as proposal
  }
  else {
    if (MHsteps == 2) cT = c0 + (T+1)/2.0;  // pre-calculation outside the loop
    else Rf_error("This setup is not yet implemented");
  }

  if (dontupdatemu == true && MHsteps == 1) { // not implemented (would be easy, though)
    Rf_error("This setup is not yet implemented");
  }

  // initialize the variables:
  arma::vec sigma = arma::ones(N+1) * std::sqrt(Bsigma);
  arma::vec phi = arma::ones(N+1) * (2*a0/(a0+b0)-1);
  arma::vec mu = arma::ones(N+1) * bmu;

  arma::vec h = -10.*arma::ones(T);  // contains h1 to hT, but not h0!
  if (!centered_baseline) h = (h-mu[0])/sigma[0];

  double h0;

  arma::ivec r(T);  // mixture indicators

  int hstorelength = T/thintime;  // thintime must either be 1 or T
  arma::mat hstore(hstorelength, draws/thin);
  arma::vec h0store(draws/thin);

  arma::vec data = y%y;  // commonly used transformation
  if (arma::any(data==0.)) {
    data += arma::stddev(data,0)/10000.;
  }
  data = arma::log(data);

  arma::vec curpara(3);  // holds mu, phi, sigma in every iteration
  curpara[0] = mu[0];
  curpara[1] = phi[0];
  curpara[2] = sigma[0];

  arma::vec omega_diag(T+1);  // contains diagonal elements of precision matrix
  double omega_offdiag;  // contains off-diag element of precision matrix (const)
  arma::vec chol_offdiag(T), chol_diag(T+1);  // Cholesky-factor of Omega
  arma::vec covector(T+1);  // holds covector (see McCausland et al. 2011)
  arma::vec htmp(T+1);  // intermediate vector for sampling h
  arma::vec hnew(T+1);  // intermediate vector for sampling h


  for (int i = 0; i < N; i++) {  // BEGIN main MCMC loop
    // a single MCMC update: update indicators, latent volatilities,
    // and parameters ONCE
    // update_sv(data, curpara, h, h0, mixprob_vec, r, 
    //   centered_baseline, C0, cT, Bsigma, a0, b0, bmu, Bmu, 
    //   B011inv, B022inv, Gammaprior, truncnormal, MHcontrol, 
    //   MHsteps, parameterization, dontupdatemu, priorlatent0);

    if (dontupdatemu) curpara.at(0) = 0.; // just to be sure
    double sigma2inv = std::pow(curpara.at(2),-2);
    double Bh0inv = 1./priorlatent0;
    if (priorlatent0 < 0) Bh0inv = 1.-curpara.at(1)*curpara.at(1);

  /*
   * Step (c): sample indicators
   */

  // calculate non-normalized CDF of posterior indicator probs
    if (centered_baseline) {
      data -= h;
    } else {
      data -= curpara.at(0) + curpara.at(2)*h;
    }


    arma::vec innov = arma::randu(T); 
    #ifdef _OPENMP
    #pragma omp parallel for shared(mix_pre,mix_mean,mix_2varinv,innov,data,T,r) default(none) schedule(auto)
    #endif
    for (unsigned int t = 0; t < T; t++) {  // SLOW (10*T calls to exp)!
      arma::vec mixp(10); mixp.fill(0.0);
      mixp.at(0) = std::exp(mix_pre[0]-(data.at(t)-mix_mean[0])*(data.at(t)-mix_mean[0])*mix_2varinv[0]);
      for (int r = 1; r < 10; r++) {
        mixp.at(r) = mixp.at(r-1) + std::exp(mix_pre[r]-(data.at(t)-mix_mean[r])*(data.at(t)-mix_mean[r])*mix_2varinv[r]);
      }

      unsigned int index = (10-1)/2;  // start searching in the middle
      double temp = innov.at(t)*mixp.at(9);  // current (non-normalized) value
      bool larger = false;  // indicates that we already went up
      bool smaller = false; // indicates that we already went down
      while(true) {
        if (temp > mixp(index)) {
          if (smaller == true) {
            index++;
            break;
          }
          else {
            index++;
            larger = true;
          }
        }
        else {
          if (larger == true) {
            break;
          }
          else {
            if (index == 0) {
              break;
            }
            else {
              index--;
              smaller = true;
            }
          } 
        }
      }
      r.at(t) = index;
    }


    /*
    * Step (a): sample the latent volatilities h:
    */
    const double phi2tmp = curpara.at(1)*curpara.at(1);

    if (centered_baseline) { // fill precision matrix omega and covector c for CENTERED para:
      data += h; // T x 1

      omega_diag.at(0) = (Bh0inv + phi2tmp) * sigma2inv;
      covector.at(0) = curpara.at(0) * (Bh0inv-curpara.at(1)+phi2tmp) * sigma2inv;

      for (int j = 1; j < T; j++) {
        omega_diag.at(j) = mix_varinv[r.at(j-1)] + (1+phi2tmp)*sigma2inv; 
        covector.at(j) = (data.at(j-1) - mix_mean[r.at(j-1)])*mix_varinv[r.at(j-1)]
          + curpara.at(0)*(1-curpara.at(1))*(1-curpara.at(1))*sigma2inv;
      }
      omega_diag.at(T) = mix_varinv[r.at(T-1)] + sigma2inv;
      covector.at(T) = (data.at(T-1) - mix_mean[r.at(T-1)])*mix_varinv[r.at(T-1)]
        + curpara.at(0)*(1-curpara.at(1))*sigma2inv;
      omega_offdiag = -curpara.at(1)*sigma2inv;  // omega_offdiag is constant

    } else { // fill precision matrix omega and covector c for NONCENTERED para:
      data += curpara.at(0) + curpara.at(2)*h; // T x 1

      const double sigmainvtmp = sqrt(sigma2inv);
      omega_diag.at(0) = phi2tmp + Bh0inv;
      covector.at(0) = 0.;

      for (int j = 1; j < T; j++) {
        omega_diag[j] = mix_varinv[r[j-1]]/sigma2inv + 1 + phi2tmp; 
        covector[j] = mix_varinv[r[j-1]]/sigmainvtmp*(data[j-1] - mix_mean[r[j-1]] - curpara[0]);
      }
      omega_diag[T] = mix_varinv[r[T-1]]/sigma2inv + 1;
      covector[T] = mix_varinv[r[T-1]]/sigmainvtmp*(data[T-1] - mix_mean[r[T-1]] - curpara[0]);
      omega_offdiag = -curpara[1];  // omega_offdiag is constant
    } 

    // Cholesky decomposition
    cholTridiag(omega_diag, omega_offdiag, chol_diag, chol_offdiag);

    // Solution of Chol*x = covector ("forward algorithm")
    forwardAlg(chol_diag, chol_offdiag, covector, htmp);

    htmp += arma::randn(T+1); // T+1

    // Solution of (Chol')*x = htmp ("backward algorithm")
    backwardAlg(chol_diag, chol_offdiag, htmp, hnew);

    h = hnew.tail(T);
    h0 = hnew.at(0);


  /*
   * Step (b): sample mu, phi, sigma
   */

    if (centered_baseline) {  // this means we have C as base
      regressionCentered(h0, h, 
        curpara.at(0), curpara.at(1), curpara.at(2),
        C0, cT, Bsigma, a0, b0, bmu, Bmu, B011inv,
        B022inv, Gammaprior, truncnormal, MHcontrol, 
        MHsteps, dontupdatemu, priorlatent0);

      if (parameterization == 3) {  // this means we should interweave
        h -= curpara.at(0); h /= curpara.at(2);
        h0 -= curpara.at(0); h0/= curpara.at(2);
        regressionNoncentered(data, h0, h, r,
          curpara.at(0), curpara.at(1), curpara.at(2), 
          Bsigma, a0, b0, bmu, Bmu, truncnormal, 
          MHsteps, dontupdatemu, priorlatent0);
        h *= curpara.at(2); h += curpara.at(0);
        h0 *= curpara.at(2); h0 += curpara.at(0);
      }
    } else {  // NC as base
      regressionNoncentered(data, h0, h, r, 
        curpara.at(0), curpara.at(1), curpara.at(2),
        Bsigma, a0, b0, bmu, Bmu, truncnormal, MHsteps,
        dontupdatemu, priorlatent0);

      if (parameterization == 4) {  // this means we should interweave
        h *= curpara.at(2); h += curpara.at(0);
        h0 *= curpara.at(2); h0 += curpara.at(0);
        regressionCentered(h0, h, 
          curpara.at(0), curpara.at(1), curpara.at(2), 
          C0, cT, Bsigma, a0, b0, bmu, Bmu, B011inv, B022inv,
          Gammaprior, truncnormal, MHcontrol, MHsteps,
          dontupdatemu, priorlatent0);
        h -= curpara.at(0); h /= curpara.at(2);
        h0 -= curpara.at(0); h0/= curpara.at(2);
      }
    }

    // storage:
    if (!((i+1) % thin)) if (i >= burnin) {  // this means we should store h
      if (centered_baseline) {
        for (int j = 0; j < hstorelength; j++) hstore.at(j, (i-burnin)/thin) = h[thintime * (j + 1) - 1];
        h0store[(i-burnin)/thin] = h0;
      } else {
        for (int j = 0; j < hstorelength; j++) hstore.at(j, (i-burnin)/thin) = curpara[0] + curpara[2]*h[thintime * (j + 1) - 1];
        h0store[(i-burnin)/thin] = curpara[0] + curpara[2]*h0;
      }
    }

    mu[i+1] = curpara[0];
    phi[i+1] = curpara[1];
    sigma[i+1] = curpara[2];

  }  // END main MCMC loop

  arma::mat res(mu.size(), 3); 
  res.col(0) = mu;
  res.col(1) = phi;
  res.col(2) = sigma;

  List val = List::create(
    _["para"] = res,
    _["latent"] = hstore,
    _["latent0"] = h0store);
  // Prepare return value and return
  return val;
}
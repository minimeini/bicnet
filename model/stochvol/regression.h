#ifndef REGRESSION_H
#define REGRESSION_H

#include <cmath>
#include <RcppArmadillo.h>
#include "auxmix.h"
#include "densities.h"


// Step (b): sample mu, phi, sigma - __CENTERED__ version:
void regressionCentered(
    const double h0,
    const arma::vec& h,
    double& mu,
    double& phi,
    double& sigma,
    const double C0,
    const double cT,
    const double Bsigma,
    const double a0,
    const double b0,
    const double bmu,
    const double Bmu,
    const double B011inv,
    const double B022inv,
    const bool Gammaprior,
    const bool truncnormal,
    const double MHcontrol,
    const int MHsteps,
    const bool dontupdatemu,
    const double priorlatent0);

// Step (b): sample mu, phi, sigma - __NONCENTERED__ version:
void regressionNoncentered(
    const arma::vec& data,
    const double h0,
    const arma::vec& h,
    const arma::ivec& r,
    double& mu,
    double& phi,
    double& sigma,
    const double Bsigma,
    const double a0,
    const double b0,
    const double bmu,
    const double Bmu,
    const bool truncnormal,
    const int MHsteps,
    const bool dontupdatemu,
    const double priorlatent0);

#endif  // REGRESSION_H


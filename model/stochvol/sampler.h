#ifndef _SAMPLER_H_
#define _SAMPLER_H_

#include <RcppArmadillo.h>


arma::mat gibbs_ar(
  const arma::vec& h, // T x 1
  const unsigned int nburnin,
  const unsigned int niter,
  const unsigned int nthin,
  const double Bsigma, // prior for the sqrt(sigma2) ~ N(0,Bsigma) of SVs
  const double a_phi, // prior for (phi+1)/2 ~ Beta(a_phi,b_phi) of SVs
  const double b_phi, // prior for (phi+1)/2 ~ Beta(a_phi,b_phi) of SVs
  const double bMuSv, // G x S
  const double BMuSv,
  const double B011inv, // 10^-12
  const double B022inv, // 10^-8
  const int MHsteps, // default = 2
  const bool Gammaprior);


Rcpp::List svsample_cpp(
    const arma::vec& y_in,
    const int draws,
    const int burnin,
    const double bmu,
    const double Bmu,
    const double a0,
    const double b0,
    const double Bsigma,
    const int thin,
    const int timethin,
    const bool quiet,
    const int parameterization,
    const int MHsteps,
    const double B011inv,
    const double B022inv,
    const double mhcontrol,
    const bool Gammaprior,
    const bool truncnormal,
    const bool dontupdatemu,
    const double priorlatent0);

#endif
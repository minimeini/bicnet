#ifndef _SIMULATION_H
#define _SIMULATION_H

#ifdef _OPENMP
#include <omp.h>
#endif

#include <iostream>
#include <cmath>
#include <RcppArmadillo.h>
#include "posteriors.h"


arma::mat left_ordered_Z(const arma::mat& Z);


arma::mat simulate_Z(const int& N, const int& K, 
	const arma::colvec& probs, 
	const bool& restricted);


arma::mat default_Z_rlt(const int& N, const int& K);


arma::mat default_Z_glt(const int& N, const int& K, const int& No);

arma::mat default_Z_swin(const unsigned int& N,
	const unsigned int& K,
	const unsigned int& wlen);


arma::mat simulate_dense_Lambda(int N, int K, 
	const arma::vec& Omega_diag);


arma::mat simulate_sparse_Lambda(int N, int K, 
	const arma::mat& Z, 
	const arma::vec& Omega_diag);


arma::colvec simulate_h(const int& T, 
	const double& mu, 
	const double& phi, 
	const double& sigma);


arma::mat simulate_static_Y(
	int T, int N, int K, 
	const arma::mat& Lambda, 
	const arma::vec& Omega_diag, 
	const arma::vec& Sigma_diag);


Rcpp::List simulate_dynamic_Y(
	const int& T, const int& N, const int& K, 
	const arma::mat& Lambda, 
	const arma::vec& Sigma_diag, 
	const arma::vec& hMu, 
	const arma::vec& hPhi, 
	const arma::vec& hSigma);


void simulate_dynamic_Y(
	arma::mat& Y, // output, N x T
	arma::mat& h, // output, K x T
	arma::mat& fac, // K x T
	const unsigned int& T,
	const unsigned int& N,
	const unsigned int& K,
	const arma::vec& VarObs, // N x 1
	const arma::mat& Lambda, // N x K
	const arma::vec& hMu, // K x 1
	const arma::vec& hPhi,  // K x 1
	const arma::vec& hVar); // K x 1


Rcpp::List simulate_dynamic_single(const int& N, 
	const int& T,
	const int& K, 
	const int& Ztype, 
	const arma::vec& Omega_diag,
	const arma::vec& Sigma_diag,
	const arma::vec& hMu,
	const arma::vec& hPhi,
	const arma::vec& hSigma,
	const int& NOverlap,  // for default_Z_glt
	const Rcpp::Nullable<Rcpp::NumericVector>& probsIn, // for simulate_Z
	const bool& restricted,
	const bool& sparse);


Rcpp::List simulate_dynamic_fused(const int& N, 
	const int& T,
	const int& K, 
	const int& Ztype, 
	const arma::vec& Omega_diag,
	const arma::vec& Sigma_diag,
	const arma::vec& hMu,
	const arma::vec& hPhi,
	const arma::vec& hSigma,
	const arma::vec& hMu_ast,
	const int& NOverlap,  // for default_Z_glt
	const Rcpp::Nullable<Rcpp::NumericVector>& probsIn, // for simulate_Z
	const bool& restricted,
	const bool& sparse);


Rcpp::List simulate_dynamic_group(const int N, 
	const int K,
	const int S,
	const arma::mat& group, // Tall x G
	const Rcpp::Nullable<Rcpp::NumericVector> VarObsIn, // G x S
	const double c_sigma,
	const double d_sigma,
	const int Ztype, 
	const Rcpp::Nullable<Rcpp::NumericVector> ZIn, // N x K
	const Rcpp::Nullable<Rcpp::NumericVector> PiIn, // N x K
	const Rcpp::Nullable<Rcpp::NumericVector> m_piIn, // K x 1
	const Rcpp::Nullable<Rcpp::NumericVector> a_piIn, // K x 1
	const double tau2,
	const Rcpp::Nullable<Rcpp::NumericVector> hMuTaskIn, // K x G x S
	const double b_mu,
	const double B_mu,
	const Rcpp::Nullable<Rcpp::NumericVector> hPhiTaskIn, // K x G x S
	const double a_phi,
	const double b_phi,
	const Rcpp::Nullable<Rcpp::NumericVector> hVarTaskIn, // K x G x S
	const double Bsigma);

#endif
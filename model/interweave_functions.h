//[[Rcpp::depends(RcppArmadillo)]]
#include <iostream>
#include <cmath>
#include <algorithm>
#include <RcppArmadillo.h>

using namespace std;
using namespace arma;
using namespace Rcpp;


/* BUGS

Error 1. chi=inf when chi is supposed to be finite positive

Error 2. chi=0 when chi is supposed to be finite positive

*/

void shallow_interweave(
	mat& Lambda, // N x K
	mat& F, // K x T
	const mat& fsigma2, // K x T, fsigma2 = exp(h), T >= 1
 	const mat& Lambda_prior) { // K x N, N >= 1
	/*
	Lambda: N x K, factor loadings
	F: K x T, factors
	h: K x T, log of dynamic factor variance
	B_lambda: double, prior for the diagonal elements (Lambda[j,j]) of Lambda
	*/
	int N = Lambda.n_rows;
	int K = Lambda.n_cols;
	int T = F.n_cols;
	int Nk = N;

	double lambda, chi, psi;
	double pivot, pivot_new, tmp;

	for (int k=0; k<K; k++) {
		pivot = std::pow(Lambda(k,k),2);
		pivot_new = pivot;

		if (fsigma2.n_cols > 1) {
			chi = sum(square(F.row(k)) / fsigma2.row(k)) * pivot;
		} else {
			chi = sum(square(F.row(k))) / fsigma2(k) * pivot;
		}

		if (pivot > 0 && chi > 0 && !std::isinf(chi)) {
			// 0. update Nj: nonzero elemtns of the j-th column
			Nk = arma::size(find(Lambda.col(k) != 0))[0];

			// 1. sample new Lambda(j,j)^2
			// ALARM - Different from Kastner's: no plus one
			lambda = (Nk-T) / 2. + 1;

			if (Lambda_prior.n_cols > 1) {
				// Different from Kastner's: no plus one
				psi = sum(square(Lambda.col(k).t())/Lambda_prior.row(k))/pivot + 1/Lambda_prior(k,k);
			} else {
				psi = (sum(square(Lambda.col(k)))/pivot + 1) / Lambda_prior(k);
			}
			
			pivot_new = do_rgig(lambda, chi, psi);

			// 2. transform back
			tmp = sqrt(pivot_new / pivot);
			if (!isinf(tmp) && !isnan(tmp) && (tmp!=0)) {
				Lambda.col(k) *= tmp;
				F.col(k) *= 1/tmp;
			}

		} else {
			Rcout << "shallow_interweave - Invalid parameters for GIG distribution: " << endl;
			Rcout << "    lambda=" << lambda << ", chi=" << chi << ", psi=" << psi << endl;
		}
	}
}


void deep_interweave() {

}
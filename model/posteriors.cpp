//[[Rcpp::depends(RcppArmadillo, RcppDist)]]
#include "posteriors.h"
using namespace Rcpp;


/*
STATIC FACTOR MODEL WITH NORMAL-GAMMA SPARSE SHRINKAGE: gibbs_static_ng

Ref: Kastner, 2019; Kastner et al., 2017.

1. sample factor and idiosyncratic variance: update_static_evar, update_static_fvar
2. sample shrinkage parameters: update_ng_lambda, update_ng_tau
3. sample factor loadings: update_Lambda, shallow_interweave_Lambda
4. sample factors: update_factors
*/


// void update_static_esigma2(double& esigma2, 
// 	const arma::mat& Y, 
// 	const arma::mat& Lambda, 
// 	const arma::mat& F, 
// 	const double& c_sigma, 
// 	const double& d_sigma) {
	
// 	>>> Parameters

// 	- Y: N x T, observables
// 	- Lambda: N x K, factor loadings
// 	- F: K x T, latent factors
// 	- c_sigma, d_sigma: (shape, rate) for the inverse-gamma distribution of sigma[i]^2

// 	>>> CHECK

// 	- No bug.
// 	- Consistent with Kastner's factorstochvol
	

// 	int N = Y.n_rows;
// 	int T = Y.n_cols;

// 	arma::mat E = Y - Lambda * F; // N x T
// 	double rss = arma::trace(E.t()*E); 
// 	esigma2 = 1/R::rgamma(c_sigma+.5*N*T, 1/(d_sigma+.5*rss));
// 	return;
// }


arma::vec update_idi_mean(
	const arma::mat& Y, // N x T
	const arma::mat& Lambda, // N x K, factor loadings
	const arma::mat& F, // K x T, latent factors
	const arma::vec& esigma2, // N x 1, idiosyncratic variance
	const double& b_mu, // prior mean for mu
	const double& B_mu){ // prior var for mu

	const int N = Y.n_rows;
	const int T = Y.n_cols;

	arma::vec tmp(N,arma::fill::zeros);
	tmp.fill(1/B_mu);

	arma::vec bmu(N); bmu.fill(b_mu);
	arma::mat BmuInv = arma::diagmat(tmp);

	arma::mat SigmaInv = arma::diagmat(1/esigma2);

	arma::vec sum_err(N,arma::fill::zeros);
	sum_err = arma::sum(Y-Lambda*F,1);

	arma::mat Sigma = BmuInv + T*SigmaInv;
	arma::mat PrecChol = arma::chol(Sigma);
	arma::mat PrecR2 = arma::solve(arma::trimatu(PrecChol), 
		arma::eye<arma::mat>(N,N));
	Sigma = PrecR2 * PrecR2.t();

	tmp = arma::randn(N,1);
	arma::vec Mean = Sigma * (SigmaInv*sum_err + BmuInv*bmu);
	return Mean + PrecR2*tmp;

}


arma::mat update_observed_mean(
	const arma::mat& Y, // N x Tall
	const arma::mat& group, // Tall x G
	const arma::mat& Lambda, // N x K
	const arma::mat& F, // K x Tall
	const arma::mat& VarObs, // N x G
	const double b_mu,
	const double B_mu) {

	const unsigned int N = Y.n_rows;
	const unsigned int G = group.n_cols;

	arma::mat MuObs(N,G,arma::fill::zeros);
	// arma::mat Err = Y - Lambda*F; // N x Tall
	arma::mat ErrG;

	arma::uvec idx;
	double Tg = 0;
	
	for (unsigned int g=0; g<G; g++) {
		idx = arma::find(group.col(g)==1);
		ErrG = Y.cols(idx) - Lambda*F.cols(idx);
		Tg = idx.n_elem;
		
		for (unsigned int n=0; n<N; n++) {
			double post_var = 1 / (1/B_mu + Tg/VarObs(n,g));
			double post_mean = post_var * (arma::accu(ErrG.row(n))/VarObs(n,g)+b_mu/B_mu);
			MuObs(n,g) = R::rnorm(post_mean, std::sqrt(post_var));
		}
	}

	return MuObs;
}



arma::vec update_static_esigma2(
	const arma::mat& Y, // N x T, observables
	const arma::mat& Lambda, // N x K, factor loadings
	const arma::mat& F, // K x T, latent factors
	const double& c_sigma, 
	const double& d_sigma) {

	int N = Y.n_rows;
	int T = Y.n_cols;
	double rss;

	arma::vec esigma2(N,arma::fill::ones);

	for (int i=0; i<N; i++) {
		rss = arma::accu(arma::square(Y.row(i) - Lambda.row(i)*F));
		esigma2(i) = 1/R::rgamma(c_sigma+.5*T, 1/(d_sigma+.5*rss));
	}

	return esigma2;
}


arma::mat update_observed_var(
	const arma::mat& Y, // N x Tall
	const arma::mat& group, // Tall x G
	const arma::mat& Lambda, // N x K
	const arma::mat& F, // K x Tall
	const double c_sigma,
	const double d_sigma) {

	const unsigned int N = Y.n_rows;
	const unsigned int G = group.n_cols;
	arma::mat VarObs(N,G,arma::fill::zeros);
	arma::mat ErrG;
	arma::uvec idx;
	double Tg = 0;
	
	for (unsigned int g=0; g<G; g++) {
		{
			idx = arma::find(group.col(g)==1);
			ErrG = Y.cols(idx)-Lambda*F.cols(idx);
			Tg = idx.n_elem;
		}
		
		for (unsigned int n=0; n<N; n++) { // add parallel
			VarObs(n,g) = 1/R::rgamma(c_sigma + 0.5*Tg, 
				1/(d_sigma + 0.5*arma::accu(arma::square(ErrG.row(n)))));
		}
	}

	return VarObs;
}


arma::vec update_static_fsigma2(const arma::mat& F, 
	const double& c_sigma, 
	const double& d_sigma) {
	int K = F.n_rows;
	int T = F.n_cols;
	double c_new = c_sigma + T/2;
	double d_new = 0;
	arma::vec fsigma2(K,arma::fill::zeros);

	for (int k=0; k<K; k++) {
		d_new = d_sigma + arma::accu(arma::square(F.row(k)))/2;
		fsigma2(k) = 1/R::rgamma(c_new, 1/d_new);
	}
	
	return fsigma2;
}



arma::vec update_ng_lambda2(const arma::mat& tau2, // N x K
	const double& a_tau, 
	const double& c_lambda, 
	const double& d_lambda, 
	const bool& restricted=false) {
	int N = tau2.n_rows;
	int K = tau2.n_cols;
	int r = K;
	double c_new, d_new;
	arma::vec lambda2(N,arma::fill::zeros);

	for (int i=0; i<N; i++) { // row-wise shrinkage, by region,parallel
		if (restricted) {
			r = std::min(i+1,K); // number of nonzero elements
		}

		c_new = c_lambda + a_tau*r;
		d_new = d_lambda + (a_tau/2.)*arma::accu(tau2.row(i));
		lambda2(i) = R::rgamma(c_new,1./d_new);
		lambda2(i) = (lambda2(i) < 1e-9)? 1e-9 : lambda2(i);
	}

	return lambda2; // N x 1
}


arma::mat update_ng_tau2(const arma::mat& tau2_old, 
	const arma::mat& Lambda, // N x K
	const arma::vec& lambda2, 
	const double& a_tau, 
	const bool& restricted=false) {
	int N = Lambda.n_rows;
	int K = Lambda.n_cols;
	int r = K;
	arma::mat tau2(N,K,arma::fill::zeros);
	double lambda, chi, psi;

	for (int i=0; i<N; i++) { // add parallel
		if (restricted) {
			r = std::min(i+1, K);
		}
		for (int k=0; k<r; k++) {
			lambda = a_tau-.5; // real value
			chi = std::pow(Lambda(i,k),2.0); // positive
			psi = a_tau*lambda2(i); // positive

			if (chi > 1e-18 && psi > 1e-9) {
				tau2(i,k) = do_rgig(lambda, chi, psi);
			} else {
				tau2(i,k) = tau2_old(i,k);
			}
			
		}
	}
	return tau2;
}


void update_ng_tau2_2(arma::vec& tau2, // K x 1
	const arma::mat& Lambda, // N x K
	const double& lambda2,
	const double& a_tau) {

	int K = Lambda.n_cols;
	arma::vec temp;
	double lambda, chi,Npos;
	double psi = a_tau*lambda2;

	for (int k=0; k<K; k++) {
		temp = arma::nonzeros(Lambda.col(k));
		Npos = temp.n_elem;
		lambda = a_tau - 0.5*Npos;
		chi = arma::accu(arma::square(temp));

		if (chi > 1e-9 && psi > 1e-9) {
			tau2(k) = do_rgig(lambda, chi, psi);
		}
	}
	return;
}




double update_Lambda_nk(const double& logodd_prior, 
	const double& old_Lambda_nk,
	const arma::vec& Err_n, // Tall
	const arma::vec& F_k, // Tall
	const double& tau_k, // variance of normal distribution of Lambda(n,k)
	const arma::vec& esigma2_n, // G
	const int& mask_nk,
	double& Z_nk, // indicator under singlesubject model
	const unsigned int& G,
	arma::field<arma::uvec>& idx,
	const bool fixdiag = true) {

	arma::vec Err_nn(Err_n);
	Err_nn += old_Lambda_nk * F_k;

	double post_prec = 1./tau_k;
	double post_mean = 0.;
	for(unsigned int g=0; g<G; g++) {
		post_prec += 1./esigma2_n.at(g) * arma::dot(F_k.elem(idx.at(g)),F_k.elem(idx.at(g)));
		post_mean += 1./esigma2_n.at(g) * arma::dot(F_k.elem(idx.at(g)),Err_nn.elem(idx.at(g)));
	}


	if (post_prec < 1e-9) {
		post_prec = 1e-9;
	}

	post_mean /= post_prec;

	double new_Lambda_nk = 0.;
	if (mask_nk == 1) {
		if (fixdiag) {
			new_Lambda_nk = 1.;
		} else {
			// diagonal - nonnegative
			double post_std = std::pow(post_prec,-0.5);
			new_Lambda_nk = r_truncnorm(post_mean, 
				post_std,0.,2.*post_std);
		}
		
		Z_nk = 1.;
	} else { 
		// mask_nk == 2
		// off diagonal
		double logodd = 0.5*(post_prec*std::pow(post_mean,2.)-std::log(post_prec)-std::log(tau_k)); // likelihood
		logodd += logodd_prior;
		double prob = 1./(1.+std::exp(-logodd));

		if (!std::isfinite(prob)) {
			Rcout << "logodd_prior: " << logodd_prior << std::endl;
			Rcout << "post_prec: " << post_prec << std::endl;
			Rcout << "logodd_ll: " << logodd << std::endl;
			Rcout << "Posterior prob: " << prob << std::endl;
			::Rf_error("update_Lambda_nk: not finite posterior probability.");
		}

		Z_nk = (arma::randu(1)[0]<prob?1.:0.);
		if (Z_nk==1.0) {
			new_Lambda_nk = post_mean + std::pow(post_prec,-0.5)*arma::randn(1)[0];
		}

	}
	
	return new_Lambda_nk;
}



void update_Pi_group(arma::mat& Pi, // N x K
	const arma::cube& Lambda, // N x K x S
	const arma::vec& Pi_m, // K x 1
	const arma::vec& Pi_a,
	const arma::imat& mask) { // K x 1

	const unsigned int N = Lambda.n_rows;
	const unsigned int K = Lambda.n_cols;
	const unsigned int S = Lambda.n_slices;

	arma::umat Z(N,K,arma::fill::zeros);

	for (unsigned int s=0; s<S; s++) {
		Z += (arma::abs(Lambda.slice(s)) > 0.0);
	}

	for (unsigned int n=0; n<N; n++) { // parallel
		for (unsigned int k=0; k<K; k++) {
			if (mask.at(n,k)>0) {
				Pi.at(n,k) = R::rbeta(Pi_a.at(k)*Pi_m.at(k)+Z.at(n,k), 
					Pi_a.at(k)*(1-Pi_m.at(k))+S-Z.at(n,k));
			} else {
				Pi.at(n,k) = 0.0;
			}
			
		}
	}

	return;
}


arma::vec update_Mu_ind(const arma::mat& Mu_s, // K x G
	const arma::vec& Mu_var, // K x 1
	const double prior_mean,
	const double prior_var){
	int K = Mu_s.n_rows;
	int G = Mu_s.n_cols;
	arma::vec Mu_prec = 1/Mu_var;// K x 1
	arma::vec Mu_sum = Mu_s * arma::ones(G,1); // K x 1
	arma::vec post_var = 1/(G*Mu_prec+1/prior_var); // K x 1
	arma::vec post_mean = (post_var%Mu_sum%Mu_prec + post_var*prior_mean/prior_var);

	return (post_mean + arma::diagmat(arma::sqrt(post_var))*arma::randn(K,1));
}


arma::vec update_Var_ind(const arma::mat& Mu_s, // K x G
	const arma::vec& Mu_ind, // K x 1
	const double c_sigma,
	const double d_sigma){
	int K = Mu_s.n_rows;
	int G = Mu_s.n_cols;

	double c_new = c_sigma + 0.5*G; // shape
	double d_new = 0; // rate
	arma::vec Var_ind(K,arma::fill::zeros);
	for (int k=0; k<K; k++) {
		d_new = d_sigma + 0.5*arma::accu(arma::square(Mu_s.row(k)-arma::ones(1,G)*Mu_ind(k)));
		Var_ind(k) = 1/R::rgamma(c_new, 1/d_new);
	}

	return Var_ind;
}


//[[Rcpp::export]]
arma::mat update_static_factors(const arma::mat& Y, // N x T
	const arma::mat& Lambda,  // N x K
	const arma::vec& esigma2, // N x 1
	const arma::vec& fsigma2) { // K x 1

	int T = Y.n_cols; // Y: N x T
	int K = Lambda.n_cols; // Lambda: N x K
	double tmp;

	arma::mat F(K,T,arma::fill::zeros); // F: K x T
	arma::mat SigmaInv = arma::diagmat(1/esigma2);
	arma::mat Prec = Lambda.t()*SigmaInv*Lambda + arma::diagmat(1/fsigma2); // precision
	arma::mat Prec_chol, Chol_inv, Sigma;

	try {
		Prec_chol = arma::chol(Prec);
	} catch (...) {
		::Rf_error("update_static_factors: Couldn't Cholesky-decompose posterior factor precision - 1.");
	}

	try {
		if (VERBOSE) {
			tmp = arma::rcond(arma::trimatu(Prec_chol));
			if (tmp < EPSILON) {
				Rcout << "update_static_factors: trimatu(Prec_chol) seems singular - rcond=" << tmp << std::endl;
			}
		}
		
		Chol_inv = arma::solve(arma::trimatu(Prec_chol), arma::eye<arma::mat>(K,K));
	} catch (...) {
		::Rf_error("update_static_factors: Couldn't invert Cholesky factor of posterior factor precision - 2.");
	}
	
	arma::mat Mu_tmp = Chol_inv * Chol_inv.t() * Lambda.t() * SigmaInv;
	arma::vec Mu(K,arma::fill::zeros);

	for (int t=0; t<T; t++) {
		Mu = Mu_tmp * Y.col(t);
		F.col(t) = (Mu + Chol_inv*arma::randn(K,1));
	}

	return F;
}


arma::mat update_dynamic_factors(const arma::mat& Y, // N x Tall
	const arma::mat& Lambda, // N x K
	const arma::mat& esigma2, // N x G
	const arma::mat& h, // Tall x K
	const arma::uvec& gid) { // Tall x 1

	unsigned int Tall = Y.n_cols; // Y: N x Tall
	unsigned int K = Lambda.n_cols; // Lambda: N x K
	arma::mat F(K,Tall,arma::fill::zeros); // F: K x Tall
	
	auto start = std::chrono::high_resolution_clock::now();
	#ifdef _OPENMP
	#pragma omp parallel for shared(Tall,K,h,Lambda,Y,F,esigma2,gid) default(none) schedule(auto)
	#endif
	for (unsigned int t=0; t<Tall; t++) { // parallel
		try {
			// Prec_chol: K x K
			arma::mat SigmaInv = arma::diagmat(1.0/esigma2.col(gid(t)));
			arma::mat Prec_chol = arma::chol(Lambda.t()*SigmaInv*Lambda + arma::diagmat(arma::exp(-h.row(t))));
			arma::mat Chol_inv = arma::solve(arma::trimatu(Prec_chol),
				arma::eye<arma::mat>(K,K));
			arma::vec Mu = Chol_inv * Chol_inv.t() * Lambda.t() * SigmaInv * Y.col(t);
			F.col(t) = (Mu + Chol_inv * arma::randn(K,1));
		} catch (...) {
			::Rf_error("update_dynamic_factors: Cholesky decomposition or Inversion failed.");
		}
	}
	auto stop = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start); 
	Rcout << "Time for update_dynamic_factors: " << duration.count() << std::endl;

	return F;
}


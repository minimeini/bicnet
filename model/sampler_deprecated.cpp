#include "sampler.h"
using namespace Rcpp;

// [[Rcpp::depends(RcppArmadillo, RcppDist)]]


// [[Rcpp::export]]
Rcpp::List gibbs_dfsv_dense(
	const arma::mat& Y, // N x T
	const int& K, 
	const int& nburnin, 
	const int& niter, 
	const int& nkeep, // number of samples returned before thinning
	const double& c_sigma, // prior for error/factor variance
	const double& d_sigma, // prior for error/factor variance
	const double& sigma2_v,// prior for Lambda[i,j]
	const bool& dynfac, // if the factor is dynamic or static
	const bool& restricted, // if the facload is plt
	const int& interweave,// 0=no; 1=shallow; 2=deep(dynamic)
	const double& Bsigma, // prior for the sqrt(sigma2) ~ N(0,Bsigma) of SVs
	const double& a_phi, // prior for (phi+1)/2 ~ Beta(a_phi,b_phi) of SVs
	const double& b_phi, 
	const double& b_mu, // prior for mu ~ N(b_mu,B_mu) of SVs
	const double& B_mu) { 
	/* 
	MODEL

	Y[:,t] = Lambda * F[:,t] + Epsilon[:,t]
	F[:,t] ~ N(0, Omega = diag(fsigma2[1],...,fsigma2[K]))
	Epsilon[:,t] ~ N(0, esigma2 * I_N)

	Lambda[i,j] ~ N(0, sigma2_v)
	1/esigma2 ~ Gamma(c_sigma, d_sigma)
	1/fsigma2[k] ~ Gamma(c_sigma, d_sigma)
	*/
	assert(nkeep <= niter);

	int N = Y.n_rows;
	int T = Y.n_cols;
	int B = nburnin + niter;

	arma::mat esigma2(N,B,arma::fill::ones);
	arma::mat fsigma2(K,B,arma::fill::ones);
	arma::cube Lambda(N,K,B); Lambda.fill(0.1);
	arma::cube F(K,T,B); F.fill(1.0);

	arma::vec Lambda_prior(K); Lambda_prior.fill(sigma2_v);

	arma::mat mixprob, hMu, hPhi, hSigma;
	arma::vec curpara, curh0, curhk, mixprobk;
	arma::cube h;
	arma::imat r;
	arma::ivec rk;

	// setting up for dynamic factor variance
	double c = 1e-4; 
	double C0 = 1.5*Bsigma;
	double cT = (T-1)/2; // posterior parameter for sigma2 (C)
	double B011inv = std::pow(10,-12);
	double B022inv = std::pow(10,-8);

	const bool centered_baseline = true;
	const bool Gammaprior = true;
	const bool truncnormal = false;
	const double MHcontrol = -1;
	const int MHsteps = 2;
	const int parameterization = 3; //=3 interweave, =2, no interweave
	const bool dontupdatemu = false;
	const double priorlatent0 = -1;

	if (dynfac) {
		hMu = arma::zeros<arma::mat>(K,B);
		hPhi = arma::ones<arma::mat>(K,B);
		hSigma = arma::ones<arma::mat>(K,B);
		curpara = arma::ones<arma::vec>(3);

		h = arma::zeros<arma::cube>(T,K,B);
		curh0 = arma::zeros<arma::vec>(K);
		curhk = arma::zeros<arma::vec>(T);

		r = arma::zeros<arma::imat>(T,K);
		rk = arma::zeros<arma::ivec>(T);
		mixprob = arma::zeros<arma::mat>(10*T,K);
		mixprobk = arma::zeros<arma::vec>(10*T);
	}

	// Initial values: mean or mode of the prior distributions
	Rcpp::List output;

	for (int b=1; b<B; b++) {
		// Static Error Variance
		esigma2.col(b) = update_static_esigma2(Y,Lambda.slice(b-1),
			F.slice(b-1),c_sigma,d_sigma); // checked.

		// Factor Variance
		if (dynfac) {
			for (int k=0; k<K; k++) {
				curpara(0) = hMu(k,b-1);
				curpara(1) = hPhi(k,b-1);
				curpara(2) = hSigma(k,b-1);
				curhk = h.slice(b-1).col(k);
				mixprobk = mixprob.col(k);
				rk = r.col(k);

				update_sv(arma::log(arma::square(F.slice(b-1).row(k).t())),
					curpara,curhk,curh0(k),mixprobk,rk,centered_baseline,
					C0,cT,Bsigma,a_phi,b_phi,b_mu,B_mu,B011inv,B022inv,
					Gammaprior,truncnormal,MHcontrol,MHsteps,
					parameterization,dontupdatemu,priorlatent0);

				h.slice(b).col(k) = curhk;
				hMu(k,b) = curpara(0);
				hPhi(k,b) = curpara(1);
				hSigma(k,b) = curpara(2);
				mixprob.col(k) = mixprobk;
				r.col(k) = rk;
			}
		} else {
			fsigma2.col(b) = update_static_fsigma2(F.slice(b-1),
				c_sigma,d_sigma); // checked.
		}

		// factor loadings and factors
		update_Lambda(Lambda.slice(b),Y,F.slice(b-1),esigma2.col(b),
			sigma2_v,restricted);

		// Interweaving
		if (interweave==1) {
			if (dynfac) {
				shallow_interweave(Lambda.slice(b),F.slice(b-1),
					exp(hMu.col(b)+c),Lambda_prior);
			} else {
				shallow_interweave(Lambda.slice(b),F.slice(b-1),
					fsigma2.col(b),Lambda_prior);
			}
		}


		if (dynfac) {
			F.slice(b) = update_dynamic_factors(Y,Lambda.slice(b),
				esigma2.col(b),h.slice(b));
		} else {
			F.slice(b) = update_static_factors(Y,Lambda.slice(b),
				esigma2.col(b),fsigma2.col(b));
		}
		Rcout << "\rProgress: " << b << "/" << B;
	}

	Rcout << "\rProgress: " << B << "/" << B << std::endl;

	output["esigma2"] = esigma2.cols(B-nkeep,B-1);
	arma::cube Lambda2 = Lambda.subcube(0,0,B-nkeep,N-1,K-1,B-1);
	output["Lambda"] = Rcpp::wrap(Lambda2);
	arma::cube F2 = F.subcube(0,0,B-nkeep,K-1,T-1,B-1);
	output["F"] = Rcpp::wrap(F2);

	if (dynfac) {
		arma::cube h2 = h.subcube(0,0,B-nkeep,T-1,K-1,B-1);
		output["h"] = Rcpp::wrap(h2);
		output["hMu"] = hMu.cols(B-nkeep,B-1);
		output["hPhi"] = hPhi.cols(B-nkeep,B-1);
		output["hSigma"] = hSigma.cols(B-nkeep,B-1);
	} else {
		output["fsigma2"] = fsigma2.cols(B-nkeep,B-1);
	}

	return output;
}


//[[Rcpp::export]]
Rcpp::List gibbs_dfsv_NG(
	const arma::mat& Y, // T: N x T
	const int& K, 
	const int& nburnin, 
	const int& niter, 
	const int& nkeep, // number of samples returned before thinning
	const double& c_sigma, 
	const double& d_sigma, 
	const double& a_tau, 
	const double& c_lambda, 
	const double& d_lambda, 
	const bool& dynfac,
	const bool& restricted, // if the facload is plt
	const int& interweave, // 0=no; 1=shallow; 2=deep(dynamic)
	const double& Bsigma, // prior for the sqrt(sigma2) ~ N(0,Bsigma) of SVs
	const double& a_phi, // prior for (phi+1)/2 ~ Beta(a_phi,b_phi) of SVs
	const double& b_phi, 
	const double& b_mu, // prior for mu ~ N(b_mu,B_mu) of SVs
	const double& B_mu) { 
	/* checked. identifiable.
	Soft sparsity with normal-gamma priors

	MODEL

	Y[:,t] = Lambda * F[:,t] + Epsilon[:,t]
	F[:,t] ~ N(0, Omega = diag(fsigma2[1],...,fsigma2[K]))
	Epsilon[:,t] ~ N(0, esigma2 * I_N)

	Lambda[i,j] ~ N(0, sigma2_v)
	1/esigma2 ~ Gamma(c_sigma, d_sigma)
	1/fsigma2[k] ~ Gamma(c_sigma, d_sigma)


	PROCEDURE

	1. Sample regional variance
	1. Sample dynamic factor variance: sample indicators, latent volatilities, and SV-parameters
	2. Sample factor loadings (consider shrinkage priors here)
	3. Interweaving
	4. Sample factors
	*/
	const bool debug=false;

	int N = Y.n_rows;
	int T = Y.n_cols;
	int B = nburnin + niter;

	assert(nkeep <= niter);

	arma::mat esigma2(N,B,arma::fill::ones);
	arma::mat fsigma2(K,B,arma::fill::ones);
	arma::mat lambda2(N,B,arma::fill::ones);
	arma::cube tau2(N,K,B); tau2.fill(1.0);
	arma::cube Lambda(N,K,B); Lambda.fill(0.1); // priorfacload = 0.1
	arma::cube F(K,T,B); F.fill(1.0);

	double curh0k;

	double c = 1e-6; 
	double C0 = 1.5*Bsigma;
	double cT = (T-1)/2; // posterior parameter for sigma2 (C)
	double B011inv = std::pow(10,-12);
	double B022inv = std::pow(10,-8);

	bool centered_baseline = true;
	bool Gammaprior = true;
	bool truncnormal = false;
	double MHcontrol = -1;
	int MHsteps = 2;
	int parameterization = 3;
	bool dontupdatemu = false;
	double priorlatent0 = -1;

	arma::mat hMu, hPhi, hSigma, mixprob;
	arma::colvec curpara, curh0, curhk, mixprobk;
	arma::cube h;
	arma::imat r;
	arma::ivec rk;

	// arma::mat hMu(K,B,arma::fill::zeros);
	// arma::mat hPhi(K,B); hPhi.fill(0.5);
	// arma::mat hSigma(K,B,arma::fill::ones);
	// arma::cube h(T,K,B); h.fill(0.0);

	// arma::colvec curpara(3,arma::fill::ones);
	// arma::colvec curh0(K,arma::fill::zeros);
	// arma::colvec curhk(T,arma::fill::zeros);

	// arma::imat r(T,K,arma::fill::zeros);
	// arma::ivec rk(T,arma::fill::zeros);
	// arma::mat mixprob(10*T,K,arma::fill::zeros);
	// arma::colvec mixprobk(10*T,arma::fill::zeros);

	if (dynfac) {
		hMu = arma::zeros<arma::mat>(K,B);
		hPhi = arma::zeros<arma::mat>(K,B); hPhi.fill(0.5);
		hSigma = arma::ones<arma::mat>(K,B);
		h = arma::zeros<arma::cube>(T,K,B);

		curpara = arma::ones<arma::colvec>(3);
		curh0 = arma::zeros<arma::colvec>(K);
		curhk = arma::zeros<arma::colvec>(T);

		r = arma::zeros<arma::imat>(T,K);
		rk = arma::zeros<arma::ivec>(T);
		mixprob = arma::zeros<arma::mat>(10*T,K);
		mixprobk = arma::zeros<arma::colvec>(10*T);
	}

	Rcpp::List output;

	for (int b=1; b<B; b++) {
		// Cross-regional variance (Static)
		esigma2.col(b) = update_static_esigma2(Y,Lambda.slice(b-1),
			F.slice(b-1),c_sigma,d_sigma); // checked.

		// Factor variance
		if (dynfac) {
		for (int k=0; k<K; k++) {
			curh0k = curh0(k);
			curpara(0) = hMu(k,b-1);
			curpara(1) = hPhi(k,b-1);
			curpara(2) = hSigma(k,b-1);
			curhk = h.slice(b-1).col(k);
			mixprobk = mixprob.col(k); // current mixing probabilities
			rk = r.col(k); // current mixing indices

			if (curpara.has_nan()) {
				::Rf_error("Invalid values - NaNs in hparas.");
			}

			update_sv(arma::log(arma::square(F.slice(b-1).row(k).t())),
				curpara,curhk,curh0k,mixprobk,rk,centered_baseline,
				C0,cT,Bsigma,a_phi,b_phi,b_mu,B_mu,B011inv,B022inv,
				Gammaprior,truncnormal,MHcontrol,MHsteps,
				parameterization,dontupdatemu,priorlatent0);

			if (debug) {
				Rcout << "Iter " << b << " - hpara_" << k+1 << ": " << curpara.t();
			}
				
			if (!curpara.has_nan() && !curhk.has_nan() && !std::isnan(curh0k) && !mixprobk.has_nan() && !rk.has_nan()) {
				h.slice(b).col(k) = curhk;
				hMu(k,b) = curpara(0);
				hPhi(k,b) = curpara(1);
				hSigma(k,b) = curpara(2);
				curh0(k) = curh0k;
				mixprob.col(k) = mixprobk;
				r.col(k) = rk;
			} else {
				if (debug) {
					Rcout << "#of elems in F: " << arma::size(arma::square(F.slice(b-1).row(k))) << std::endl;
					Rcout << "Inf in log-squared F: " << arma::log(arma::square(F.slice(b-1).row(k))).has_inf() << std::endl;
					Rcout << "NaN in log-squared F: " << arma::log(arma::square(F.slice(b-1).row(k))).has_nan() << std::endl;
					Rcout << "#of Nonzero elems in F: " << arma::size(arma::nonzeros(square(F.slice(b-1).row(k)))) << std::endl;
					Rcout << "NaNs in the MCMC sampling - don't update." << arma::endl;
					Rcout << "(Mu, Phi, Sigma) = " << curpara;
				}
				h.slice(b).col(k) = h.slice(b-1).col(k);
				hMu(k,b) = hMu(k,b-1);
				hPhi(k,b) = hPhi(k,b-1);
				hSigma(k,b) = hSigma(k,b-1);
			}
		}
		} else {
			fsigma2.col(b) = update_static_fsigma2(F.slice(b-1),c_sigma,d_sigma);
		}

		
		// Factor loading matrix
		lambda2.col(b) = update_ng_lambda2(tau2.slice(b-1),
			a_tau,c_lambda,d_lambda,restricted); // checked.

		tau2.slice(b) = update_ng_tau2(tau2.slice(b-1),Lambda.slice(b-1),
			lambda2.col(b),a_tau,restricted); // checked.
		if (debug) {
			Rcout << "Iter " << b << " - tau2: ";
			Rcout << "NaN="<< tau2.slice(b).has_nan() << " Inf=" << tau2.slice(b).has_inf()<<std::endl;
		}

		update_Lambda_NG(Lambda.slice(b),Y,tau2.slice(b),
			F.slice(b-1),esigma2.col(b),restricted); // checked

		if (debug) {
			Rcout << "Iter " << b << " - Lambda: ";
			Rcout << "NaN="<< Lambda.slice(b).has_nan() << " Inf=" << Lambda.slice(b).has_inf()<<std::endl;
		}


		// Interweaving
		if (interweave==1) {
			shallow_interweave(Lambda.slice(b),F.slice(b-1),
				exp(hMu.col(b)+c),tau2.slice(b).t());
		}

		// Latent factors
		if (dynfac) {
		F.slice(b) = update_dynamic_factors(Y,Lambda.slice(b),
			esigma2.col(b),h.slice(b));
		} else {
			F.slice(b) = update_static_factors(Y,Lambda.slice(b),
				esigma2.col(b),fsigma2.col(b));
		}

		if (debug) {
			Rcout << "Iter " << b << " - F: ";
			Rcout << "NaN="<< F.slice(b).has_nan() << " Inf=" << F.slice(b).has_inf()<<std::endl;
		}

		if(!debug) {
			Rcout << "\rProgress: " << b << "/" << B;
		}
	}

	if (!debug) {
		Rcout << "\rProgress: " << B << "/" << B << std::endl;
	}

	output["esigma2"] = esigma2.cols(B-nkeep,B-1);
	arma::cube Lambda2 = Lambda.subcube(0,0,B-nkeep,N-1,K-1,B-1);
	output["Lambda"] = Rcpp::wrap(Lambda2);
	arma::cube F2 = F.subcube(0,0,B-nkeep,K-1,T-1,B-1);
	output["F"] = Rcpp::wrap(F2);

	if (dynfac) {
	arma::cube h2 = h.subcube(0,0,B-nkeep,T-1,K-1,B-1);
	output["h"] = Rcpp::wrap(h2);
	output["hMu"] = hMu.cols(B-nkeep,B-1);
	output["hPhi"] = hPhi.cols(B-nkeep,B-1);
	output["hSigma"] = hSigma.cols(B-nkeep,B-1);
	} else {
		output["fsigma2"] = fsigma2.cols(B-nkeep,B-1);
	}


	return output;
}



//[[Rcpp::export]]
List gibbs_dfsv_SS(
	const arma::mat& Y, // T: N x T
	const int& K, 
	const int& nburnin, 
	const int& niter, 
	const int& nkeep, // number of samples returned before thinning
	const double& c_sigma, 
	const double& d_sigma, 
	const double& beta, 
	const bool& dynfac, // if the factor is dynamic or static
	const double& Bsigma, // prior for the sqrt(sigma2) ~ N(0,Bsigma) of SVs
	const double& a_phi, // prior for (phi+1)/2 ~ Beta(a_phi,b_phi) of SVs
	const double& b_phi, 
	const double& b_mu, // prior for mu ~ N(b_mu,B_mu) of SVs
	const double& B_mu) { 
	/* checked. identifiable.
	Hard sparsity with spike-and-slab priors

	PROCEDURE

	1. Sample regional variance
	1. Sample dynamic factor variance: sample indicators, latent volatilities, and SV-parameters
	2. Sample factor loadings (consider shrinkage priors here)
	3. Interweaving
	4. Sample factors
	*/

	const bool debug = false;

	int N = Y.n_rows;
	int T = Y.n_cols;
	int B = nburnin + niter;

	assert(nkeep <= niter);
	arma::mat esigma2(N,B,arma::fill::ones);
	arma::mat curZ(N,K,arma::fill::ones);
	arma::cube Z(N,K,B,arma::fill::ones);
	arma::cube Lambda(N,K,B); Lambda.fill(0.1);
	arma::cube F(K,T,B); F.fill(1.0);

	arma::mat fsigma2, mixprob, hMu, hPhi, hSigma;
	arma::vec curpara, curh0, curhk, mixprobk;
	arma::cube h;
	arma::imat r;
	arma::ivec rk;
	double curh0k;

	double c = 1e-4; 
	double C0 = 1.5*Bsigma;
	double cT = (T-1)/2; // posterior parameter for sigma2 (C)
	double B011inv = std::pow(10,-12);
	double B022inv = std::pow(10,-8);

	const bool centered_baseline = true;
	const bool Gammaprior = true;
	const bool truncnormal = false;
	const double MHcontrol = -1;
	const int MHsteps = 2;
	const int parameterization = 3;
	const bool dontupdatemu = false;
	const double priorlatent0 = -1;

	if (dynfac) {
		hMu = arma::zeros<arma::mat>(K,B);
		hPhi = arma::ones<arma::mat>(K,B);
		hSigma = arma::ones<arma::mat>(K,B);
		curpara = arma::ones<arma::colvec>(3);
		curpara[1] = 0;

		h = arma::zeros<arma::cube>(T,K,B);
		curh0 = arma::zeros<arma::colvec>(K);
		curhk = arma::zeros<arma::colvec>(T);

		r = arma::zeros<arma::imat>(T,K);
		rk = arma::zeros<arma::ivec>(T);
		mixprob = arma::zeros<arma::mat>(10*T,K);
		mixprobk = arma::zeros<arma::vec>(10*T);
	} else {
		fsigma2 = arma::ones<arma::mat>(K,B);
	}

	List output;

	for (int b=1; b<B; b++) {
		// Cross-regional variance (Static)
		esigma2.col(b) = update_static_esigma2(Y,Lambda.slice(b-1),
			F.slice(b-1),c_sigma,d_sigma); // checked.

		// Factor variance
		if (dynfac) {
			for (int k=0; k<K; k++) {
				curh0k = curh0(k);
				curpara(0) = hMu(k,b-1);
				curpara(1) = hPhi(k,b-1);
				curpara(2) = hSigma(k,b-1);
				curhk = h.slice(b-1).col(k);
				mixprobk = mixprob.col(k); // current mixing probabilities
				rk = r.col(k); // current mixing indices

				update_sv(arma::log(arma::square(F.slice(b-1).row(k).t())+c),
					curpara,curhk,curh0k,mixprobk,rk,centered_baseline,
					C0,cT,Bsigma,a_phi,b_phi,b_mu,B_mu,B011inv,B022inv,
					Gammaprior,truncnormal,MHcontrol,MHsteps,
					parameterization,dontupdatemu,priorlatent0);

				if (debug) {
					Rcout << "Iter " << b << " - hpara_" << k+1 << ": " << curpara.t();
				}

				curh0(k) = curh0k;
				h.slice(b).col(k) = curhk;
				hMu(k,b) = curpara(0);
				hPhi(k,b) = curpara(1);
				hSigma(k,b) = curpara(2);
				mixprob.col(k) = mixprobk;
				r.col(k) = rk;
			}
		} else {
			fsigma2.col(b) = update_static_fsigma2(F.slice(b-1),
				c_sigma,d_sigma); // checked.
		}
		
		// Factor loading matrix
		if (dynfac) {
			update_Lambda_SS(curZ,Lambda.slice(b),Lambda.slice(b-1),Y,
				F.slice(b-1),exp(hMu.col(b)),esigma2.col(b),beta);
		} else {
			update_Lambda_SS(curZ,Lambda.slice(b),Lambda.slice(b-1),Y,
				F.slice(b-1),fsigma2.col(b),esigma2.col(b),beta);
		}

		if (debug) {
			Rcout << "Iter " << b << " - Lambda: ";
			Rcout << "NaN="<< Lambda.slice(b).has_nan() << " Inf=" << Lambda.slice(b).has_inf()<<std::endl;
		}
		
		Z.slice(b) = curZ;


		// Latent factors
		if (dynfac) {
			F.slice(b) = update_dynamic_factors(Y,Lambda.slice(b),
				esigma2.col(b),h.slice(b));
		} else {
			F.slice(b) = update_static_factors(Y,Lambda.slice(b),
				esigma2.col(b),fsigma2.col(b));
		}

		if (debug) {
			Rcout << "Iter " << b << " - F: ";
			Rcout << "NaN="<< F.slice(b).has_nan() << " Inf=" << F.slice(b).has_inf()<<std::endl;
		}
		

		if (!debug) {
			Rcout << "\rProgress: " << b << "/" << B;
		}
		
	}

	if (!debug) {
		Rcout << "\rProgress: " << B << "/" << B << std::endl;
	}
	

	output["esigma2"] = esigma2.cols(B-nkeep,B-1);
	arma::cube Lambda2 = Lambda.subcube(0,0,B-nkeep,N-1,K-1,B-1);
	output["Lambda"] = wrap(Lambda2);
	arma::cube F2 = F.subcube(0,0,B-nkeep,K-1,T-1,B-1);
	output["F"] = wrap(F2);

	if (dynfac) {
		arma::cube h2 = h.subcube(0,0,B-nkeep,T-1,K-1,B-1);
		output["h"] = wrap(h2);
		output["hMu"] = hMu.cols(B-nkeep,B-1);
		output["hPhi"] = hPhi.cols(B-nkeep,B-1);
		output["hSigma"] = hSigma.cols(B-nkeep,B-1);
	} else {
		output["fsigma2"] = fsigma2.cols(B-nkeep,B-1);
	}

	return output;
}


//[[Rcpp::export]]
List gibbs_dfsv_SS_fused(
	const arma::mat& YA, // N x Ta
	const arma::mat& YB, // N x Tb
	const int& K, 
	const int& nburnin, 
	const int& niter, 
	const int& nkeep, // number of samples returned before thinning
	const double& c_sigma, 
	const double& d_sigma, 
	const double& beta, 
	const double& Bsigma, // prior for the sqrt(sigma2) ~ N(0,Bsigma) of SVs
	const double& a_phi, // prior for (phi+1)/2 ~ Beta(a_phi,b_phi) of SVs
	const double& b_phi, 
	const double& b_mu, // prior for mu ~ N(b_mu,B_mu) of SVs
	const double& B_mu) { 
	/* checked. identifiable.
	Hard sparsity with spike-and-slab priors

	PROCEDURE

	1. Sample regional variance
	1. Sample dynamic factor variance: sample indicators, latent volatilities, and SV-parameters
	2. Sample factor loadings (consider shrinkage priors here)
	3. Interweaving
	4. Sample factors
	*/

	const bool debug = false;

	assert(YA.n_rows == YB.n_rows);

	const int N = YA.n_rows;
	const int Ta = YA.n_cols;
	const int Tb = YB.n_cols;
	const int B = nburnin + niter;

	assert(nkeep <= niter);

	arma::mat YdemeanA(N,Ta);
	arma::mat YdemeanB(N,Tb);

	arma::mat meanA(N,B,arma::fill::zeros);
	arma::mat meanB(N,B,arma::fill::zeros);

	arma::rowvec kronA(Ta,arma::fill::ones);
	arma::rowvec kronB(Tb,arma::fill::ones);

	// if (idiosyncratic) {
	// 	mat esigma2A(N,B,fill::ones);
	// 	mat esigma2B(N,B,fill::ones);
	// } else {
	arma::mat esigma2A(N,B,arma::fill::ones);
	arma::mat esigma2B(N,B,arma::fill::ones);
	// }

	arma::mat curZ(N,K,arma::fill::ones);
	arma::cube Z(N,K,B,arma::fill::ones);
	arma::cube Lambda(N,K,B); Lambda.fill(0.1);
	arma::cube FA(K,Ta,B); FA.fill(1.0);
	arma::cube FB(K,Tb,B); FB.fill(1.0);

	double c = 1e-4; 
	double C0 = 1.5*Bsigma;
	double cTa = (Ta-1)/2; // posterior parameter for sigma2 (C)
	double cTb = (Tb-1)/2; // posterior parameter for sigma2 (C)
	double B011inv = std::pow(10,-12);
	double B022inv = std::pow(10,-8);

	const bool centered_baseline = true;
	const bool Gammaprior = true;
	const bool truncnormal = false;
	const double MHcontrol = -1;
	const int MHsteps = 2;
	const int parameterization = 3;
	const double priorlatent0 = -1;
	const bool dontupdatemu = false;

	arma::vec curpara = arma::ones<arma::vec>(6);
	curpara[1] = curpara[4] = 0;
	arma::cube hpara = arma::ones<arma::cube>(6,K,B); 
	// hpara: mu, phiA, sigmaA, mu_ast, phiB, sigmaB

	arma::cube hA = arma::zeros<arma::cube>(Ta,K,B);
	arma::cube hB = arma::zeros<arma::cube>(Tb,K,B);
	
	double curhA0k, curhB0k;
	arma::vec curhA0 = arma::zeros<arma::vec>(K);
	arma::vec curhB0 = arma::zeros<arma::vec>(K);
	arma::vec curhA = arma::zeros<arma::vec>(Ta);
	arma::vec curhB = arma::zeros<arma::vec>(Tb);

	arma::imat rA = arma::zeros<arma::imat>(Ta,K);
	arma::imat rB = arma::zeros<arma::imat>(Tb,K);
	arma::ivec rAk = arma::zeros<arma::ivec>(Ta);
	arma::ivec rBk = arma::zeros<arma::ivec>(Tb);
	arma::mat mixprobA = arma::zeros<arma::mat>(10*Ta,K);
	arma::mat mixprobB = arma::zeros<arma::mat>(10*Tb,K);
	arma::vec mixprobAk = arma::zeros<arma::vec>(10*Ta);
	arma::vec mixprobBk = arma::zeros<arma::vec>(10*Tb);


	for (int b=1; b<B; b++) {
		// Regional Mean
		meanA.col(b) = update_idi_mean(YA,Lambda.slice(b-1),
			FA.slice(b-1),esigma2A.col(b-1),b_mu,B_mu);
		meanB.col(b) = update_idi_mean(YB,Lambda.slice(b-1),
			FB.slice(b-1),esigma2B.col(b-1),b_mu,B_mu);

		YdemeanA = YA - arma::kron(meanA.col(b),kronA);
		YdemeanB = YB - arma::kron(meanB.col(b),kronB);

		// Cross-regional variance (Static)
		esigma2A.col(b) = update_static_esigma2(YdemeanA,Lambda.slice(b-1),
			FA.slice(b-1),c_sigma,d_sigma); // checked.
		esigma2B.col(b) = update_static_esigma2(YdemeanB,Lambda.slice(b-1),
			FB.slice(b-1),c_sigma,d_sigma); // checked.

		// Factor variance
		for (int k=0; k<K; k++) {
			curhA0k = curhA0(k);
			curhB0k = curhB0(k);
			curpara = hpara.slice(b-1).col(k);
			curhA = hA.slice(b-1).col(k);
			curhB = hB.slice(b-1).col(k);
			mixprobAk = mixprobA.col(k); // current mixing probabilities
			mixprobBk = mixprobB.col(k); 
			rAk = rA.col(k); // current mixing indices
			rBk = rB.col(k);

			update_fused_sv(arma::log(arma::square(FA.slice(b-1).row(k).t())+c), 
				arma::log(arma::square(FB.slice(b-1).row(k).t())+c), 
				curpara, // mu, phiA, sigmaA, mu_ast, phiB, sigmaB 
    			curhA, curhB, curhA0k, curhB0k, 
    			mixprobAk, mixprobBk, rAk, rBk, 
    			centered_baseline, C0, cTa, cTb, Bsigma, a_phi, b_phi, b_mu,
    			B_mu, B011inv, B022inv, Gammaprior, truncnormal,
    			MHcontrol, MHsteps, parameterization, 
    			dontupdatemu,priorlatent0);

			if (debug) {
				Rcout << "Iter " << b << " - hpara_" << k+1 << ": " << curpara.t();
			}

			curhA0(k) = curhA0k;
			curhB0(k) = curhB0k;
			hA.slice(b).col(k) = curhA;
			hB.slice(b).col(k) = curhB;
			hpara.slice(b).col(k) = curpara;
			mixprobA.col(k) = mixprobAk;
			mixprobB.col(k) = mixprobBk;
			rA.col(k) = rAk;
			rB.col(k) = rBk;
		}

		
		// Factor loading matrix
		update_Lambda_SS_fused(curZ,Lambda.slice(b),Lambda.slice(b-1),
			YdemeanA,YdemeanB,FA.slice(b-1),FB.slice(b-1),
			arma::exp(hpara.slice(b).row(0).t()), 
			esigma2A.col(b),esigma2B.col(b),beta);


		if (debug) {
			Rcout << "Iter " << b << " - Lambda: ";
			Rcout << "NaN="<< Lambda.slice(b).has_nan() << " Inf=" << Lambda.slice(b).has_inf()<<std::endl;
		}
		
		Z.slice(b) = curZ;


		// Latent factors
		FA.slice(b) = update_dynamic_factors(YdemeanA,Lambda.slice(b),
			esigma2A.col(b),hA.slice(b));
		FB.slice(b) = update_dynamic_factors(YdemeanB,Lambda.slice(b),
			esigma2B.col(b),hB.slice(b));


		if (debug) {
			Rcout << "Iter " << b << " - FA: ";
			Rcout << "NaN="<< FA.slice(b).has_nan() << " Inf=" << FA.slice(b).has_inf()<<std::endl;

			Rcout << "Iter " << b << " - FB: ";
			Rcout << "NaN="<< FB.slice(b).has_nan() << " Inf=" << FB.slice(b).has_inf()<<std::endl;
		}
		

		if (!debug) {
			Rcout << "\rProgress: " << b << "/" << B;
		}
		
	}

	if (!debug) {
		Rcout << "\rProgress: " << B << "/" << B << std::endl;
	}
	

	List output;

	output["meanA"] = meanA.cols(B-nkeep,B-1);
	output["meanB"] = meanB.cols(B-nkeep,B-1);
	output["esigma2A"] = esigma2A.cols(B-nkeep,B-1);
	output["esigma2B"] = esigma2B.cols(B-nkeep,B-1);

	arma::cube Lambda2 = Lambda.subcube(0,0,B-nkeep,N-1,K-1,B-1);
	output["Lambda"] = wrap(Lambda2);
	arma::cube Z2 = Z.subcube(0,0,B-nkeep,N-1,K-1,B-1);
	output["Z"] = wrap(Z2);

	arma::cube FA2 = FA.subcube(0,0,B-nkeep,K-1,Ta-1,B-1);
	output["FA"] = wrap(FA2);
	arma::cube FB2 = FB.subcube(0,0,B-nkeep,K-1,Tb-1,B-1);
	output["FB"] = wrap(FB2);

	arma::cube hA2 = hA.subcube(0,0,B-nkeep,Ta-1,K-1,B-1);
	output["hA"] = wrap(hA2);
	arma::cube hB2 = hB.subcube(0,0,B-nkeep,Tb-1,K-1,B-1);
	output["hB"] = wrap(hB2);

	arma::cube hpara2 = hpara.subcube(0,0,B-nkeep,5,K-1,B-1);
	output["hpara"] = wrap(hpara2);

	return output;
}

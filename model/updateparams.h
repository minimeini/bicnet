#ifndef _UPDATEPARAMS_H
#define _UPDATEPARAMS_H


#include <cmath>
#include <vector>
#include <algorithm>
#include <RcppArmadillo.h>
#include <stmpct/gk.hpp>


void initialize(const arma::cube& Y,
	const arma::mat& group,
	const int K,
	arma::cube& MuObs,
	arma::cube& VarObs,
	arma::cube& Lambda,
	arma::cube& F,
	arma::cube& h,
	arma::mat& hMu_g,
	arma::mat& hPhi_g,
	arma::mat& hVar_g,
	arma::mat& hMu,
	arma::mat& hMuVar);


void initialize(const arma::cube& Y,
	const arma::mat& group,
	const int K,
	arma::cube& MuObs,
	arma::cube& VarObs,
	arma::cube& Lambda,
	arma::cube& F,
	arma::cube& h,
	arma::mat& hMu_g,
	arma::mat& hPhi_g,
	arma::mat& hVar_g);


void initialize(const arma::cube& Y,
	const arma::mat& group,
	const int K,
	arma::cube& MuObs,
	arma::cube& VarObs,
	arma::field<arma::sp_mat>& Lambda,
	arma::cube& F,
	arma::cube& h,
	arma::mat& hMu_g, // K x (G*S) 
	arma::mat& hPhi_g, // K x (G*S) 
	arma::mat& hVar_g);


void update_meanvar(arma::mat& ParaStats, // nelem x 5
	arma::cube& ParaNew, // dim1 x dim2 x dim3
	const int& idx);

void update_quantile(arma::cube& ParaNew, 
	std::vector<stmpct::gk<double>>& ParaPct);


arma::mat get_quantile(const std::vector<stmpct::gk<double>>& ParaPct,
	const arma::vec& pct,
	const int nelem);


void label_switching(arma::field<arma::cube>& LambdaTmp, //(B)(N,K,S)
	arma::field<arma::cube>& FacTmp,//(B)(K,Tall,S)
	const unsigned int& S,
	const unsigned int& N,
	const unsigned int& K,
	const unsigned int& Tall,
	const arma::uvec& idx_keep);

#endif
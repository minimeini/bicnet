#ifndef _UTILS_H
#define _UTILS_H

#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <RcppArmadillo.h>


arma::imat get_uptri_mask(unsigned int nrow, 
	unsigned int ncol, 
	bool PosDiag);


double loglike_para(const arma::mat& Y, // N x T
	const arma::mat& Lambda, // N x K x B
	const arma::mat& F, // K x T x B
	const arma::colvec& esigma2); // N x 1


double loglike_model(const arma::mat& Y, // N x T
	const arma::cube& Lambda, // N x K x B
	const arma::cube& F, // K x T x B
	const arma::mat& esigma2); // N x B


arma::cube R2ArmaCube(SEXP Vec);
arma::mat cube2mat(arma::cube A);

SEXP dyn_corr(const arma::mat& Y, 
	SEXP Lambda, 
	SEXP h, 
	SEXP esigma2);

#endif
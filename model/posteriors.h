#ifndef _POSTERIORS_H
#define _POSTERIORS_H


#include <cstdlib>
#include <iostream>
#include <limits>
#include <cmath>
#include <algorithm>
#include <chrono>
#ifdef _OPENMP
#include <omp.h>
#endif
#include <RcppArmadillo.h>
#include <truncnorm.h>
#include "./GIGrvg/GIGrvg.h"

/*
STATIC FACTOR MODEL WITH NORMAL-GAMMA SPARSE SHRINKAGE: gibbs_static_ng

Ref: Kastner, 2019; Kastner et al., 2017.

1. sample factor and idiosyncratic variance: update_static_evar, update_static_fvar
2. sample shrinkage parameters: update_ng_lambda, update_ng_tau
3. sample factor loadings: update_Lambda, shallow_interweave_Lambda
4. sample factors: update_factors
*/


const double EPSILON = 1e-16;
const bool VERBOSE = TRUE;


// void update_static_esigma2(double& esigma2,
// 	const arma::mat& Y, 
// 	const arma::mat& Lambda, 
// 	const arma::mat& F, 
// 	const double& c_sigma, 
// 	const double& d_sigma);


arma::vec update_idi_mean(
	const arma::mat& Y,
	const arma::mat& Lambda,
	const arma::mat& F,
	const arma::vec& esigma2,
	const double& b_mu,
	const double& B_mu);


arma::mat update_observed_mean(
	const arma::mat& Y, // N x Tall
	const arma::mat& group, // Tall x G
	const arma::mat& Lambda, // N x K
	const arma::mat& F, // K x Tall
	const arma::mat& VarObs, // N x G
	const double b_mu,
	const double B_mu);


arma::vec update_static_esigma2(
	const arma::mat& Y, 
	const arma::mat& Lambda, 
	const arma::mat& F, 
	const double& c_sigma, 
	const double& d_sigma);


arma::mat update_observed_var(
	const arma::mat& Y, // N x Tall
	const arma::mat& group, // Tall x G
	const arma::mat& Lambda, // N x K
	const arma::mat& F, // K x Tall
	const double c_sigma,
	const double d_sigma);


arma::vec update_static_fsigma2(const arma::mat& F, 
	const double& c_sigma, 
	const double& d_sigma);


arma::vec update_ng_lambda2(const arma::mat& tau2, 
	const double& a_tau, 
	const double& c_lambda, 
	const double& d_lambda, 
	const bool& restricted);


arma::mat update_ng_tau2(const arma::mat&tau2_old, 
	const arma::mat& Lambda, 
	const arma::vec&lambda2, 
	const double& a_tau, 
	const bool& restricted);


void update_ng_tau2_2(arma::vec& tau2, // K x 1
	const arma::mat& Lambda, // N x K
	const double& lambda2,
	const double& a_tau);


double update_Lambda_nk(const double& logodd_prior, 
	const double& old_Lambda_nk,
	const arma::vec& Err_n, // Tall
	const arma::vec& F_k, // Tall
	const double& tau_k, // variance of normal distribution of Lambda(n,k)
	const arma::vec& esigma2_n, // G
	const int& mask_nk,
	double& Z_nk, // indicator under singlesubject model
	const unsigned int& G,
	arma::field<arma::uvec>& idx,
	const bool fixdiag);


void update_Pi_group(arma::mat& Pi, // N x K
	const arma::cube& Lambda, // N x K x S
	const arma::vec& Pi_m, // K x 1
	const arma::vec& Pi_a,
	const arma::imat& mask);


arma::vec update_Mu_ind(const arma::mat& Mu_s, // K x G
	const arma::vec& Mu_var, // K
	const double prior_mean,
	const double prior_var);


arma::vec update_Var_ind(const arma::mat& Mu_s, // K x G
	const arma::vec& Mu_ind, // K x 1
	const double c_sigma,
	const double d_sigma);


arma::mat update_static_factors(const arma::mat& Y, 
	const arma::mat& Lambda, 
	const arma::vec& esigma2, 
	const arma::vec& fsigma2);


arma::mat update_dynamic_factors(const arma::mat& Y, // N x Tall
	const arma::mat& Lambda, // N x K
	const arma::mat& esigma2, // N x G
	const arma::mat& h, // Tall x K
	const arma::uvec& gid); // Tall x 1


#endif
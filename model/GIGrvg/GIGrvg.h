#include <RcppArmadillo.h>
// #include <Rdefines.h>
#include <cmath>
#include <limits>
#include <cassert>


double _gig_mode(double lambda, 
	double omega);

void _rgig_ROU_noshift(double *res, 
	double lambda, 
	double lambda_old, 
	double omega, 
	double alpha);

void _rgig_newapproach1(double *res, 
	double lambda, 
	double lambda_old,
	 double omega, 
	 double alpha);

void _rgig_ROU_shift_alt(double *res, 
	double lambda, 
	double lambda_old, 
	double omega, 
	double alpha);

double _unur_bessel_k_nuasympt(double x, 
	double nu, 
	int islog, 
	int expon_scaled);

double do_rgig(double lambda, 
	double chi, 
	double psi);

//[[Rcpp::depends(RcppArmadillo)]]
#include "simulation.h"
using namespace Rcpp;


arma::mat left_ordered_Z(const arma::mat& Z){
	/*
	Z: N x K
	1. Each column is a binary number
	2. Convert the binary number to the decimal number
	3. Sort the columns by decimal numbers
	*/
	unsigned int K = Z.n_cols;
	unsigned int N = Z.n_rows;
	arma::uvec digits(K, arma::fill::zeros);
	for (unsigned int k=0; k<K; k++) {
		for (unsigned int i=0; i<N; i++) {
			digits(k) += Z(i,k)*std::pow(2.0,N-1-i);
		}
	}

	digits = arma::sort_index(digits,"descend");
	return Z.cols(digits);
}



//[[Rcpp::export]]
arma::mat simulate_Z(const int& N, 
	const int& K, 
	const arma::colvec& probsIn, 
	const bool& restricted=false) {

	arma::mat Z(N,K,arma::fill::zeros);

	arma::vec probs(K,arma::fill::zeros);
	if (probsIn.n_elem == 1) {
		probs.fill(probsIn[0]);
	} else{
		probs(arma::span(0,probsIn.n_elem-1)) = probsIn;
	}

	for (int k=0; k<K; k++) {
			Z.col(k) = as<arma::colvec>(Rcpp::rbinom(N,1,probs[k]));

			if ((k < (K-1)) && restricted){
				Z(k,arma::span(k+1,K-1)) = arma::zeros<arma::rowvec>(K-1-k);
			}
			
	}

	return Z;
}


//[[Rcpp::export]]
arma::mat default_Z_rlt(const int& N, const int& K) {
	// restricted lower triangular
	arma::mat Z(N,K,arma::fill::ones);
	for (int i=0; i<(K-1); i++) {
		Z(i,arma::span(i+1,K-1)) = arma::zeros<arma::rowvec>(K-1-i);
	}

	return Z;
}


//[[Rcpp::export]]
arma::mat default_Z_glt(const int& N, 
	const int& K, 
	const int& No) {// number of overlapping
	// generalized lower triangular

	int Nk = N/K + No;
	if (No>1) { Nk -= 1;}

	arma::mat Z(N,K,arma::fill::zeros);
	for (int k=0; k<(K-1); k++) {
		Z(arma::span(k*(Nk-No),(k+1)*Nk-k*No-1),k) = arma::ones<arma::colvec>(Nk);
	}

	Z(arma::span((K-1)*(Nk-No),N-1),K-1) = arma::ones<arma::colvec>(N-(K-1)*(Nk-No));
	return Z;
}


//[[Rcpp::export]]
arma::mat default_Z_swin(const unsigned int& N,
	const unsigned int& K,
	const unsigned int& wlen) {

	const unsigned int nstep = (N-wlen) / (K-1);
	arma::mat Z(N,K,arma::fill::zeros);
	for (unsigned int k=0; k<K; k++) {
		Z(arma::span(k*nstep,wlen+k*nstep-1),arma::span(k,k)).ones();
	}
	return Z;
}


arma::mat simulate_dense_Lambda(
	int N, 
	int K, 
	const arma::vec& Omega_diag){

	arma::mat Lambda(N,K,arma::fill::zeros);
	for (int k=0; k<K; k++){
		Lambda.col(k) = std::sqrt(Omega_diag(k)) * arma::randn(N,1); 
		// V(n,k) ~ N(0,Omega_diag(k))
	}

	return Lambda;
}


//[[Rcpp::export]]
arma::mat simulate_sparse_Lambda(
	const arma::mat&Z, 
	const arma::vec& Omega_diag){

	const int N = Z.n_rows;
	const int K = Z.n_cols;
	arma::mat V(N,K,arma::fill::zeros);
	for (int k=0; k<K; k++){
		V.col(k) = std::sqrt(Omega_diag(k)) * arma::randn(N,1); 
		// V(n,k) ~ N(0,Omega_diag(k))
	}

	arma::mat Lambda = Z % V;
	//Lambda.diag().ones();
	return Lambda;
}



arma::colvec simulate_h(const int& T, 
	const double& mu, 
	const double& phi, 
	const double& sigma){

	assert(std::abs(phi) < 1);
	assert(sigma > 0);

	arma::vec h(T,arma::fill::zeros);
	arma::vec eta = arma::randn(T,1);

	double h0 = mu + sigma/std::sqrt(1-phi*phi) * arma::randn(1)[0];
	h[0] = mu + phi*(h0-mu) + sigma*eta[0];
	for (int t=1; t<T; t++) {
		h[t] = mu + phi*(h[t-1]-mu) + sigma*eta[t];
	}

	return h;
}



arma::mat simulate_static_Y(
	int T, int N, int K, 
	const arma::mat& Lambda, 
	const arma::vec& Omega_diag, 
	const arma::vec& Sigma_diag) { // checked.

	// generate omega and sigma from diag
	arma::mat Omega = arma::diagmat(Omega_diag);
	arma::mat Sigma = arma::diagmat(Sigma_diag);
	arma::mat SigmaY = Lambda * Omega * Lambda.t() + Sigma;

	arma::vec muY(N,1,arma::fill::zeros);
	arma::mat Y = arma::mvnrnd(muY, SigmaY, T);

	return Y;
}



Rcpp::List simulate_dynamic_Y(
	const int& T, const int& N, const int& K, 
	const arma::mat& Lambda, 
	const arma::vec& Sigma_diag, 
	const arma::vec& hMu, 
	const arma::vec& hPhi, 
	const arma::vec& hSigma) { // checked.

	// generate stochastic volatilities from AR(1)
	arma::mat h(K,T,arma::fill::zeros);
	for (int k=0; k<K; k++) {
		h.row(k) = simulate_h(T, hMu[k], hPhi[k], hSigma[k]).t();
	}

	arma::vec epsilon(N,arma::fill::zeros);
	arma::vec fac(K,arma::fill::zeros);
	arma::mat Y(N,T,arma::fill::zeros);

	for (int t=0; t<T; t++) {
		epsilon = arma::diagmat(arma::sqrt(Sigma_diag)) * arma::randn(N);
		fac = arma::diagmat(arma::exp(h.col(t)/2)) * arma::randn(K);
		Y.col(t) = Lambda * fac + epsilon;
	}

	Rcpp::List output;
	output["Y"] = Y;
	output["h"] = h;

	return output;
}

void simulate_dynamic_Y(
	arma::mat& Y, // output, N x T
	arma::mat& h, // output, K x T
	arma::mat& fac, // K x T
	const unsigned int& T,
	const unsigned int& N,
	const unsigned int& K,
	const arma::vec& VarObs, // N x 1
	const arma::mat& Lambda, // N x K
	const arma::vec& hMu, // K x 1
	const arma::vec& hPhi,  // K x 1
	const arma::vec& hVar) { // K x 1

	for (unsigned int k=0; k<K; k++) {
		h.row(k) = simulate_h(T, hMu.at(k), hPhi.at(k), 
			std::sqrt(hVar.at(k))).t();
	}

	#ifdef _OPENMP
	#pragma omp parallel for shared(N,K,T,fac,Y,h,Lambda,VarObs) default(none) schedule(auto)
	#endif
	for (unsigned int t=0; t<T; t++) {
		fac.unsafe_col(t) = arma::diagmat(arma::exp(h.unsafe_col(t)/2.))*arma::randn(K);
		Y.unsafe_col(t) = Lambda*fac.unsafe_col(t) + arma::diagmat(arma::sqrt(VarObs))*arma::randn(N);
	}

	return;
} 


//[[Rcpp::export]]
List simulate_dynamic_single(const int& N, 
	const int& T,
	const int& K, 
	const int& Ztype, 
	const arma::vec& Omega_diag,
	const arma::vec& Sigma_diag,
	const arma::vec& hMu,
	const arma::vec& hPhi,
	const arma::vec& hSigma,
	const int& NOverlap=0,  // for default_Z_glt
	const Rcpp::Nullable<Rcpp::NumericVector>& probsIn = R_NilValue, // for simulate_Z
	const bool& restricted=false,
	const bool& sparse=false) { // for simulate_Z

	arma::mat Z(N,K,arma::fill::zeros);
	if (Ztype == 0) {
		Z = default_Z_rlt(N, K);
	} else if (Ztype == 1) {
		Z = default_Z_glt(N, K, NOverlap);
	} else if (Ztype == 2 && probsIn.isNotNull()) {
		arma::colvec probs = as<arma::colvec>(probsIn);
		Z = simulate_Z(N, K, probs, restricted);
	}

	arma::mat Lambda(N,K,arma::fill::zeros);
	if (sparse) {
		Lambda = simulate_sparse_Lambda(Z, Omega_diag);
	} else {
		Lambda = simulate_dense_Lambda(N,K,Omega_diag);
	}

	List output = simulate_dynamic_Y(T, N, K, Lambda, Sigma_diag, 
		hMu, hPhi, hSigma); 

	output["Z"] = wrap(Z);
	output["Lambda"] = wrap(Lambda);

	return(output);
}


//[[Rcpp::export]]
List simulate_dynamic_fused(const int& N, 
	const int& T,
	const int& K, 
	const int& Ztype, 
	const arma::vec& Omega_diag,
	const arma::vec& Sigma_diag,
	const arma::vec& hMu,
	const arma::vec& hPhi,
	const arma::vec& hSigma,
	const arma::vec& hMu_ast,
	const int& NOverlap=0,  // for default_Z_glt
	const Rcpp::Nullable<Rcpp::NumericVector>& probsIn = R_NilValue, // for simulate_Z
	const bool& restricted=false,
	const bool& sparse=true) { // for simulate_Z

	arma::mat Z(N,K,arma::fill::zeros);
	if (Ztype == 0) {
		Z = default_Z_rlt(N, K);
	} else if (Ztype == 1) {
		Z = default_Z_glt(N, K, NOverlap);
	} else if (Ztype == 2) {
		arma::colvec probs = as<arma::colvec>(probsIn);
		Z = simulate_Z(N, K, probs, restricted);
	}

	arma::mat Lambda(N,K,arma::fill::zeros);
	if (sparse) {
		Lambda = simulate_sparse_Lambda(Z,Omega_diag);
	} else {
		Lambda = simulate_dense_Lambda(N,K,Omega_diag);
	}

	List simA = simulate_dynamic_Y(T, N, K, Lambda, Sigma_diag, 
		hMu, hPhi, hSigma); 
	List simB = simulate_dynamic_Y(T, N, K, Lambda, Sigma_diag, 
		hMu+hMu_ast, hPhi, hSigma); 

	List output;
	output["YA"] = simA["Y"];
	output["hA"] = simA["h"];
	output["YB"] = simB["Y"];
	output["hB"] = simB["h"];
	output["Z"] = wrap(Z);
	output["Lambda"] = wrap(Lambda);

	return(output);
}


// [[Rcpp::export]]
Rcpp::List simulate_dynamic_group(const int N, 
	const int K,
	const int S,
	const arma::mat& group, // Tall x G
	const Rcpp::Nullable<Rcpp::NumericVector> VarObsIn = R_NilValue, // G x S, homogeneous variance
	const double c_sigma = 1., // ObsVar - inverse-gamma
	const double d_sigma = 1., // ObsVar - inverse-gamma
	const int Ztype = 0, 
	const Rcpp::Nullable<Rcpp::NumericVector> ZIn = R_NilValue, // N x K
	const Rcpp::Nullable<Rcpp::NumericVector> PiIn = R_NilValue, // N x K
	const Rcpp::Nullable<Rcpp::NumericVector> m_piIn = R_NilValue, // K x 1, default=0.75, IncluProb - beta
	const Rcpp::Nullable<Rcpp::NumericVector> a_piIn = R_NilValue, // K x 1, default=10, IncluProb - beta
	const double tau2=1., // slab distribution
	const Rcpp::Nullable<Rcpp::NumericVector> hMuTaskIn = R_NilValue, // K x G x S
	const double b_mu = 0., // mu of sv - normal
	const double B_mu = 1., // mu of sv - normal
	const Rcpp::Nullable<Rcpp::NumericVector> hPhiTaskIn = R_NilValue, // K x G x S
	const double a_phi = 5., // phi of sv - beta
	const double b_phi = 2.5, // phi of sv - beta
	const Rcpp::Nullable<Rcpp::NumericVector> hVarTaskIn = R_NilValue, // K x G x S
	const double Bsigma = 1.){ // delta of sv - gamma / sqrt(normal)

	int Tall = group.n_rows;
	int G = group.n_cols;

	/* BEGIN --- Input Checking */
	// [checked] VarObs - Homogeneous variance of observed variables
	arma::mat VarObs(G,S,arma::fill::ones);
	if (VarObsIn.isNotNull()) {
		Rcpp::NumericVector VarObsRcpp(VarObsIn);
		if (VarObsRcpp.size() != G*S) {
			::Rf_error("Dimension of VarObs is G x S.");
		}
		VarObsRcpp.attr("dim") = Rcpp::Dimension(G,S);
		VarObs = Rcpp::as<arma::mat>(VarObsRcpp);
	} else {
		VarObs /= arma::randg(G,S,arma::distr_param(c_sigma,1/d_sigma));
		
	}


	// [checked] Pi and Z - Group level inclusion probability matrix
	arma::mat Z(N,K,arma::fill::ones);
	if (ZIn.isNotNull()) {
		Rcpp::NumericVector ZRcpp(ZIn);
		if (ZRcpp.size() != N*K) {
			::Rf_error("Dimension of Pi is N x K.");
		}
		ZRcpp.attr("dim") = Rcpp::Dimension(N,K);
		Z = Rcpp::as<arma::mat>(ZRcpp);
	} else {
		arma::mat Z_default(N,K,arma::fill::ones);
		if (Ztype == 0) {
			Z_default = default_Z_rlt(N, K);
		}
		Z %= Z_default;
		Z.diag().ones();
	}

	arma::mat Pi(N,K,arma::fill::zeros);
	if (PiIn.isNotNull()) {
		Rcpp::NumericVector PiRcpp(PiIn);
		if (PiRcpp.size() != N*K) {
			::Rf_error("Dimension of Pi is N x K.");
		}
		PiRcpp.attr("dim") = Rcpp::Dimension(N,K);
		Pi = Rcpp::as<arma::mat>(PiRcpp);
	} else {
		Rcpp::NumericVector m_piRcpp (K,0.75);
		Rcpp::NumericVector a_piRcpp (K,10.0);
		if (m_piIn.isNotNull()) {
			m_piRcpp = Rcpp::NumericVector(m_piIn);
		}
		if (a_piIn.isNotNull()) {
			a_piRcpp = Rcpp::NumericVector(a_piIn);
		}

		for (int k=0; k<K; k++) {
			Pi.col(k) = Rcpp::as<arma::colvec>(Rcpp::rbeta(N,
				a_piRcpp[k]*m_piRcpp[k],
				a_piRcpp[k]*(1.-m_piRcpp[k])));
		}
	}

	Pi %= Z;
	Pi.diag().ones();


	// // tau2 - subject-specific shrinkage on factor loadings
	// arma::mat tau2(K,S,arma::fill::ones);
	// arma::vec lambda2(S,arma::fill::ones);
	// if (tau2In.isNotNull()) {
	// 	Rcpp::NumericVector tau2Rcpp(tau2In);
	// 	if (tau2Rcpp.size() != K*S) {
	// 		::Rf_error("Dimension of tau2 is K x S.");
	// 	}
	// 	tau2Rcpp.attr("dim") = Rcpp::Dimension(K,S);
	// 	tau2 = Rcpp::as<arma::mat>(tau2Rcpp);
		
	// } else {
	// 	if (lambda2In.isNotNull()) {
	// 		lambda2 = Rcpp::as<arma::colvec>(lambda2In);
	// 		if (lambda2.n_elem != S) {
	// 		::Rf_error("Dimension of lambda2 is S.");
	// 		}
	// 	} else {
	// 		lambda2 = arma::randg(S,arma::distr_param(c_ng,1/d_ng));
	// 	}

	// 	for (int s=0; s<S; s++) {
	// 		tau2.col(s) = arma::randg(K,
	// 			arma::distr_param(a_ng,2.0/(a_ng*lambda2(s))));
	// 	}
	// }


	// Lambda - subject-specific factor loading matrix
	arma::cube Lambda(N,K,S); Lambda.fill(0.0);
	for (int s=0; s<S; s++) { // foreach subject
		for (int n=0; n<N; n++) { // 1. Sample factor loading matrix
			for (int k=0; k<K; k++) {
				if (n==k) {
					Lambda.at(n,k,s) = 1.;
				} else {
					Lambda.at(n,k,s) = ((R::rbinom(1,Pi.at(n,k))==1)? R::rnorm(0.,std::sqrt(tau2)) : 0);
				}
			}
		}
	}
	// Lambda.each_slice() %= Z;


	// hMu and hMuVar - subject-specific base level, dim=KxS
	// arma::mat hMu, hMuVar;

	// if (hMuIn.isNotNull()) {
	// 	Rcpp::NumericVector hMuRcpp(hMuIn);
	// 	if (hMuRcpp.size() != K*S) {
	// 		::Rf_error("Dimension of hMu is K x S.");
	// 	}
	// 	hMuRcpp.attr("dim") = Rcpp::Dimension(K,S);
	// 	hMu = Rcpp::as<arma::mat>(hMuRcpp);

	// } else {
	// 	hMu = b_mu + std::sqrt(B_mu)*arma::randn(K,S);
	// }

	// if (hMuVarIn.isNotNull()) {
	// 	Rcpp::NumericVector hMuVarRcpp(hMuVarIn);
	// 	if (hMuVarRcpp.size() != K*S) {
	// 		::Rf_error("Dimension of hMuVar is K x S.");
	// 	}
	// 	hMuVarRcpp.attr("dim") = Rcpp::Dimension(K,S);
	// 	hMuVar = Rcpp::as<arma::mat>(hMuVarRcpp);

	// } else {
	// 	hMuVar = 1 / arma::randg(K,S,arma::distr_param(c_sigma,1/d_sigma));
	// }


	// hPhi and hVar - parameters for SV processes, dim=KxGxS
	arma::cube hPhi, hVar;

	if (hPhiTaskIn.isNotNull()) {
		Rcpp::NumericVector hPhiRcpp(hPhiTaskIn);
		if (hPhiRcpp.size() != K*G*S) {
			::Rf_error("Dimension of hPhi is K x G x S.");
		}
		hPhiRcpp.attr("dim") = Rcpp::Dimension(K,G,S);
		hPhi = Rcpp::as<arma::cube>(hPhiRcpp);
		
	} else {
		Rcpp::NumericVector hPhiRcpp = Rcpp::rbeta(K*G*S,a_phi,b_phi);
		hPhiRcpp.attr("dim") = Rcpp::Dimension(K,G,S);
		hPhi = Rcpp::as<arma::cube>(hPhiRcpp);

	}

	if (hVarTaskIn.isNotNull()) {
		Rcpp::NumericVector hVarRcpp(hVarTaskIn);
		if (hVarRcpp.size() != K*G*S) {
			::Rf_error("Dimension of hVar is K x G x S.");
		}
		hVarRcpp.attr("dim") = Rcpp::Dimension(K,G,S);
		hVar = Rcpp::as<arma::cube>(hVarRcpp);
	} else {
		hVar = arma::pow(std::sqrt(Bsigma)*arma::randn(K,G,S), 2.0);
		
	}


	// hMu_g - parameters for SV processes, dim=KxGxS
	arma::cube hMu_g(K,G,S); hMu_g.fill(0.0);
	if (hMuTaskIn.isNotNull()) {
		Rcpp::NumericVector hMu_gRcpp(hMuTaskIn);
		if (hMu_gRcpp.size() != K*G*S) {
			::Rf_error("Dimension of hMu_g is K x G x S.");
		}
		hMu_gRcpp.attr("dim") = Rcpp::Dimension(K,G,S);
		hMu_g = Rcpp::as<arma::cube>(hMu_gRcpp);
	} else {
		hMu_g = b_mu + std::sqrt(B_mu)*arma::randn(K,G,S);
		// for (int s=0; s<S; s++) {
		// 	for (int g=0; g<G; g++) {
		// 		hMu_g.slice(s).col(g) = hMu.col(s) + arma::diagmat(arma::sqrt(hMuVar.col(s)))*arma::randn(K,1);
		// 	}
		// }
	}

	/* END --- Input Checking */



	/* BEGIN --- Simulate Y and h */
	arma::cube Y(N,Tall,S); Y.fill(0.0);
	arma::cube h(K,Tall,S); h.fill(0.0);
	arma::cube Factor(K,Tall,S); Factor.fill(0.0);
	arma::uvec idx;
	arma::vec varobstmp(N);
	unsigned int idx_tg_from = 0;
	unsigned int idx_tg_to = 0;
	int Tg = 0;
	for (int s=0; s<S; s++) { // foreach subject
		idx_tg_from = 0;
		idx_tg_to = -1;
		for (int g=0; g<G; g++) { // foreach stimulus
			idx = arma::find(group.col(g)==1);
			Tg = idx.n_elem;
			idx_tg_from = idx_tg_to + 1;
			idx_tg_to = idx_tg_from + Tg - 1;
			varobstmp.fill(VarObs.at(g,s));

			arma::mat Ynew(N,Tg);
			arma::mat hnew(K,Tg);
			arma::mat fnew(K,Tg);
			simulate_dynamic_Y(Ynew,hnew,fnew,
				Tg,N,K,varobstmp,
				Lambda.slice(s),
				hMu_g.slice(s).col(g),
				hPhi.slice(s).col(g),
				hVar.slice(s).col(g));

			Y.slice(s).submat(arma::span::all,
				arma::span(idx_tg_from,idx_tg_to)) = Ynew;
			h.slice(s).submat(arma::span::all,
				arma::span(idx_tg_from,idx_tg_to)) = hnew;
			Factor.slice(s).submat(arma::span::all,
				arma::span(idx_tg_from,idx_tg_to)) = fnew;
		}
	}
	/* END --- Simulate Y and h */


	return Rcpp::List::create(_["Y"]=Rcpp::wrap(Y),
		_["group"]=Rcpp::wrap(group),
		_["h"]=Rcpp::wrap(h),
		_["Factor"]=Rcpp::wrap(Factor),
		// _["MuObs"]=Rcpp::wrap(MuObs),
		_["VarObs"]=Rcpp::wrap(VarObs),
		_["Pi"]=Pi,
		_["Lambda"]=Rcpp::wrap(Lambda),
		_["tau2"]=tau2,
		// _["lambda2"]=Rcpp::wrap(lambda2),
		// _["hMu"]=hMu,
		// _["hMuVar"]=hMuVar,
		_["hMu_g"]=Rcpp::wrap(hMu_g),
		_["hPhi_g"]=Rcpp::wrap(hPhi),
		_["hVar_g"]=Rcpp::wrap(hVar));

}

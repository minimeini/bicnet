source("./model/sampler.R")
# For some unknown reasons, R studio and R GUI fails 
# to compile the code. So please **run R in terminal**.

# Path of HCP rfMRI and tfMRI data
rest_path = "./data/rfMRI_roi_mean/REST1"
task_path = "./data/tfMRI_roi_mean/LANGUAGE"
rest_pref = "rfMRI_REST1_LR_AAL90_mean_" # prefix of file names
task_pref = "tfMRI_LANGUAGE_LR_AAL90_mean_"
fmrt = ".csv" # data are stored in csv files

# folder to store the results
backpath = "./output/example"
if (!dir.exists(backpath)) {
	dir.create(backpath,recursive=TRUE)
}


# prepare input data
Ybin = "Y.bin"
Ydesc = "Y.desc"
nsub = 10
input = create_y_bigmat(nsub,rest_path,rest_pref,
	task_path,task_pref,fmrt,backpath,Ybin,Ydesc)
# Store Y in a bigmemory matrix
# -- Create `Y.bin` and `Y.desc` in your 
# `backpath` folder


# MCMC - first run
Khat = 20 # Number of latent factors
nburnin = 100
niter = 100
nthin = 1
mcmc = gibbs_dfsv(input$Y_pb,
	c(input$N,input$Tall,input$S), input$group, 
	backpath, K=Khat, MultiSubject=TRUE,
	nburnin=nburnin, niter=niter, nthin=nthin)
saveRDS(mcmc,file=file.path(backpath,"mcmc.rds"))
rm(mcmc)
rm(input)
gc(reset=TRUE)
# All results are stored in the `backpath` folder


# MCMC - continued
# If you have got the MCMC output but are not satisfied
# and wanna keep running
mcmc_filepath = file.path(backpath,"mcmc.rds")
bm_folderpath = backpath
input = load_lastsample(mcmc_filepath, # mcmc file
	bm_folderpath, # other output in bigmemory format
	backpath, # folder to store new output
	rest_path=rest_path, # load input data
	rest_pref=rest_pref,
	task_path=task_path,
	task_pref=task_pref,
	fmrt=fmrt)
mcmc_cont = gibbs_dfsv(input$Y_pb,
	c(input$N,input$Tall,input$S), input$group, 
	backpath, K=Khat, MultiSubject=TRUE,
	nburnin=nburnin,niter=niter,nthin=nthin,
	Inits=input) # provide the last MCMC sample as initial values
saveRDS(mcmc_cont,file=file.path(backpath,"mcmc.rds"))
# set a different path if you don't want to overwrite 
# previous results.


# Visualization is done in Python3
# `Minimal Example Visualization.ipynb`

gelman.rubin = function(param) {
	# params
	# - iterations x chains

    # mcmc information
    n = nrow(param) # number of iterations
    m = ncol(param) # number of chains

    # calculate the mean of the means
    # colMeans: means of each column/chain, across rows/iterations
    theta.bar.bar = mean(colMeans(param))

    # within chain variance
    W = mean(apply(param, 2, var))

    # between chain variance
    B = n / (m - 1) * sum((colMeans(param) - theta.bar.bar) ^ 2)

    # variance of stationary distribution
    theta.var.hat = (1 - 1 / n) * W + 1 / n * B

    # Potential Scale Reduction Factor (PSRF)
    R.hat = sqrt(theta.var.hat / W)

    return(R.hat)
}


effective_sample_size = function(post_series, average=FALSE) {
	# N_{eff} = N / (1 + 2*sum_{h=1}^{\inf} acf(h))
	effs = NULL

	# reshape into a two-way matrix
	dim_orig = dim(post_series)
	ndim_orig = length(dim_orig)
	if (ndim_orig > 2) {
		dim(post_series) = c(prod(dim_orig[1:(ndim_orig-1)]), dim_orig[ndim_orig])
	}
	if (is.null(dim_orig)) {
		dim(post_series) = c(1, length(post_series))
	}

	# calculate ACF
	nvar = dim(post_series)[1]
	for (v in c(1:nvar)) {
		nsample = length(post_series[v,])
		tmp = acf(ts(data=post_series[v,]),
			type="correlation", plot=FALSE,
			lag.max=(nsample/2))$acf
				
		tmp = abs(c(tmp))
		tmp = nsample / (1 + 2*sum(tmp[-1]))
		effs = c(effs, tmp)
	}

	if (average) {
		effs = mean(effs, na.rm=TRUE)
	}
	
	return(effs)
}



effective_sample_size_all = function(MCMCoutput, paras_all=NULL) {
	# N_{eff} = N / (1 + 2*sum_{h=1}^{\inf} acf(h))
	if(is.null(paras_all)) {
		paras_all = c("Lambda", "F", "fsigma2", "h", "hMu", "hPhi", "hSigma")
	}
	
	paras_out = names(MCMCoutput)
	npara = length(paras_all)
	
	effs = NULL
	for (p in c(1:npara)) {
		par = paras_all[p]
		if (par %in% paras_out) {
			tmp = effective_sample_size(MCMCoutput[[par]], average=FALSE)
			effs = c(effs, tmp)
			
		}
	}
	
	effs = mean(effs, na.rm=TRUE)
	return(effs)
}


mcmc_accuracy = function(mcmc_list,
	sim_list,
	params=c("MuObsStats","VarObsStats",
		"LambdaStats","Pi","tau2","lambda2",
		"hStats","hMu_g","hPhi_g","hVar_g",
		"hMu","hMuVar")){

	params_mcmc = names(mcmc_list)
	params_sim = names(sim_list)

	# skim mcmc_list
	for (par in params_mcmc) {
		dimtmp = dim(mcmc_list[[par]])
		ndim = length(dimtmp)
		if (ndim==4) {
			if (dimtmp[ndim]==5) {
				mcmc_list[[par]] = mcmc_list[[par]][,,,4]
			} else if (dimtmp[ndim]==2) {
				mcmc_list[[par]] = mcmc_list[[par]][,,,1]
			}
		} else if (ndim==3){
			mcmc_list[[par]] = mcmc_list[[par]][,,4]
		} else if (ndim==2) {
			mcmc_list[[par]] = as.vector(mcmc_list[[par]][,4])
		}

		if (par == "hStats") {
			mcmc_list[[par]] = aperm(mcmc_list[[par]],c(2,1,3))
		}
	}

	acc_list = list()
	for (par in params) {
		if ((par %in% params_mcmc) && (par %in% params_sim)) {
			if (par=="Pi" || par=="LambdaStats") {
				# RMSE and MAE
				acc_list[[par]] = abind(apply((sim_list[[par]]-mcmc_list[[par]])^2,c(1,2),mean),
					apply(abs(sim_list[[par]]-mcmc_list[[par]]),c(1,2),mean),along=3)
			} else {
				# RMSE
				acc_list[[par]] = apply((sim_list[[par]]-mcmc_list[[par]])^2,c(1,2),mean)
			}
			
		}
	}
}



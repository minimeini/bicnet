//[[Rcpp::depends(RcppArmadillo)]]
#include "evaluation.h"
using namespace Rcpp;


// [[Rcpp::export]]
double logLikelihood(const arma::mat& Y, 
	const Rcpp::List ParaMCMC) {

	arma::cube Lambda, F;
	arma::mat esigma2;

	Lambda = R2ArmaCube(ParaMCMC["Lambda"]);
	F = R2ArmaCube(ParaMCMC["F"]);
	esigma2 = Rcpp::as<arma::mat>(ParaMCMC["esigma2"]);
	return loglike_model(Y, Lambda, F, esigma2);
}


// [[Rcpp::export]]
double logBayesFactors(const arma::mat& Y, 
	const Rcpp::List ParaMod1, 
	const Rcpp::List ParaMod2) {
	
	double loglike_1 = logLikelihood(Y, ParaMod1);
	double loglike_2 = logLikelihood(Y, ParaMod2);

	return (loglike_1 - loglike_2);
}


// [[Rcpp::export]]
double calcInfoCriteria(const arma::mat& Y, // N x Tall
	const arma::cube& Lambda, // N x K (x nsample)
	const arma::cube& Factor, // K x Tall (x nsample)
	const arma::cube& VarObs, // N x G (x nsample)
	const arma::mat& group, // Tall x G
	const unsigned int restricted=1, // uptri + one diag
	const unsigned int criteria=0, // 0=AIC, 1=BIC, 2=DIC, 3=loglikelihood
	const bool IsotropicObsVar=true) {

	const unsigned int G = group.n_cols;
	const unsigned int B = Lambda.n_slices;
	double K = Lambda.n_cols;
	
	// Step 1. Calculate point estimate
	arma::mat LamHat, FacHat, VarHat;
	if (B>1) {
		LamHat = arma::mean(Lambda,2); // N x K
    	FacHat = arma::mean(Factor,2); // K x Tall
    	VarHat = arma::mean(VarObs,2); // N x G
	} else {
		LamHat = Lambda.slice(0);
		FacHat = Factor.slice(0);
		VarHat = VarObs.slice(0);
	}

	if (LamHat.has_nan() || FacHat.has_nan() || VarHat.has_nan()) {
		::Rf_error("NaN Estimation.");
	}

	arma::uvec idx;
	arma::mat err;
	double Tg,rss;
	double DivHat = 0.;
	arma::vec DivSample;
	if (B>1) {
		DivSample.set_size(B);
		DivSample.fill(0.);
	}

	for (unsigned int g=0; g<G; g++) {
		idx = arma::find(group.unsafe_col(g) == 1.);
		Tg = idx.n_elem;
		if (Tg>0) {
			err = Y.cols(idx) - LamHat*FacHat.cols(idx);
			rss = arma::trace(err.t()*arma::diagmat(1./VarHat.unsafe_col(g))*err);
			DivHat += (Tg*arma::accu(arma::log(VarHat.unsafe_col(g))) + rss);

			if (B>1 && criteria==2) {
				for (unsigned int b=0; b<B; b++) {
					err = Y.cols(idx) - Lambda.slice(b)*Factor.slice(b).cols(idx);
					rss = arma::trace(err.t()*arma::diagmat(1./VarObs.slice(b).col(g))*err);
					DivSample.at(b) += (Tg*arma::accu(arma::log(VarObs.slice(b).col(g))) + rss);
				}
			}
		}
	}

	if (DivSample.has_nan() || !std::isfinite(DivHat)) {
		::Rf_error("NaN Divergence.");
	}

	// DivHat *= -2.;

	double N = Y.n_elem; // sample size
	double P = LamHat.n_elem + FacHat.n_elem;
	if (IsotropicObsVar) {
		P += VarHat.n_cols;
	} else {
		P += VarHat.n_elem;
	}
	
	if (restricted==1) {
		P -= 0.5*K*(K+1);
	}
	
	double val=0.;
	switch (criteria) {
		case 0: // AIC
			val = DivHat + P*2.;
			break;
		case 1: // BIC
			val = DivHat + P*std::log(N);
			break;
		case 2: // DIC
			val = 2.*arma::mean(DivSample) - DivHat;
			break;
		case 3: // loglikelihood
			val = -0.5*DivHat;
	}

	return val;

}


// [[Rcpp::export]]
double calcInfoCriteriaEst(const arma::mat& Y, // N x Tall
	const arma::mat& LamHat, // N x K (x nsample)
	const arma::mat& FacHat, // K x Tall (x nsample)
	const arma::mat& VarHat, // N x G (x nsample)
	const arma::mat& group, // Tall x G
	const unsigned int restricted=1, // uptri + one diag
	const unsigned int criteria=0, // 0=AIC, 1=BIC, 2=DIC, 3=loglikelihood
	const bool IsotropicObsVar=true) {

	const unsigned int G = group.n_cols;
	double K = LamHat.n_cols;
	
	// Step 1. Calculate point estimate
	if (LamHat.has_nan() || FacHat.has_nan() || VarHat.has_nan()) {
		::Rf_error("NaN Estimation.");
	}

	arma::uvec idx;
	arma::mat err;
	double Tg,rss;
	double DivHat = 0.;

	for (unsigned int g=0; g<G; g++) {
		idx = arma::find(group.unsafe_col(g) == 1.);
		Tg = idx.n_elem;
		if (Tg>0) {
			err = Y.cols(idx) - LamHat*FacHat.cols(idx);
			rss = arma::trace(err.t()*arma::diagmat(1./VarHat.unsafe_col(g))*err);
			DivHat += (Tg*arma::accu(arma::log(VarHat.unsafe_col(g))) + rss);
		}
	}

	if (!std::isfinite(DivHat)) {
		::Rf_error("NaN Divergence.");
	}

	// DivHat *= -2.;

	double N = Y.n_elem; // sample size
	double P = LamHat.n_elem + FacHat.n_elem;
	if (IsotropicObsVar) {
		P += VarHat.n_cols;
	} else {
		P += VarHat.n_elem;
	}
	if (restricted==1) {
		P -= 0.5*K*(K+1);
	}
	
	double val=0.;
	switch (criteria) {
		case 0: // AIC
			val = DivHat + P*2.;
			break;
		case 1: // BIC
			val = DivHat + P*std::log(N);
			break;
		case 3: // loglikelihood
			val = -0.5*DivHat;
	}

	return val;

}


// [[Rcpp::export]]
double calcInfoCriteriaLL(const arma::mat& Y, // N x Tall
	const arma::mat& LamHat, // N x K
	const arma::mat& FacHat, // K x Tall
	const arma::mat& VarHat, // N x G
	const arma::vec& loglike, // 1 x nsample
	const arma::mat& group, // Tall x G
	const unsigned int restricted=1, // uptri + one diag
	const unsigned int criteria=0, // 0=AIC, 1=BIC, 2=DIC, 3=loglikelihood
	const bool IsotropicObsVar=true) {

	const unsigned int G = group.n_cols;
	double K = LamHat.n_cols;
	
	// Step 1. Calculate point estimate
	if (LamHat.has_nan() || FacHat.has_nan() || VarHat.has_nan()) {
		::Rf_error("NaN Estimation.");
	}

	arma::uvec idx;
	arma::mat err;
	double Tg,rss;
	double DivHat = 0.;

	for (unsigned int g=0; g<G; g++) {
		idx = arma::find(group.unsafe_col(g) == 1.);
		Tg = idx.n_elem;
		if (Tg>0) {
			err = Y.cols(idx) - LamHat*FacHat.cols(idx);
			rss = arma::trace(err.t()*arma::diagmat(1./VarHat.unsafe_col(g))*err);
			DivHat += (Tg*arma::accu(arma::log(VarHat.unsafe_col(g))) + rss);
		}
	}

	if (!std::isfinite(DivHat)) {
		::Rf_error("NaN Divergence.");
	}

	// DivHat *= -2.;

	double N = Y.n_elem; // sample size
	double P = LamHat.n_elem + FacHat.n_elem;
	if (IsotropicObsVar) {
		P += VarHat.n_cols;
	} else {
		P += VarHat.n_elem;
	}
	if (restricted==1) {
		P -= 0.5*K*(K+1);
	}
	
	double val=0.;
	switch (criteria) {
		case 0: // AIC
			val = DivHat + P*2.;
			break;
		case 1: // BIC
			val = DivHat + P*std::log(N);
			break;
		case 2: // DIC
			val = -4.*arma::mean(loglike) - DivHat;
			break;
		case 3: // loglikelihood
			val = -0.5*DivHat;
	}

	return val;

}


//[[Rcpp::export]]
double MAE_2D(const arma::mat& post_Lambda,
	const arma::mat& true_Lambda){

	if (post_Lambda.n_cols != true_Lambda.n_cols) {
		::Rf_error("MAE_Lambda - numbers of factors not equal.");
	}
	int N = post_Lambda.n_elem;

	return arma::accu(arma::abs(post_Lambda-true_Lambda)) / N;
}


//[[Rcpp::export]]
double MAE_1D(const arma::vec& samples,
	double truth=0.){
	return arma::mean(arma::abs(samples-truth));
}



//[[Rcpp::export]]
double RMSE_2D(const arma::mat& post_Lambda,
	const arma::mat& true_Lambda){

	if (post_Lambda.n_cols != true_Lambda.n_cols) {
		::Rf_error("MAE_Lambda - numbers of factors not equal.");
	}
	int N = post_Lambda.n_elem;

	return std::sqrt(arma::accu(arma::square(post_Lambda-true_Lambda)) / N);
}



double RMSE_1D(const arma::vec& samples,
	double truth=0.){
	return std::sqrt(arma::mean(arma::square(samples-truth)));
}

//[[Rcpp::export]]
double RMSE_1D(const arma::vec& samples,
	const arma::vec& truth){
	return std::sqrt(arma::mean(arma::square(samples-truth)));
}


// [[Rcpp::export]]
arma::mat pairwise_err(SEXP post_corrt, // N x N x T
	SEXP true_corrt, // N x N x T
	const int& meas_type){
	/*
	meas_type: 1=RMSE, 2=MAE
	*/

	arma::cube post_corrt_arma = R2ArmaCube(post_corrt); // N x N x T
	arma::cube true_corrt_arma = R2ArmaCube(true_corrt); // N x N x T

	const int N = post_corrt_arma.n_rows;
	const int T = post_corrt_arma.n_slices;

	arma::vec post(T,arma::fill::zeros);
	arma::vec truth(T,arma::fill::zeros);
	arma::vec diff(T,arma::fill::zeros);
	arma::mat err(N,N,arma::fill::zeros);

	for (int i=0; i<N; i++) {
		for (int j=0; j<N; j++) {
			post = post_corrt_arma.subcube(i,j,0,i,j,T-1);
			truth = true_corrt_arma.subcube(i,j,0,i,j,T-1);
			diff = post - truth;

			if (meas_type == 1) { // RMSE
				err(i,j) = std::sqrt(arma::accu(arma::square(diff)) / T);
			} else if (meas_type == 2) { // MAE
				err(i,j) = arma::accu(arma::abs(diff)) / T;
			}
		}
	}

	err = arma::trimatu(err,1);

	return err;
}


//[[Rcpp::export]]
arma::cube MCMC_MeanStd_2D(arma::cube& Para){ // dim1 x dim2 x nsamples
	unsigned int nsamples = Para.n_slices;
	unsigned int nelem = Para.n_rows*Para.n_cols;

	arma::mat ParaMat = arma::reshape(arma::mat(Para.memptr(),
		Para.n_elem,1,false),nelem,nsamples);
	arma::mat ParaMeanStd(nelem,2);

	ParaMeanStd.col(0) = arma::mean(ParaMat,1);
	ParaMeanStd.col(1) = arma::stddev(ParaMat,0,1);
	return arma::cube(ParaMeanStd.memptr(),Para.n_rows,Para.n_cols,2,false);
}


//[[Rcpp::export]]
arma::mat MCMC_MeanStd_1D(const arma::mat& Para){ // dim1 x nsamples
	unsigned int nelem = Para.n_rows;
	arma::mat ParaMeanStd(nelem,2);
	ParaMeanStd.col(0) = arma::mean(Para,1);
	ParaMeanStd.col(1) = arma::stddev(Para,0,1);
	return ParaMeanStd;
}


//[[Rcpp::export]]
arma::cube MCMC_Quantile_2D(arma::cube& Para, // dim1 x dim2 x nsamples
	arma::vec pct){
	unsigned int nsamples = Para.n_slices;
	unsigned int nelem = Para.n_rows*Para.n_cols;
	unsigned int npct = pct.n_elem;

	arma::mat ParaMat = arma::reshape(arma::mat(Para.memptr(),
		Para.n_elem,1,false),nelem,nsamples);
	arma::mat ParaQuantile = arma::quantile(ParaMat,pct,1); // nelem x npct
	return arma::reshape(arma::cube(ParaQuantile.memptr(),nelem*npct,1,1,false),
		Para.n_rows,Para.n_cols,npct);
}


//[[Rcpp::export]]
arma::mat MCMC_Quantile_1D(const arma::mat& Para, // dim1 x nsamples
	arma::vec pct){
	return arma::quantile(Para,pct,1);
}


// [[Rcpp::export]]
arma::mat MCMC_EffectiveSampleSize_2D(arma::cube& Para, // dim1 x dim2 x nsamples
	unsigned int maxlag=0){
	// TODO - use FFT for fast computation
	// REF - https://lingpipe-blog.com/2012/06/08/autocorrelation-fft-kiss-eigen/
	// - The results are consistent with the R version using acf from R
	// - But the R version outperforms Cpp
	// slice by slice, column by column
	unsigned int dim1 = Para.n_rows;
	unsigned int dim2 = Para.n_cols;
	unsigned int nelem = dim1*dim2;
	unsigned int nsamples = Para.n_slices;
	if (maxlag == 0) {
		maxlag = (int)(nsamples/2);
	}

	maxlag = std::min(maxlag,nsamples);

	arma::mat ParaMat = arma::reshape(arma::mat(Para.memptr(),
		Para.n_elem,1,false),nelem,nsamples);
	arma::mat ness(dim1,dim2,arma::fill::zeros);

	double tmpsum = 0;
	double tmpvar = 0;
	double tmp = 0;
	for (unsigned int i=0; i<nelem; i++) {
		tmpsum = 0; // sum of autocorrelation from lag 1 to (nsample-1)
		tmpvar = arma::var(ParaMat.row(i));
		for (unsigned int lag=1; lag<maxlag+1; lag++) { // for each lag lag
			tmp = arma::mean(arma::square(arma::diff(ParaMat.row(i),lag))); // sample variogram
			tmp = 1 - tmp / (2*tmpvar); // sample autocorrelation
			if (tmp>0) {
				tmpsum += tmp;
			}
		}

		ness(i) = nsamples / (1+2*tmpsum);
	}

	return ness;
}


//[[Rcpp::export]]
arma::vec MCMC_EffectiveSampleSize_1D(const arma::mat& Para, // nsamples x ndim
	unsigned int maxlag=0){
	// TODO - use FFT for fast computation
	// REF - https://lingpipe-blog.com/2012/06/08/autocorrelation-fft-kiss-eigen/
	// - The results are consistent with the R version using acf from R
	// - But the R version outperforms Cpp
	// slice by slice, column by column
	unsigned int nelem = Para.n_cols;
	unsigned int nsamples = Para.n_rows;
	if (maxlag == 0) {
		maxlag = (int)(nsamples/2);
	} else {
		maxlag = std::min(maxlag,nsamples);
	}

	arma::vec ness(nelem,arma::fill::zeros);

	double tmpsum = 0;
	double tmpvar = 0;
	double tmp = 0;
	for (unsigned int i=0; i<nelem; i++) {
		tmpsum = 0; // sum of autocorrelation from lag 1 to (nsample-1)
		tmpvar = arma::var(Para.unsafe_col(i));
		for (unsigned int lag=1; lag<maxlag+1; lag++) { // for each lag lag
			tmp = arma::mean(arma::square(arma::diff(Para.unsafe_col(i),lag))); // sample variogram
			tmp = 1. - tmp / (2.*tmpvar); // sample autocorrelation
			if (tmp>0.) {
				tmpsum += tmp;
			}
		}

		ness.at(i) = nsamples / (1+2*tmpsum);
	}

	return ness;
}


//[[Rcpp::export]]
arma::mat MCMC_MCSE_2D(const arma::mat& ParaStd, // dim1 x dim2
	const arma::mat& ParaNess){ // dim1 x dim2
	return ParaStd / arma::sqrt(ParaNess);
}


//[[Rcpp::export]]
arma::vec MCMC_MCSE_1D(const arma::vec& ParaStd,
	const arma::vec& ParaNess){
	return ParaStd / arma::sqrt(ParaNess);
}


//[[Rcpp::export]]
Rcpp::NumericVector Gelman_Rubin(SEXP mcmc_data_){
	// mcmc_data: niter x nchain x dim1 (x dim2)
	Rcpp::NumericVector mcmc_data(mcmc_data_);
	Rcpp::NumericVector full_dim = mcmc_data.attr("dim");
	unsigned int ndim = full_dim.length();
	unsigned int niter = full_dim(0);
	unsigned int nchain = full_dim(1);

	Rcpp::NumericVector dim_rcpp = full_dim[Rcpp::Range(2,ndim-1)];
	arma::vec dim_arma = Rcpp::as<arma::vec>(dim_rcpp);
	unsigned int nelem = arma::prod(dim_arma);

	arma::cube mcmc_arma(mcmc_data.begin(),niter,nchain,nelem,false);
	arma::rowvec chain_mean(nchain,arma::fill::zeros);
	arma::dvec mcmc_gr(nelem,arma::fill::zeros);

	double W,B,Vhat;
	for (unsigned int i=0; i<nelem; i++) {
		// within chain variance
		W = arma::mean(arma::var(mcmc_arma.slice(i),0,0));

		// between chain variance
		chain_mean = arma::mean(mcmc_arma.slice(i),0); 
		B = (double)arma::accu(arma::pow(chain_mean-arma::mean(chain_mean),2.0));
		B *= (double)(niter/(nchain-1));

		// variance of stationary distribution
		Vhat = (double)(1.0-1.0/niter)*W + (double)(1.0/niter)*B;
		// Potential scale reduction factor
		mcmc_gr(i) = std::sqrt((double)(Vhat / W)); 
	}

	Rcpp::NumericVector output = Rcpp::wrap(mcmc_gr);
	output.attr("dim") = dim_rcpp;
	return output;
}


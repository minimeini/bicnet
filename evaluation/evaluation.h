#ifndef _EVALUATION_H
#define _EVALUATION_H

#include <iostream>
#include <cassert>
#include <cmath>
#include <algorithm>
#include <RcppArmadillo.h>
#include "../model/utils.h"
#include "../model/posteriors.h"


double logLikelihood(const arma::mat& Y, 
	const Rcpp::List ParaMCMC);

double logBayesFactors(const arma::mat& Y, 
	const Rcpp::List ParaMod1,
	const Rcpp::List ParaMod2);

double calcInfoCriteria(const arma::mat& Y, // N x Tall
	const arma::cube& Lambda, // N x K (x nsample)
	const arma::cube& Factor, // K x Tall (x nsample)
	const arma::cube& VarObs, // N x G (x nsample)
	const arma::mat& group, // Tall x G
	const unsigned int restricted, // uptri + one diag
	const unsigned int criteria,
	const bool IsotropicObsVar);

double calcInfoCriteriaEst(const arma::mat& Y, // N x Tall
	const arma::mat& LamHat, // N x K (x nsample)
	const arma::mat& FacHat, // K x Tall (x nsample)
	const arma::mat& VarHat, // N x G (x nsample)
	const arma::mat& group, // Tall x G
	const unsigned int restricted, // uptri + one diag
	const unsigned int criteria,
	const bool IsotropicObsVar);


double calcInfoCriteriaLL(const arma::mat& Y, // N x Tall
	const arma::mat& LamHat, // N x K
	const arma::mat& FacHat, // K x Tall
	const arma::mat& VarHat, // N x G
	const arma::vec& loglike, // 1 x nsample
	const arma::mat& group, // Tall x G
	const unsigned int restricted, // uptri + one diag
	const unsigned int criteria,
	const bool IsotropicObsVar);


double MAE_2D(const arma::mat& post_Lambda,
	const arma::mat& true_Lambda);

double MAE_1D(const arma::vec& samples,
	double truth);

double RMSE_2D(const arma::mat& post_Lambda,
	const arma::mat& true_Lambda);

double RMSE_1D(const arma::vec& samples,
	double truth);

double RMSE_1D(const arma::vec& samples,
	const arma::vec& truth);

arma::mat pairwise_err(SEXP post_corrt, 
	SEXP true_corrt, 
	const int& meas_type);

arma::cube MCMC_MeanStd_2D(arma::cube& Para);
arma::mat MCMC_MeanStd_1D(const arma::mat& Para);

arma::cube MCMC_Quantile_2D(arma::cube& Para, // dim1 x dim2 x nsamples
	arma::vec pct);
arma::mat MCMC_Quantile_1D(const arma::mat& Para, // dim1 x nsamples
	arma::vec pct);

arma::mat MCMC_EffectiveSampleSize_2D(arma::cube& Para,
	unsigned int maxlag);
arma::vec MCMC_EffectiveSampleSize_1D(const arma::mat& Para, // dim1 x nsamples
	unsigned int maxlag);

arma::mat MCMC_MCSE_2D(const arma::mat& ParaStd, // dim1 x dim2
	const arma::mat& ParaNess);
arma::vec MCMC_MCSE_1D(const arma::vec& ParaStd,
	const arma::vec& ParaNess);


Rcpp::NumericVector Gelman_Rubin(SEXP mcmc_data_);

#endif
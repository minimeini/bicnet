Nnode=(6 20 50 100)
Tg=(100 500 1000)
G=(2 4 6)
S=(1 10 50)

for nn in ${Nnode[@]}; do
  for tt in ${Tg[@]}; do
  	for gg in ${G[@]}; do
  		for ss in ${S[@]}; do
  			sbatch ./run_scalability.sh ${nn} ${tt} ${gg} ${ss}
  		done
  	done
  done
done
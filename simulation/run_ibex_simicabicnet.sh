#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20
#SBATCH --partition=batch
#SBATCH --array=0-5

#SBATCH -J simicabicnet
#SBATCH -o simicabicnet.%J.out
#SBATCH -e simicabicnet.%J.err

#SBATCH --mail-user=meini.tang@kaust.edu.sa
#SBATCH --mail-type=ALL

#SBATCH --time=24:00:00
#SBATCH --mem=8G

## run the application:
nburnin=${1:-10000}
niter=${2:-20000}
kest=${3:-18}
simfilename=${4:-"sim_ica_K18_S20_G1_20Nov18-16:09:31.rds"}
savepath=${5:-"/tmp/output"}

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
module load gcc/6.4.0
module load R/3.6.0/gnu-6.4.0
Rscript ../rutils/inst.R

for seed in 1 2 3 4 5
do
	R -e "source('main_simulation_ica_vs_bicnet.R');main_estimation_ica_vs_bicnet('../data/${simfilename}',${kest},nburnin=${nburnin},niter=${niter},PiPrior=${SLURM_ARRAY_TASK_ID},output_path='${savepath}',seed=${seed});"
	echo "Done simulation of ICA vs BICNET."
done

rsync -a /tmp/output tangm0a@ilogin.ibex.kaust.edu.sa:/ibex/scratch/tangm0a/repository/spdynfac/output/${SLURM_JOB_ID}_${SLURM_ARRAY_TASK_ID}
echo "Done simulation of ICA vs BICNET."

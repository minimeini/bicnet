function [sesInfo] = gift_roi(ntime,nroi,nfac,varargin)

% change `group_modality` in `./icatb/icatb_defaults.m` to EEG
inputFilesPath = fullfile(getenv('HOME'),'data/fMRI_mat');
prefix = 'gica_hcp200_roi';
info_mat_path = "../info/gift_info.mat";
reductPCT = 1.5;
blockPCT = 0.3;
outputDir = '../output/gica_roi';


if nargin > 3, inputFilesPath = varargin{1}; end
if nargin > 4, prefix = varargin{2}; end
if nargin > 5, info_mat_path = varargin{3}; end
if nargin > 6, reductPCT = varargin{4}; end
if nargin > 7, blockPCT = varargin{5}; end
if nargin > 8, outputDir = varargin{6}; end

if exist(outputDir,'dir') == 0
    mkdir(outputDir);
end

load(info_mat_path);
inputFiles = dir(inputFilesPath);
excludePattern = {'.' '..' '.DS_Store'};
idx = ~ismember({inputFiles.name},excludePattern);
inputFiles = inputFiles(idx);
nsub = numel(inputFiles);

%%
addpath("~/software/GroupICAT/icatb");
addpath("~/software/GroupICAT/icatb/icatb_analysis_functions");
addpath("~/software/GroupICAT/icatb/icatb_analysis_functions/icatb_algorithms");
addpath("~/software/GroupICAT/icatb/icatb_batch_files");
addpath("~/software/GroupICAT/icatb/icatb_io_data_functions");
addpath("~/software/GroupICAT/icatb/icatb_parallel_files");
addpath("~/software/GroupICAT/icatb/icatb_scripts");
addpath("~/software/GroupICAT/icatb/icatb_display_functions");
addpath("~/software/GroupICAT/icatb/icatb_helper_functions");
addpath("~/software/GroupICAT/icatb/icatb_mex_files");
addpath("~/software/GroupICAT/icatb/icatb_spm_files");
addpath("~/software/GroupICAT/icatb/icatb_mancovan_files");


%% Edit input files
sesInfo.outputDir = outputDir;
sesInfo.userInput.pwd = outputDir;

for i = 1:nsub
    inputFiles(i).name = strcat(inputFiles(i).folder,'/',inputFiles(i).name);
end

fname = fieldnames(inputFiles);
inputFiles = rmfield(inputFiles,fname(2:end));

sesInfo.inputFiles = inputFiles;
sesInfo.userInput.files = inputFiles;

%% Change other variables
sesInfo.userInput.numOfSub = nsub;
sesInfo.userInput.numOfGroups1 = nsub;
sesInfo.userInput.diffTimePoints = ones(1,nsub) .* nroi;
sesInfo.userInput.numOfPC1 = ceil(nfac * reductPCT);
sesInfo.userInput.numOfPC2 = nfac;
sesInfo.userInput.numComp = nfac;
sesInfo.userInput.mask_ind = [1:ntime]';
sesInfo.userInput.HInfo.DIM(1) = ntime;
sesInfo.userInput.HInfo.V.dim(1) = ntime;
sesInfo.userInput.ICA_Options{20} = nfac;
sesInfo.userInput.ICA_Options{2} = ceil(ntime * blockPCT);

sesInfo.numOfScans = nroi;
sesInfo.numOfSub = nsub;
sesInfo.numOfDataSets = nsub;
sesInfo.numComp = nfac;
sesInfo.diffTimePoints = ones(1,nsub) .* nroi;
sesInfo.ICA_Options = sesInfo.userInput.ICA_Options;
sesInfo.mask_ind = sesInfo.userInput.mask_ind;


sesInfo.reduction(1).numOfGroupsBeforeCAT = nsub;
sesInfo.reduction(1).numOfGroupsAfterCAT = nsub;
sesInfo.reduction(1).numOfPCBeforeCAT = ones(1,nsub) .* nroi;
sesInfo.reduction(1).numOfPCAfterReduction = ceil(nfac * reductPCT);
sesInfo.reduction(1).numOfPrevGroupsInEachNewGroupAfterCAT = ones(1,nsub);
sesInfo.reduction(1).numOfPCInEachGroupAfterCAT = ones(1,nsub) .* nroi;

sesInfo.reduction(2).numOfGroupsBeforeCAT = nsub;
sesInfo.reduction(2).numOfPCBeforeCAT = ceil(nfac * reductPCT);
sesInfo.reduction(2).numOfPCAfterReduction = nfac;
sesInfo.reduction(2).numOfPrevGroupsInEachNewGroupAfterCAT = nsub;
sesInfo.reduction(2).numOfPCInEachGroupAfterCAT = ...
    sesInfo.reduction(2).numOfPCBeforeCAT * ...
    sesInfo.reduction(2).numOfGroupsBeforeCAT;

sesInfo.userInput.prefix = prefix;

%%
sesInfo = icatb_runAnalysis(sesInfo,1);
save(info_mat_path,'sesInfo');

end
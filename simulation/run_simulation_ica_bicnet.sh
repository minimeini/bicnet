#!/bin/bash

PiPrior=${1:-0}
nburnin=${2:-10000}
niter=${3:-20000}
kest=${4:-18}
simfilename=${5:-"sim_ica_K18_S20_G1_20Nov18-16:09:31.rds"}
savepath=${6:-"../output"}
gicapath=${7:-"gift_g1_mean_component_ica_s1_.mat"}

for seed in 1 2 3 4 5
do
	R -e "source('main_simulation_ica_vs_bicnet.R');main_estimation_ica_vs_bicnet('../data/${simfilename}',${kest},nburnin=${nburnin},niter=${niter},PiPrior=${PiPrior},output_path='${savepath}',seed=${seed},gica_path='${gicapath}');"
	echo "Done simulation of ICA vs BICNET."
done

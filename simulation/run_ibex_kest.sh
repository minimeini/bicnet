#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=40
#SBATCH --partition=batch

#SBATCH -J simkest
#SBATCH -o simkest.%J.out
#SBATCH -e simkest.%J.err

#SBATCH --mail-user=meini.tang@kaust.edu.sa
#SBATCH --mail-type=ALL

#SBATCH --time=24:00:00
#SBATCH --mem=16G

## run the application:
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

module load gcc/6.4.0
module load R/3.6.0/gnu-6.4.0
R -e "source('main_simulation_kest.R');main_simulation_kest();q();"

echo "Done simulation of kest."

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=40
#SBATCH --partition=batch

#SBATCH -J simscalability
#SBATCH -o simscalability.%J.out
#SBATCH -e simscalability.%J.err

#SBATCH --mail-user=meini.tang@kaust.edu.sa
#SBATCH --mail-type=ALL

#SBATCH --time=48:00:00
#SBATCH --mem=16G

## run the application:
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
module load gcc/6.4.0
module load R/3.6.0/gnu-6.4.0

R -e "source('main_simulation_scalability.R');main_simulation_scalability(N=$1,Tg=$2,G=$3,S=$4);"
echo "Done simulation of scalability."





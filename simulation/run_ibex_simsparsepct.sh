#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=40
#SBATCH --partition=batch
#SBATCH --array=0-5

#SBATCH -J simsparsepct
#SBATCH -o simsparsepct.%J.out
#SBATCH -e simsparsepct.%J.err

#SBATCH --mail-user=meini.tang@kaust.edu.sa
#SBATCH --mail-type=ALL
 
#SBATCH --time=12:00:00
#SBATCH --mem=8G

## run the application:
isibex=${1:-1}

SparsePct=(0.5 0.6 0.7 0.8 0.9 1.0)

if [[ $isibex -eq 1 ]]
then
	export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
	module load gcc/6.4.0
	module load R/3.6.0/gnu-6.4.0
	Rscript ../rutils/inst.R
	R -e "source('main_simulation_sparsepct.R');main_simulation_sparsepct(SparsePct=${SparsePct[${SLURM_ARRAY_TASK_ID}]},save_path_data='/tmp/output');"
else
	for pct in ${SparsePct[@]}; do
		R -e "source('main_simulation_sparsepct.R');main_simulation_sparsepct(SparsePct=${pct},save_path_data='../output');"
	done
fi


echo "Done simulation of sparsepct."

if [[ $isibex -eq 1 ]]
then
	rsync -a /tmp/output tangm0a@ilogin.ibex.kaust.edu.sa:/ibex/scratch/tangm0a/repository/bicnet/output/${SLURM_JOB_ID}
fi
echo "Done."
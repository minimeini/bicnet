#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=batch

#SBATCH -J simicabicnet
#SBATCH -o simicabicnet.%J.out
#SBATCH -e simicabicnet.%J.err

#SBATCH --mail-user=meini.tang@kaust.edu.sa
#SBATCH --mail-type=ALL

#SBATCH --time=00:30:00
#SBATCH --mem=16G

## run the application:
df -h /tmp
